/*
 * History
 * kellarev 2015/10/09 iridium_data_server.c
 * andrei 2015/10/22 main.c
 * @file main.c
 *
 * Build:
 * 		g++ -g -O2 -o iridium_data_server -I /usr/include/postgresql main.c checkuptime.cpp dbpgiridium.cpp geolocation.cpp iridiummessageparser.cpp dump.cpp iridiummessage.cpp iridiumpacket.c -lpq
 * or cd collarie; make
 * Files:
 *		/home/andrei/src/iridium -> /opt/projects/iridium/
 *
 * rm -f ./tcp5;clear;gcc ./tcp5.c -o ./tcp5 -lpq;./tcp4
 */

#define PRIu64  "lu"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <netdb.h>

#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <math.h>
#include <limits.h>
#include <libpq-fe.h>
#include "iridiumpacketattr.h"
#include "dbpgiridium.h"

#define DEF_TIME_ZONE_SECS	9 * 3600
#define INPORT 5680 /* Порт, на который будет идти соединение с пользователем*/
#define OUTPORT 22255 /* Порт, на который будет идти соединение с пользователем*/
#define BACKLOG 12  /* Сколько ожидающих подключений может быть в очереди */
#define BufferLength 82
#define SIZE 256
#define UPTIME 468
#define MESSAGE 154
#define SENDMESSAGESUM 441
#define SENDXLS	22222
#define PGRES_TUPLES_OK 8916403
#define PGRES_FATAL_ERROR 8916488

#define OLD_IRIDIUM_MSG_ADDR	"12.47.179.12"
#define NEW_IRIDIUM_MSG_ADDR	"5.3.83.213"	// 17.01.2018  5.3.83.213 sbd1.iridium-russia.com
// #define IRIDIUM_MSG_ADDR	"84.237.104.57"	// komisia218. e:\bin\nc -l -p 10800 -o a.txt
#define IRIDIUM_MSG_PORT	10800

#define WD "/opt/projects/iridium"

#define SQL_LOG_FILE WD"/DATA/sql_err.log"
#define RAW_LOG_FILE WD"/DATA/RAW_DATA_IRIDIUM"
#define GARBAGE WD"/DATA/GARBAGE"
#define LOG_FILE WD"/DATA/iridium.log"

#define SER_VER "v.0.05"

const char *MESG_PREFIX = "MESSG";
const char *MESG_DELIM = ":";

PGconn *mycon;// = PQconnectdb("host=pgsql.ysn.ru dbname=iridium user=imz password='imz|LC50vbHc'");

int SQL(PGconn *mycon, const char *command);

static int columns[] = {
	DATA_PROTVER, DATA_PROTSIZE, DATA_DIRECT_ID, DATA_DIRECT_SIZE,
	DATA_CDRREF, DATA_IMEI, DATA_DIRECT_STAT, DATA_MOMSN, DATA_MTMSN,
	DATA_DIRECT_DATE, DATA_LOC_ID, DATA_LOC_SIZE, DATA_LOC_DIR,
	DATA_LATITUDE, DATA_LONGITUDE, DATA_CEPRADIUS, DATA_PAYLOAD_ID,
	DATA_PAYLOAD_SIZE, DATA_PAYLOAD_TYPE,
	DATA_LATF, DATA_LONF, DATA_HF, DATA_ID, DATA_PACKETDATE,
	DATA_END
};

static int ALL_COLUMNS[] = {
	1, 2, 3, 4, 5, 6, 7, 8, 9,
	10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
	20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
	30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
	40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
	50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
	DATA_END
};

/*
 Protocol Revision Number char 1 1
 Overall Message Length unsigned short 2 104
 MO Header IEI char 1 0x01
 MO Header Length unsigned short 2 28
 CDR Reference (Auto ID) unsigned integer 4 1234567
 IMEI char 15 300034010123450
 Session Status unsigned char 1 0 (Transfer OK)
 MOMSN unsigned short 2 54321
 MTMSN unsigned short 2 12345
 Time of Session unsigned integer 4 1135950305 (12/30/05 13:45:05)
 MO Payload IEI char 1 0x02
 MO Payload Length unsigned short 2 70
 MO Payload char 70 Payload Bytes
 */

//Payload1, Payload2, Payload2,
double GetDoubleM(unsigned char data0, unsigned char data1, unsigned char data2, unsigned char data3, unsigned char data4, unsigned char data5, unsigned char data6, unsigned char data7)
{
	//printf("\n\n\n\n\n\n\nIN BYTES:%.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x\n",data0,data1,data2,data3,data4,data5,data6,data7);
	union
	{
		double num;
		unsigned char byte[8];
	} SimpleFloat;
	SimpleFloat.byte[0] = data5;
	SimpleFloat.byte[1] = data4;
	SimpleFloat.byte[2] = data7;
	SimpleFloat.byte[3] = data6;
	SimpleFloat.byte[4] = data1;
	SimpleFloat.byte[5] = data0;
	SimpleFloat.byte[6] = data3;
	SimpleFloat.byte[7] = data2;
	return SimpleFloat.num;
}

// GDOP, PDOP
float GetSingleM(unsigned char data0, unsigned char data1, unsigned char data2, unsigned char data3)
{
	//printf("\nIN BYTES:%.2x %.2x %.2x %.2x\n",data0,data1,data2,data3);
	union
	{
		float num;
		unsigned char byte[4];
	} SimpleFloat;
	SimpleFloat.byte[0] = data3;
	SimpleFloat.byte[1] = data2;
	SimpleFloat.byte[2] = data1;
	SimpleFloat.byte[3] = data0;
	//	printf("GET SINGLE M INVOKE %E\n", SimpleFloat.num);
	return SimpleFloat.num;
}

// copy packet to the pkt2
// ------------------------------------------

/**
 * @brief open TCP/IP socket
 * @param sock return value
 * @param host host name or address
 * @param port port number
 */
int open_socket
(
	int *sock,
	const char *host,
	int port
)
{
	*sock = socket(AF_INET, SOCK_STREAM, 0);
	if (*sock < 0)
	{
		perror("Не могу создать сокет");
		return -1;
	}

	int optval = 1;
	setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	struct linger ling;
	ling.l_onoff = 1;
	ling.l_linger = 0;
	setsockopt(*sock, SOL_SOCKET, SO_LINGER, &ling, sizeof(ling));

	struct sockaddr_in ni_addr;
	bzero(&ni_addr, sizeof(ni_addr));
	ni_addr.sin_family = AF_INET;
	ni_addr.sin_addr.s_addr	= inet_addr(host);
	ni_addr.sin_port = htons(port);

	if (connect(*sock, (struct sockaddr *) &ni_addr, sizeof(ni_addr)) == -1)
	{
		perror("Не могу соединить сокет");
		// shutdown(*sock, SHUT_RDWR);
		close(*sock);
		return -3;
	}

	return 0;
}

void raw_to_port
(
	unsigned char *a, 
	int size,
	const char *intface, 
	int port
)
{
	int sock;
	int r = open_socket(&sock, intface, port);
	if (r == 0)
	{
		int n = write(sock, a, size);
		if (n < 0)
			perror("ошибка записи в порт");
		// shutdown(sock, SHUT_RDWR);
		close(sock);
	}
}
// ------------------------------------------

//WRITE RAW PACKET TO FILE
void raw_to_file(unsigned char *a, int size, char *d)
{
	FILE *fp;
	int i = 0;
	fp = fopen(RAW_LOG_FILE, "a");
	if (fp == NULL)
	{
		perror("ошибка открытия файла RAW_LOG_FILE");
	}
	if (size > 81)
		size = 81;
	for (i = 0; i < size; i++)
	{
		fprintf(fp, "%c", a[i]);
	}

	fprintf(fp, "\n");
	fprintf(fp, "%s", d);
	fprintf(fp, "\n");
	fclose(fp);
}

void garb_to_file(char *a)
{
	FILE *fp;
	fp = fopen(GARBAGE, "a");
	if (fp == NULL)
	{
		perror("ошибка открытия файла GARBAGE");
	}
	fprintf(fp, "%s", a);
	fclose(fp);
}

static char CMDID[5];
static uint16_t commandSequence;
static char *nextCommandIdentifier()
{
	snprintf(CMDID, 5, "%4.4x", commandSequence);
	commandSequence++;
	return CMDID;
}

//REPLACE ONE CHARACTER TO ANOTHER
char *charrepl(const char *str, char oldChar, char newChar)
{
	char *strPtr = (char *) str;
	while ((strPtr = strchr(strPtr, oldChar)) != NULL)
		*strPtr++ = newChar;
	return strPtr;
}

//GET LOCAL SYSTEM TIME
char *glt()
{
	struct timeval tv;
	time_t curtime;
	char tbuf[30];
	static char micro_date[30];
	bzero(micro_date, 30);
	bzero(tbuf, 30);
	gettimeofday(&tv, NULL);
	curtime = tv.tv_sec;
	strftime(tbuf, 30, "%d-%m-%Y %T.", localtime(&curtime));
	sprintf(micro_date, "%s%ld", tbuf, tv.tv_usec);
	return micro_date;
}
//IF ERROR WITH DB THAT WILL BE WRITE TO LOG FILE!
void sql_log(const char *mess, const char *cmd)
{
	FILE *sqlerr = fopen(SQL_LOG_FILE, "a+");
	if (sqlerr == NULL)
	{
		perror("ошибка открытия файла логов для SQL");
		perror(SQL_LOG_FILE);
	}
	fprintf(sqlerr, "%s\tCOMMAND:%s\n\t\t\t\t%s\n", glt(), cmd, charrepl(mess, '\n', ' '));
	fclose(sqlerr);
}
//	printf("IN SQL LOG CMD:%s MESS:%s\nREPLACEDCHAR:%s\nLOCTIME:%s\n",cmd,mess,charrepl(mess,'\n',' '),getloctime());


void iridium_log(const char *mess)
{
	//printf("PID %d",getpid());
	FILE *lf = fopen(LOG_FILE, "a+");
	if (lf == NULL)
	{
		perror("Ошибка открытия файла логов Iridium");
		perror(LOG_FILE);
	}
	fprintf(lf, "%s\t%s\n", glt(), mess);
	fclose(lf);
	PGconn *log_conn = PQconnectdb("host=pgsql.ysn.ru dbname=iridium user=imz password='LC50vbHc'");
	char command[4096];
	bzero(command, 4096);
	sprintf(command, "insert into web_log(event,web_user) values('%s','server');", mess);
	PGresult *result = PQexec(log_conn, command);
	PQclear(result);
	PQfinish(log_conn);
}

const char *STRNULL = "NULL";

const char *selectSQL(PGconn *mycon, char *command)
{
	static char retbuf[1024];
	bzero(retbuf, 1024);
	PGresult *result;
	result = PQexec(mycon, command);
	printf("STATUS of exec is %s INT:%s ERR:%s SIZE_ERR:%ld\n", PQresStatus(PQresultStatus(result)), PQresStatus(PQresultStatus(result)), PQresultErrorMessage(result), sizeof(PQresultErrorMessage(result)));
	//	if((int)PQresStatus(PQresultStatus(result)) == PGRES_FATAL_ERROR ){
	sql_log(PQresultErrorMessage(result), command);
	PQclear(result);//return "ERR";
	//	}
	//	else if((int)PQresStatus(PQresultStatus(result)) == PGRES_TUPLES_OK){
	int r;
	int nrows = PQntuples(result);
	int nfields = PQnfields(result);
	printf("NORR=%d\nNOFR=%d\n", nrows, nfields);
	if (nrows > 0)
	{
		for (r = 0; r < nrows; r++)
		{
			//for(n=0;n<nfields;n++){printf("%s =%s(%d),\n",PQfname(result,n),PQgetvalue(result,r,n),PQgetlength(result,r,n));}
			sprintf(retbuf, "'%s','%s',%s", PQgetvalue(result, r, 0), PQgetvalue(result, r, 1), PQgetvalue(result, r, 2));
		}
	} else if (nrows == 0)
	{
		return STRNULL;
	}
	PQclear(result);
	return retbuf;
	//}
}

/**
 * set data reference to the raw table
 */
int linkRawToData(PGconn *mycon, int full_processed, uint64_t raw_id, uint64_t data_id, void *buffer, size_t size)
{
	char cmd[256] = { 0 };
	int flags = 0;
	switch (size)
	{
		case 34:
			flags = 6;
			break;
		case 48:
			flags = 4;
			break;			
		case 67:
			flags = 2;
			break;			
		case 81:
			flags = 0;
			break;			
		default:
			flags = 7;
	}

	sprintf(cmd, "UPDATE raw SET is_full_processed = '%s', flags = %d WHERE raw_id = %lu", 
		(full_processed > 0 ? "t" : "f"), flags, raw_id
	);
	PGresult *result;
	result = PQexec(mycon, cmd);
	// iridium_log(cmd);
	PQclear(result);
	return 0;
}

void SQL_state(PGconn **mycon)
{
	/*Проверка состояния соединения с БД!!!*/
	//iridium_log("Проверка состояния соединения с БД ... ");

	if (PQstatus(*mycon) == CONNECTION_OK)
	{
		//    printf("Connection to DB ...connected %s\n",glt());
		int sql_out = SQL(*mycon, "select 1;");
		//    sprintf(mess,"Попытка извлечения данных из БД ... %d", sql_out );
		//    iridium_log( mess );
		if (sql_out < 1)
		{
			*mycon = PQconnectdb("host=pgsql.ysn.ru dbname=iridium user=imz password='LC50vbHc'");
			if (PQstatus(*mycon) == CONNECTION_OK)
			{
				printf("RECONNECT to DB state connected %s\n", glt());
				iridium_log("Связь с БД переустановлена ... OK!!!");
			} else
			{
				iridium_log("Невозможно установить связь с БД!!!");
			}
		}
	} else
	{
		printf("Connection to DB ... FAILED %s\n", glt());
		// НЕОБХОДИМО Переподключение к БД!!!
		iridium_log("Нет связи с БД !!!");
	}
}

int SQL(PGconn *mycon, const char *command)
{
	//printf("FUNCT SQL\n");
	int e = 0;
	if (atoi(command) == 250)
	{
		return e;
	} else
	{
		PGresult *result;
		result = PQexec(mycon, command);
		int nr = PQntuples(result);
		int nf = PQnfields(result);
		if (nr && nf)
		{
			e = atoi(PQgetvalue(result, 0, 0));
			PQclear(result);
			//printf("FUNCT SQL NR:%d NF:%d RETURN:%d\n",nr,nf,e);
		} else
		{
			sql_log(PQresultErrorMessage(result), command);
		}
	}
	return e;
}

// Convert bytes to HEX string
// TODO remove static s
char* Dec_HEX(void *in, size_t sz, int tz)
{
	static char s[2048];
	message2PCharClause2(in, sz, SQL_CT_HEX, NULL, NULL, s, sizeof(s), 0, tz, "");
	return s;
}

uint64_t insertRAW (
	PGconn *mycon,
	void *value,
	size_t len,
	char* who,
	int g,
	int tz
)
{
	uint64_t e = 0;
	char command[2048];
	fprintf(stderr, "insertRAW len %" PRIu64 "\n", len);
	char *valHex = Dec_HEX(value, len, tz);

	if (!g)
		sprintf(command, "insert into raw(data, who) values('%s','%s') RETURNING raw_id", valHex, who);
	else
		sprintf(command, "insert into raw(data, who, garbage) values('%s','%s',true) RETURNING raw_id", valHex, who);

	PGresult *result = PQexec(mycon, command);
	int nr = PQntuples(result);
	int nf = PQnfields(result);
	if (nr && nf)
	{
		e = strtoull(PQgetvalue(result, 0, 0), NULL, 10);
		PQclear(result);
	} else
		sql_log(PQresultErrorMessage(result), command);
	return e;
}

uint64_t insertLocation
(
	PGconn *mycon,
	MessageDescription *md,
	void *value,
	size_t len,
	int tz
)
{
	uint64_t e = 0;
	if (md == NULL)
		return 0;
	uint64_t id = md->id;
	PGresult *result;
	char command[2048];
	char json[2048];
	int sz = message2PCharClause2(value, len, SQL_CT_JSON, NULL, md,
		json, sizeof(json), 0, tz, "");
	if (sz <= 0)
	{
		iridium_log("Мал буфер вставки строки локации");
		strcpy(json, "{\"error\": \"no memory\"}");
	}
	md->id = e;
	sprintf(command, "INSERT INTO loc(utc, gpstime, imei, lat, lon, asjson, raw_id, packettype) values( \
		%ld, %ld, '%s', %10.8f, %10.8f, '%s', %" PRIu64 ", %d)  RETURNING raw_id",
		md->utc, md->gpstime + tz, md->imei, md->location.lat, md->location.lon, json, id, md->packettype);
	result = PQexec(mycon, command);
	int nr = PQntuples(result);
	int nf = PQnfields(result);
	if (nr && nf)
	{
		e = strtoull(PQgetvalue(result, 0, 0), NULL, 10);
		PQclear(result);
	} else
		sql_log(PQresultErrorMessage(result), command);
	return e;
}

uint64_t insertGarbage(
	PGconn *mycon,
	void *value,
	size_t len,
	char* who,
	int g,
	int tz
)
{
	uint64_t e = 0;
	PGresult *result;
	char command[2048];

	char *valHex = Dec_HEX(value, len, tz);
	if (!g)
		sprintf(command, "insert into raw(data, who) values('%s','%s') RETURNING raw_id", valHex, who);
	else
		sprintf(command, "insert into raw(data, who, garbage) values('%s','%s',true) RETURNING raw_id", valHex, who);

	result = PQexec(mycon, command);
	int nr = PQntuples(result);
	int nf = PQnfields(result);
	if (nr && nf)
	{
		e = strtoull(PQgetvalue(result, 0, 0), NULL, 10);
		PQclear(result);
	} else
		sql_log(PQresultErrorMessage(result), command);
	return e;
}

char *byte_to_bit_mask(unsigned int byte0)
{
	static char bits[32];
	bzero(bits, 32);
	int z;
	for (z = 31; z >= 0; z--)
		strcat(bits, ((byte0 >> z) & 1) ? "1" : "0");
	return bits;
}

char *byte_to_bit_stat(unsigned int byte0)
{
	//	printf("SIZEOF BYTE0:%d\n",sizeof(byte0));
	unsigned char mask = 1;
	static char bits[8];
	bzero(bits, 8);
	int z;
	for (z = 7; z >= 0; z--)
	{
		strcat(bits, ((byte0 >> z) & mask) ? "1" : "0");
	}
	return bits;
}

/**
 *	Отправка ответа WEB КЛИЕНТУ
 */
void sendReplyUptime(int sock, int sec, int pc, int wp)
{
	char mess[16];
	sprintf(mess, "%u:%u:%u:", sec, pc, wp);
	if (!fork())
	{
		if (send(sock, mess, sizeof(mess), 0) == -1)
			perror("send");
		printf("Send uptime: %s\n", mess);
		close(sock);
		exit(0);
	}
	close(sock);
	while (waitpid(-1, NULL, __WALL) > 0)
	{
	}
}

/**
 * Send text reply to the web server socket
 */
void sendTextSocket(int sock, char *buffer, size_t size)
{
	if (!fork())
	{
		if (send(sock, buffer, size, 0) == -1)
			perror("sendTextSocket");
		else
			printf("Sent text to socket: %s\n", buffer);
		close(sock);
		exit(0);
	}
	close(sock);
	while (waitpid(-1, NULL, __WALL) > 0)
	{
	}
}

char *IRIDIUM_ERR_MSG[] = {
	"Unknown error",
	"Invalid IMEI",
	"Unknown IMEI",
	"Message is too big",
	"Message is too small",
	"Queue full",
	"Message unavaliable",
	"Message violation",
	"Ring disabled",
	"No attach"
};

/**
 * Return socket or invalid socket
 */
int sendNReadConfirm(char *addr, int port, char* buf,  size_t size, void* reply,  size_t replysize) {
	int sock, status;
	if (-1 == (sock = socket(AF_INET, SOCK_STREAM, 0)))
	{
		iridium_log("Sending app command message error: can not open socket");
		return -1;
	}

	struct sockaddr_in ni_addr;
	bzero(&ni_addr, sizeof(ni_addr));
	ni_addr.sin_family = AF_INET;
	ni_addr.sin_addr.s_addr	= inet_addr(addr);
	ni_addr.sin_port = htons(port);
	if (connect(sock, (struct sockaddr *) &ni_addr, sizeof(ni_addr)) == -1)
	{
		iridium_log("Sending app command message error: can not connect to socket");
		close(sock);
		return -2;
	}

	if (size != sendto(sock, buf, size, 0, (struct sockaddr *) &ni_addr, sizeof(ni_addr)))
	{
		iridium_log("Sending app command message error: can not sent to socket");
		close(sock);
		return -3;
	}

	int n = read(sock, reply, replysize);
	int r = -10;
	if ((n < 81) && (n >= 28))
	{
		static char response[81];
		int rsz = message2PCharClause2(reply, replysize, SQL_CT_CONVERT, NULL, NULL, response, sizeof(response), 0, DEF_TIME_ZONE_SECS, "");

		if (rsz > 28)
		{
			IEMTConfirmation* c = (IEMTConfirmation*) &response[6];
			r = c->status;
			char ss[256];
			
			// dump
			char *valHex = Dec_HEX(reply, replysize, DEF_TIME_ZONE_SECS);
			printf("Iridium reply: %s %zu bytes\n", valHex, replysize);
			iridium_log(ss);
			
			if (c->status <= 0) {
				int idx = -c->status;
				if (!((idx >= 1) && (idx <= 9)))
				{
					sprintf(ss, "msgid: %d, idref: %u, Unknown error: %d\n", c->msgid, c->idref, c->status);
				}
				else
				{
					char imei[16];
					strncpy(imei, (char *) &c->imei, 15);
					imei[15] = '\0';
					sprintf(ss, "msgid: %d, idref: %u, IMEI: %s Error %d: %s\n", c->msgid, c->idref, imei, c->status, IRIDIUM_ERR_MSG[idx]);
				}
			}
			else 
			{
				sprintf(ss, "msgid: %d, idref: %u, queue order: %d\n", c->msgid, c->idref, c->status);
			}
			printf("%s", ss);
			iridium_log(ss);
		}
	}
	close(sock);
	return r;
}

/**
 * Parse & send app command message
 * resultcode:IPADDR_FROM:TO_IMEI:cmd:msg
 */
int sendAppCommandMessage(char *addr, int port, 
	const char *from, const char *imei, const char *cmdid, char *cmd, char *pars)
{
	char ss[256];
	char msg[81];
	int sz = appCommands2PChar(msg, sizeof(msg), imei, cmdid, cmd, pars);
	if (sz <= 0)
	{
		sprintf(ss, "Error appCommands2PChar() %s %s\n", cmd, pars);
		printf("%s", ss);
		iridium_log(ss);
		return -1;
	}

	sprintf(ss, "Sending app cmd %s, size: %d\n", Dec_HEX((char *)&msg, sz, DEF_TIME_ZONE_SECS), sz);
	printf("%s", ss);
	iridium_log(ss);

	char confirm[81];
	int queueOrder = sendNReadConfirm(addr, port, (char *) &msg, sz, &confirm, sizeof(confirm));
	sprintf(ss, "Receiving confirmation status: %d\n", queueOrder);
	printf("%s", ss);
	iridium_log(ss);

	return queueOrder;
}

/**
 * Send reply to the sender with send result code
 * resultcode:IPADDR_FROM:TO_IMEI:cmd:msg
 */
void sendReplyMesgText(int sock, int code, char* from, char *imei)
{
	char mess[2048];
	sprintf(mess, "%d:%s:%s", code, from, imei);
	sendTextSocket(sock, mess, strlen(mess));
}

/**
 * Parse packet XLS:REMOTE_ADDR:uname:uid:start:finish
 */
int getXLSParams(char *buffer, size_t size, char **from, char **uname, char **uid, char **start, char **finish, char **imei)
{
	if (!buffer)
		return -1;
	char *prefix = strtok(buffer, MESG_DELIM);
	if (!prefix)
		return -2;
	if (strcmp(prefix, "XLS") != 0)
		return -3;
	*from = strtok(NULL, MESG_DELIM);
	if (!*from)
		return -4;
	*uname = strtok(NULL, MESG_DELIM);
	if (!*uname)
		return -5;
	*uid = strtok(NULL, MESG_DELIM);
	if (!*uid)
		return -6;
	*start = strtok(NULL, MESG_DELIM);
	if (!*start)
		return -7;
	*finish = strtok(NULL, MESG_DELIM);
	if (!*finish)
		return -8;
	*imei = strtok(NULL, MESG_DELIM);
	if (!*imei)
		return -9;
	return 	0;
}

int sendXLS(int fileNumber, const char *path, int sock, int sec, char *buffer, size_t size)
{
	char *from;
	char *uname;
	char *uid;
	char *start;
	char *finish;
	char *imei;
	int r = getXLSParams(buffer, size, &from, &uname, &uid, &start, &finish, &imei);
	pid_t pid = fork();

	if (pid == -1)
		perror("send xls fork error");
	else if (pid > 0) {
		int status;
    	waitpid(pid, &status, 0);
	} else {
		char BINXLSPATH[PATH_MAX + 1];
		char *rp = realpath(path, BINXLSPATH);
		char *rrp = strrchr(rp, '/');
		if (rrp)
			*rrp = 0;
		strcat(rp, (char *) "/mkxls.sh");
		char XLSPATH[PATH_MAX + 1];
		char *fn = XLSPATH;
		sprintf(fn, "/tmp/iridium-xls-%d.tmp", fileNumber); // mktemp(templ);
		char cmd [1024];
		sprintf(cmd, "%s %s %s %s %s \"%s\" %s", rp, uname, uid, start, finish, fn, imei ? imei : "");
// printf("Spawn process %s to create XLS file %d %s: %s.\n", rp, fileNumber, fn, cmd);
		int r = system(cmd);
		if (r != 0) {
			perror("Error execute program to produce XLS file");
			// remove(fn);
			exit(-1);
		}
		int status;
		WIFEXITED(status);

		FILE *fd = fopen(fn, "rb");
		if (!fd) {
			perror("Error open just created XLS temporary file");
			remove(fn);
			exit(-2);
		}
		fseek(fd, 0L, SEEK_END);
		size_t fsize = ftell(fd);
		fseek(fd, 0L, SEEK_SET);

		char ss[255];
  
		size_t chunksize = fsize > 1024 * 1024 ? 1024 * 1024 : fsize;
		char *buffer = malloc(chunksize);
		
		if (buffer)
		{
			sprintf(ss, "Sending XLS %lu bytes in %lu bytes long chunks", fsize, chunksize);
			iridium_log(ss);
			int rd;
			int total = 0;
			
			do {
				rd = fread(buffer, 1, chunksize, fd);
				if (rd > 0) 
				{
					char *p = buffer;
					int cnt = 5;
					int written = 0;
					do 
					{
						int wr = send(sock, p, rd, 0);
						if (wr < 0)
						{
							sprintf(ss, "Error write XLS to socket errno %d\n", errno);
							iridium_log(ss);
							wr = 0;
							break;
						}
						if (wr == 0)
						{
							cnt--;
							if (cnt <= 0)
							{
								sprintf(ss, "Error write XLS to socket after 5 tries: %d\n", wr);
								iridium_log(ss);
								break;
							}
							sleep(1);
							continue;
						}
						written += wr;
						p += wr;
						total += wr;
						if (written >= rd)
							break;
					} while (1);
				}
			} while (rd > 0);
			sprintf(ss, "Sent %d bytes\n", total);
			iridium_log(ss);
			free(buffer);
		}
		else
		{
			sprintf(ss, "Can not send XLS: error allocate buffer %lu bytes\n", chunksize);
			iridium_log(ss);
		}
		
		fclose(fd);
		shutdown(sock, SHUT_RDWR);
		close(sock);
		remove(fn);
		exit(0);
	}
}

/**
 * Parse packet MESSG:1.1.1.1:123456789012345678901234567890:1::
 */
int getSendMessage(char *buffer, size_t size, char **from, char **imei, char **cmd, char **msg)
{
	if ((buffer == NULL) || (size < 47))
		return -1;
	// check MESG:
	char *prefix = strtok(buffer, MESG_DELIM);
	if (prefix == NULL)
		return -2;
	if (strcmp(prefix, MESG_PREFIX) != 0)
		return -3;
	*from = strtok(NULL, MESG_DELIM);
	if (*from == NULL)
		return -4;
	*imei = strtok(NULL, MESG_DELIM);
	if (*imei == NULL)
		return -5;
	*cmd = strtok(NULL, MESG_DELIM);
	if (*cmd == NULL)
		return -6;
	*msg = strtok(NULL, MESG_DELIM);
	if (*msg == NULL)
		return -7;
	return 	(*from && *imei && *cmd && *msg) ? 0 : -7;
}

char* Decode_Date(int y, int dm, int h, int mi, int s)
{
	//printf("DECODE DATE IN %d %d %d %d %d\n",y,dm,h,mi,s);
	//printf("DECODE DATE IN %.2x %.2x %.2x %.2x %.2x\n",y,dm,h,mi,s);
	static char out[30];
	bzero(out, 30);
	int year = (y >> 1) + 2000, mons = ((y * 0x100 + dm) >> 5) & 15, days =
			(dm) & 31;
	//       printf("'%d-%.2d-%.2d %.2d:%.2d:%.2d'",year,mons,days,h,mi,s);
	//if(year&&mons&&days){
	sprintf(out, "'%d-%.2d-%.2d %.2d:%.2d:%.2d'", year, mons, days, h, mi, s);//}else{ sprintf(out,"'NULL'");}
	//        printf("%s\n",out);
	return out;
}

void term_handler(int sig)
{
	printf("Terminating SIG %d\n", sig);
	char mess[160] = { 0 };
	if (sig == 2)
		sprintf(mess, "IRIDIUM %s, SHUTDOWN BY USER INTERRUPT, PID %d, SIGNAL %d", SER_VER, getpid(), sig);
	else if (sig == 15)
		sprintf(mess, "IRIDIUM %s, SHUTDOWN BY SYSTEM INTERRUPT, PID %d, SIGNAL %d", SER_VER, getpid(), sig);
	else
	{
		sprintf(mess, "IRIDIUM %s, SHUTDOWN UNKNOWN, PID %d, SIGNAL %d", SER_VER, getpid(), sig);
	}
	iridium_log(mess);
	exit(EXIT_SUCCESS);
}

/**
 * @return in seconds
 */
static int getDeviceTimeZone
(
	const char *imei
)
{
	char sql[255] = {0};
	sprintf(sql, "SELECT tz FROM device WHERE d_imei = '%s'", imei);
	
	int t = SQL(mycon, sql);
	int tsave = t;
	int ta = labs(t);
	if (ta < 23)
	{
		// hours
		t = t * 3600;
	} else {
		if (ta < 1400)
		{
			// minutes
			t = t * 60;
		}
	}
	char mess[160] = { 0 };
	sprintf(mess, "Device %s time zone %d: %d", imei, tsave, t);
	iridium_log(mess);
	return t;
}

int main(int argc, char **argv)
{
	commandSequence = 1;
	//Установка обработчика сигнала TERM
	struct sigaction sa;
	sa.sa_handler = term_handler;
	sigaction(SIGTERM, &sa, 0);
	sigaction(SIGINT, &sa, 0);
	char mess[60] = { 0 };
	sprintf(mess, "IRIDIUM %s стартует .... PID  %d", SER_VER, getpid());
	iridium_log(mess);
	printf("Server %s is starting... PID %d\n", SER_VER, getpid());

	int sd, newsd = 0, n, packet_count = 0, wrong_packet = 0; /* Слушаем на сокете sd, новое подключение на сокете newsd */
	int xlsFileNumber = 0;

	struct sockaddr_in my_addr, their_addr;/* Серверная адресная информация и Адресная информация запрашивающей стороны (клиента) */

	struct linger ling;
	socklen_t sin_size;

	unsigned char buffer[BufferLength];
	unsigned char out[512];

	/* Создаем сокет, ориентированный на соединение, для домена Интернет */
	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("socket");
		exit(1);
	}
	my_addr.sin_family = AF_INET; /* В порядке байтов хоста */
	my_addr.sin_port = htons(INPORT); /* short, в порядке байтов сети */
	my_addr.sin_addr.s_addr = INADDR_ANY; /* Авто-заполнение IP-адресом серверного сетевого интерфейса */
	bzero(&(my_addr.sin_zero), 8); /* Обнуляем остальную часть struct */
	int opt = 1;
	ling.l_onoff = 1;
	ling.l_linger = 0;
	setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt));
	setsockopt(sd, SOL_SOCKET, SO_LINGER, &ling, sizeof(ling));
	if (bind(sd, (struct sockaddr *) &my_addr, sizeof(struct sockaddr)) == -1)
	{
		perror("bind");
		exit(1);
	}
	/* Связываем только-что созданный сокет с его локальным адресом */
	if (listen(sd, BACKLOG) == -1)
	{
		perror("listen");
		exit(1);
	}
	printf("Listen socket %d\n", INPORT);
	/* Организуем очередь прослушивания сети на порту MYPORT */
	/*Интерфейс к postgres  PGconn *GLOBAL */
	mycon = PQconnectdb("host=pgsql.ysn.ru dbname=iridium user=imz password='LC50vbHc'");

	if (PQstatus(mycon) == CONNECTION_OK)
	{
// 		printf("Connection to DB ... OK\n");
		iridium_log("POSTGRESQL ... OK");
	} else
	{
		printf("Connection to db failed: %s\n", PQerrorMessage(mycon));
		sprintf(mess, "POSTGRESQL ... ERROR !!! %s", PQerrorMessage(mycon));
		iridium_log(mess);
		return EXIT_FAILURE;
	}

	time_t curtime, start_time, data_time;
	struct tm *loctime;
	char buff_1[64];
	start_time = time(NULL);
	loctime = localtime(&start_time);
	sin_size = sizeof(struct sockaddr_in);

	/* Главный цикл accept ()*/

	while (1)
	{
		if ((newsd = accept(sd, (struct sockaddr *) &their_addr, &sin_size)) == -1)
		{
			perror("accept");
			continue; /* Несмотря на неудачу, продолжаем прием*/
		} else
		{
			//    printf("NEW ACCEPT\n");
		}
		curtime = time(NULL);
		bzero(buffer, 82);
		bzero(out, 512);
		char str3[256];

		n = read(newsd, buffer, sizeof(buffer)); //Запись с дескриптора сокета в буфер.

		/*
		// 0123456789012345678901
		// UPTIME:xxx.xxx.xxx.xxx
		// 0x55 + 0x50 + 0x54 + 0x49 + 0x4D + 0x45 // + 0x3A = 468
		// 85+80+84+73+77+69+58
		// MESSG:
		// 4D+45+53+53+47+3A = 441
		// das ist fantastisch!!!
		int ut = buffer[0] + buffer[1] + buffer[2] + buffer[3]
			+ buffer[4] + buffer[5]
			// + buffer[34] + buffer[35] + buffer[36]
			// + buffer[48] + buffer[49] + buffer[50]
			;

		printf("Magic number %d is sum of %x + %x + %x + %x + %x + %x\n", ut, buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]);
		*/
		int ut;
		if (strncmp("UPTIME:", (char *) &buffer[0], 7) == 0)
			ut = UPTIME;
		else if (strncmp("MESSG:", (char *) &buffer[0], 6) == 0)
			ut = SENDMESSAGESUM;
		else if (strncmp("XLS:", (char *) &buffer[0], 4) == 0)
			ut = SENDXLS;
		else
			ut = MESSAGE;

		switch(ut) {
		case SENDXLS:
				// Web service PHP script request
				xlsFileNumber++;
				sendXLS(xlsFileNumber, argv[0], newsd, curtime, (char *)&buffer[0], sizeof(buffer));
				break;
		case UPTIME:
				// Web service PHP script request
				sendReplyUptime(newsd, curtime - start_time, packet_count, wrong_packet);
				break;
		case SENDMESSAGESUM:
				{
					/**
					 * resultcode:IPADDR_FROM:TO_IMEI:cmd:msg
					 */
					int code = -1;
					char* from;
					char *imei;
					char *cmd;
					char *pars;
					char ss[256];
					int r = getSendMessage((char *)&buffer[0], sizeof(buffer), &from, &imei, &cmd, &pars);
					if (r != 0)
					{
						sprintf(ss, "Error getSendMessage(): invalid command or parameters\n");
						iridium_log(ss);
						printf("%s", ss);
						code = -2;
					}
					else
					{
						sprintf(ss, "Message to the Iridium %s:%d gateway: %s, %s, %s, %s\n", NEW_IRIDIUM_MSG_ADDR, IRIDIUM_MSG_PORT, from, imei, cmd, pars);
						iridium_log(ss);
						printf("%s", ss);
						code = sendAppCommandMessage((char*) NEW_IRIDIUM_MSG_ADDR, IRIDIUM_MSG_PORT, from, imei, nextCommandIdentifier(), cmd, pars);
					}

					if (code >= 0)
						sprintf(ss, "sendMessage() success\n");
					else
						sprintf(ss, "Error, sendMessage() return %d\n", code);
					iridium_log(ss);
					printf("%s", ss);
					sendReplyMesgText(newsd, code, from, imei);
				}
				break;
		case MESSAGE:
			{
				if (strcmp(inet_ntoa(their_addr.sin_addr), "84.237.111.10") == 0)
				{
					//    printf("TRUE ADDR %s strcmp %d \n", inet_ntoa(their_addr.sin_addr) , strcmp( inet_ntoa(their_addr.sin_addr) , "84.237.111.10" )  );
					close(newsd);
					unsigned char mess1[30];
					bzero(mess1, 30);
					int z = 0;
					for (z = 51; z < 81; z++)
					{
						mess1[z - 51] = buffer[z]; /* printf("Z %d %.2x ",z, mess1[ z-51 ]);*/
					}
					// printf("\nRight direction\n%s\n ",Dec_HEX(mess1));
					int ni_socketd, status;//        printf("Fork OK , my pid %d, parent pid %d\n",getpid(),getppid());//        printf("Doing connect () \n");
					switch (fork())
					{
					case -1:
						perror("fork");
					case 0:
						if (-1 == (ni_socketd = socket(AF_INET, SOCK_STREAM, 0)))
						{
							perror("socket");
							exit(1);
						}//        printf("NI SOCKET CREATED %d \n", ni_socketd );
						struct sockaddr_in ni_addr;
						bzero(&ni_addr, sizeof(ni_addr));
						ni_addr.sin_family = AF_INET; /* В порядке байтов хоста */
						ni_addr.sin_addr.s_addr	= inet_addr("91.229.154.8");//                ni_addr.sin_addr.s_addr=inet_addr("84.237.104.199");
						ni_addr.sin_port = htons(OUTPORT);
						if (connect(ni_socketd, (struct sockaddr *) &ni_addr, sizeof(ni_addr)) == -1)
						{
							perror("connect()");
							exit(1);
						}//        printf("Connect .. OK\n");

						if (30 != sendto(ni_socketd, mess1, 30, 0, (struct sockaddr *) &ni_addr, sizeof(ni_addr)))
						{
							iridium_log("Sending to nicenter ERROR");
						}
						close(ni_socketd);
						exit(0);
					}
					if (wait(&status) == -1)
					{
						perror("wait");
						return EXIT_FAILURE;
					}
					if (WIFEXITED(status))
					{
						if (WEXITSTATUS(status))
						{
							iridium_log("Sending to nicenter ERROR");
							printf("Child terminated no normally with exit code %i\n",
							WEXITSTATUS(status));
						}
					}
					if (WIFSIGNALED(status))
						printf("Child was terminated by a signal #%i\n", WTERMSIG(status));
					if (WCOREDUMP(status))
						printf("Child dumped core\n");
					if (WIFSTOPPED(status))
						printf("Child was stopped by a signal #%i\n", WSTOPSIG(status));
					continue;
				}

				packet_count++;
				iridium_log("Новое сообщение");
				// Проверка соединения с БД
				SQL_state(&mycon);
				iridium_log("Соединение с БД проверено");
				// Разбор пакета по полям
				data_time = time(NULL);
				loctime = localtime(&data_time);
				strftime(buff_1, 64, "%d-%m-%Y %H:%M:%S", loctime);
				sprintf(str3, "%s from %s\n", buff_1, inet_ntoa(their_addr.sin_addr));
				iridium_log("Разбор пакета по полям 1");
				unsigned char p = buffer[0];
				unsigned short ol = buffer[1] * 0x100 + buffer[2];
				unsigned char mo_h = buffer[3];
				unsigned short mol = buffer[4] * 0x100 + buffer[5];
				unsigned int cdrr_autoid = (unsigned int) buffer[6]
						* 0x1000000 + (unsigned int) buffer[7]
						* 0x10000 + (unsigned int) buffer[8]
						* 0x100 + buffer[9];

				char imei[16];
				int z = 0;
				for (z = 10; z <= 24; z++)
				{
					imei[z - 10] = buffer[z];
				}
				imei[15] = '\0';

				unsigned char s_stat = buffer[25];
				unsigned short momsn = buffer[26] * 0x100 + buffer[27];
				unsigned short mtmsn = buffer[28] * 0x100 + buffer[29];
				unsigned int stime = (unsigned int) buffer[30]
						* 0x1000000 + (unsigned int) buffer[31]
						* 0x10000 + (unsigned int) buffer[32]
						* 0x100 + buffer[33];
				char mo_pl_id = buffer[34];
				unsigned short mo_len = buffer[35] * 0x100 + buffer[36];
				unsigned char direction = buffer[37];
				unsigned int lat = buffer[38];
				unsigned int p_lat = buffer[39] * 0x100 + buffer[40];
				unsigned int lon = buffer[41];
				unsigned int p_lon = buffer[42] * 0x100 + buffer[43];
				unsigned int ceprad = (unsigned long) buffer[44]
						* 0x1000000 + buffer[45] * 0x10000
						+ buffer[46] * 0x100 + buffer[47];
				char pl_id = buffer[48];
				unsigned short pl_len = buffer[49] * 0x100 + buffer[50];
				
				char pl_type = buffer[51];

				double data_payload_1 = GetDoubleM(buffer[52], buffer[53], buffer[54], buffer[55], buffer[56], buffer[57], buffer[58], buffer[59]);//Только в радианах или метрах в секунду!!!
				double data_payload_2 =	GetDoubleM(buffer[60], buffer[61], buffer[62], buffer[63], buffer[64], buffer[65], buffer[66], buffer[67]);//Только в радианах!!!
				double data_payload_3 =	GetDoubleM(buffer[68], buffer[69], buffer[70], buffer[71], buffer[72], buffer[73], buffer[74], buffer[75]);//Только в метрах или метрох в секунду!!!
				char* da = Decode_Date(buffer[76], buffer[77], buffer[78], buffer[79], buffer[80]);

				char sqlcmd[4096] = { 0 };
				int max_data_seq = 0;
				uint64_t last_inserted_loc = 0,
				last_inserted_data = 0;
				iridium_log("Запись пакета в файл");
				raw_to_file(buffer, n, str3); //Записи бинарных данных в файл
				iridium_log("Запись пакета в порт");
				raw_to_port(buffer, n, "0.0.0.0", 50052); //

				iridium_log("Начало записи пакета в таблицу сырых данных");
				int tz = getDeviceTimeZone(imei);
				// TODO
				// Запись пакета в таблицу сырых данных
				uint64_t last_inserted_raw = insertRAW(mycon, buffer, n, inet_ntoa(their_addr.sin_addr), 0, tz);// Это будет id который пойдет в DATA (ссылка на пакет)
				if (last_inserted_raw <= 0)
				{
					// Пакет не записан в таблицу сырых данных !!!
					printf("    ... ERROR         \n");
					wrong_packet++;
					iridium_log("Пакет не записан в таблицу сырых данных, возможно потеряно соединение с базой!");
					// sprintf(buffer, "SELECT raw_id FROM raw ORDER BY raw_id DESC LIMIT 1"); // ?!!
				}
				else
				{
					printf("    ... OK\n");
					iridium_log("Пакет записан в таблицу сырых данных!");
					MessageDescription ret;
					// insert last location
					ret.id = last_inserted_raw;
					last_inserted_loc = insertLocation(mycon, &ret, buffer, n, tz);
					// parse message to SQL INSERT
					int sz = message2PCharClause2(buffer, n, SQL_CT_INSERT, ALL_COLUMNS, &ret,
						sqlcmd, sizeof(sqlcmd), last_inserted_raw, tz, "");
					if (sz <= 0)
						iridium_log("Мал буфер");
					else
						strcat(sqlcmd, " RETURNING data_pk");

					// Запись команды SQL и разобранного пакета зависит от типа сообщения.
					switch (pl_type)
					{
					case 0x01:
						// КООРДИНАТЫ
						printf("MESSAGE 01 LAT LON H \t %f %f %f %s\n", data_payload_1
							* 180 / M_PI/*Приходят в радианах, это перевод в градусы и далее по тексту также*/, data_payload_2
							* 180 / M_PI, data_payload_3, str3);
						sprintf(sqlcmd, "insert into data(protver,protsize,direct_id,direct_size,cdrref,imei,direct_stat,momsn, mtmsn, \
								direct_date, loc_id,loc_size,loc_dir,latitude,longitude, cepradius, payload_id, payload_size, \
								payload_type,lat,lon,h,id,packetdate,lat1) values(\
								%.2x, '%u', '%.2x', '%u', '%u', '%s', '%.2x', '%u', '%u',(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + %u  * INTERVAL '1 second'), '%.2x', '%u', '%.2x', '%u.%.5u', '%u.%.5u', '%u', '%.2x', '%u', '%.2x', '%.50f', '%.50f', '%.50f',%" PRIu64 ", %s, '%.50f') RETURNING data_pk;\n",
								p, ol, mo_h, mol, cdrr_autoid, imei, s_stat, momsn, mtmsn, stime, mo_pl_id, mo_len, direction, lat, p_lat, lon, p_lon, ceprad, pl_id, pl_len, pl_type, data_payload_1
								* 180 / M_PI, data_payload_2
								* 180 / M_PI, data_payload_3,
								last_inserted_raw,
								da,
								data_payload_1 * 180 / M_PI);
						iridium_log("Пакет с координатами ");
						break;
					case 0x02:
						// ФИЛЬТРОВАННЫЕ КООРДИНАТЫ
						printf("MESSAGE 02 LATF LONF HF \t %f %f %f %s\n ", data_payload_1
								* 180 / M_PI, data_payload_2
								* 180 / M_PI, data_payload_3, str3);
						sprintf(sqlcmd, "insert into data(protver,protsize,direct_id,direct_size,cdrref,imei,direct_stat,momsn,mtmsn, direct_date, loc_id,loc_size,loc_dir,latitude,longitude,cepradius,payload_id,payload_size,payload_type,latf,lonf,hf,id,packetdate) \
								values(%.2x, '%u', '%.2x', '%u', '%u', '%s', '%.2x', '%u', '%u',(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + %u  * INTERVAL '1 second'), \
								'%.2x', '%u', '%.2x', '%u.%.5u', '%u.%.5u', '%u', '%.2x', '%u', '%.2x', '%.50f', '%.50f', '%.50f',%" PRIu64 ", %s) RETURNING data_pk",
								p, ol, mo_h, mol, cdrr_autoid, imei, s_stat, momsn, mtmsn, stime, mo_pl_id, mo_len, direction, lat, p_lat, lon, p_lon, ceprad, pl_id, pl_len, pl_type,
								data_payload_1 * 180 / M_PI,
								data_payload_2 * 180 / M_PI,
								data_payload_3,
								last_inserted_raw, da);
						iridium_log("Пакет с фильтрованными координатами");
						break;
					case 0x03:
						// МОДУЛЬ СКОРОСТИ
						printf("MESSAGE 03 VE AZ VH \t %f %f %f %s\n", data_payload_1, data_payload_2
								* 180 / M_PI, data_payload_3, str3);
						sprintf(sqlcmd, "insert into data(protver,protsize,direct_id,direct_size,cdrref,imei,direct_stat,momsn,mtmsn, direct_date, loc_id,loc_size,loc_dir,latitude,longitude,cepradius,payload_id,payload_size,payload_type,ve,az,vh,id,packetdate) \
								values(%.2x, '%u', '%.2x', '%u', '%u', '%s', '%.2x', '%u', '%u',(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + %u  * INTERVAL '1 second'), '%.2x', '%u', '%.2x', '%u.%.5u', '%u.%.5u', '%u', '%.2x', '%u', '%.2x', '%.50f', '%.50f', '%.50f',%" PRIu64 ",%s) RETURNING data_pk;\n",
								p, ol, mo_h, mol, cdrr_autoid, imei, s_stat, momsn, mtmsn, stime, mo_pl_id, mo_len, direction, lat, p_lat, lon, p_lon, ceprad, pl_id, pl_len, pl_type, data_payload_1, data_payload_2
								* 180 / M_PI, data_payload_3, last_inserted_raw, da);
						iridium_log("Пакет от модуля скорости");
						break;
					case 0x04:
						// ФИЛЬТРОВАННЫЙ МОДУЛЬ СКОРОСТИ
						printf("MESSAGE 04 VEF AZF VHF \t %f %f %f %s\n ", data_payload_1, data_payload_2
								* 180 / M_PI, data_payload_3, str3);
						sprintf(sqlcmd, "insert into data(protver,protsize,direct_id,direct_size,cdrref,imei,direct_stat,momsn,mtmsn, direct_date, loc_id,loc_size,loc_dir,latitude,longitude,cepradius,payload_id,payload_size,payload_type,vef,azf,vhf,id,packetdate) \
								values(%.2x, '%u', '%.2x', '%u', '%u', '%s', '%.2x', '%u', '%u',(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + %u  * INTERVAL '1 second'), '%.2x', '%u', '%.2x', '%u.%.5u', '%u.%.5u', '%u', '%.2x', '%u', '%.2x', '%.50f', '%.50f', '%.50f',%" PRIu64 ",%s) RETURNING data_pk;\n",
								p, ol, mo_h, mol, cdrr_autoid, imei, s_stat, momsn, mtmsn, stime, mo_pl_id, mo_len, direction, lat, p_lat, lon, p_lon, ceprad, pl_id, pl_len, pl_type, data_payload_1, data_payload_2
								* 180 / M_PI, data_payload_3, last_inserted_raw, da);
						iridium_log("Пакет от модуля скорости фильтрованные данные");
						break;
					case 0x08:
						iridium_log("Пакет от животного");
						break;
					case 0x10:
						// ФЛАГИ и СТАТУСЫ сервисное сообщение
						{
						unsigned int mch =
							(unsigned char) buffer[52] * 0x1000000
							+ (unsigned char) buffer[53] * 0x10000
							+ (unsigned char) buffer[54] * 0x100
							+ buffer[55];
						unsigned int gdop_h =
							(unsigned char) buffer[56] * 0x1000000
							+ (unsigned char) buffer[57] * 0x10000
							+ (unsigned char) buffer[58] * 0x100
							+ buffer[59];
						unsigned int pdop_h =
							(unsigned char) buffer[60] * 0x1000000
							+ (unsigned char) buffer[61] * 0x10000
							+ (unsigned char) buffer[62] * 0x100
							+ buffer[63];
						float gdop = GetSingleM(buffer[56], buffer[57], buffer[58], buffer[59]);
						float pdop = GetSingleM(buffer[60], buffer[61], buffer[62], buffer[63]);
						int v_h = buffer[64];
						float v = buffer[64] / 10.;
						int time_ok = atoi((buffer[65] >> 1)
								& 1 ? "1" : "0");
						int gps_ok = atoi(buffer[65] & 1 ? "1"
								: "0");
						printf("MESSAGE 10 STATUS %s\n", str3);
						printf("MASKCHANNEL:%.8x\t--> %s\n", mch, byte_to_bit_mask(mch));
						printf("GDOP:%.8x\t--> %E\n", gdop_h, gdop);
						printf("PDOP:%.8x\t--> %E\n", pdop_h, pdop);
						printf("Voltage:%.2x\t--> %.1f\n", v_h, v);
						printf("STATUS:%.2x\t--> Time %s GPS  %s\n", buffer[65], time_ok
								? "OK"
								: "ER", gps_ok
								? "OK"
								: "ER");
						sprintf(sqlcmd, "insert into data(protver,protsize,direct_id,direct_size,cdrref,imei,direct_stat,momsn,mtmsn, direct_date,loc_id,loc_size,loc_dir,latitude,longitude,cepradius,payload_id,payload_size,payload_type,mch,mch_b,gdop,gdop_h,pdop,pdop_h,v,v_h,status,time_ok,gps_ok,id,packetdate) values(%.2x, '%u', '%.2x', '%u', '%u', '%s', '%.2x', '%u', '%u',(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + %u  * INTERVAL '1 second'), '%.2x', '%u', '%.2x', '%u.%.5u', '%u.%.5u', '%u', '%.2x', '%u', '%.2x','%.8x', '%s', '%E', '%.8x','%E','%.8x',%.1f,'%.2x','%s','%s','%s',%" PRIu64 ",%s) RETURNING data_pk;\n",
								p, ol, mo_h, mol, cdrr_autoid, imei, s_stat, momsn, mtmsn, stime, mo_pl_id, mo_len, direction, lat, p_lat, lon, p_lon, ceprad, pl_id, pl_len, pl_type, mch, byte_to_bit_mask(mch), gdop, gdop_h, pdop, pdop_h, v, v_h, byte_to_bit_stat(buffer[65]), time_ok
								? "OK" : "ER", gps_ok
								? "OK" : "ER", last_inserted_raw, da);
						iridium_log("Пакет со сотатусами и флагами");
						}
						break;

					case 0x11:
						// ТЕКСТОВОЕ СООБЩЕНИЕ
						{
						char text_ns = buffer[52];
						char text_mess[20];
						int z = 0;
						for (z = 53; z < 72; z++)
						{
							text_mess[z - 53] = buffer[z];
						}
						char
							text_reserv = (unsigned char) buffer[73] * 0x10000
								+ (unsigned char) buffer[74] * 0x100
								+ buffer[75];
						printf("MESSAGE 11 TEXT %s\n", str3);
						//    sprintf(sqlcmd,"%d",250);
						sprintf(sqlcmd, "insert into data(protver,protsize,direct_id,direct_size,cdrref,imei,direct_stat,momsn,mtmsn, direct_date,loc_id,loc_size,loc_dir,latitude,longitude,cepradius,payload_id,payload_size,payload_type,text_ns,text_message,text_reserv, id, packetdate ) \
								values(%.2x, '%u', '%.2x', '%u', '%u', '%s', '%.2x', '%u', '%u',(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + %u  * INTERVAL '1 second'), '%.2x', '%u', '%.2x', '%u.%.5u', '%u.%.5u', '%u', '%.2x', '%u', '%.2x','%.2x', '%s', '%.6x',%" PRIu64 ", %s) RETURNING data_pk;\n",
								p, ol, mo_h, mol, cdrr_autoid, imei, s_stat, momsn, mtmsn, stime, mo_pl_id, mo_len, direction, lat, p_lat, lon, p_lon, ceprad, pl_id, pl_len, pl_type, text_ns, text_mess, text_reserv, last_inserted_raw, da);
						iridium_log("Пакет с текстовым сообщением");
						}
						break;
					default:
						// НЕИЗВЕСТНЫЙ ТИП СООБЩЕНИЯ
						printf("UNKNOWN MESSAGE %s\n", glt());
						if (strlen(sqlcmd) == 0)
							sprintf(sqlcmd, "%d", 250);
						wrong_packet++;
						iridium_log("Пакет с неизвестным типом сообщения или неполным набором полей");
					};

					last_inserted_data = SQL(mycon, sqlcmd);
					max_data_seq = SQL(mycon, "select max(data_pk) from data;");
					printf("Insert into DATA LAST_INSERTED_ID:%" PRIu64 " MAX_DATA_ID:%d",
						last_inserted_data, max_data_seq);

					linkRawToData(mycon, last_inserted_data > 0, last_inserted_raw, last_inserted_data, buffer, n);
					if (last_inserted_data > 0)	//  && last_inserted_data == max_data_seq
					{
						printf("    ... OK\n");
						iridium_log("Пакет записан в таблицу обработанных данных!");
						iridium_log(sqlcmd);

					} else
					{
						wrong_packet++;
						printf("              ... ERROR!\n");
						iridium_log(sqlcmd);
						iridium_log("Пакет НЕ записан в таблицу обработанных данных (нет локации, флаги не отмечены поэтому)");
					}
				}
			}
			break;
		default:
			{
				// МУСОР НАС ТОЖЕ ИНТЕРЕСУЕТ =)
				char str_garb[1024];
				sprintf(str_garb, "***\n\n%s\n\n***In time: %s from %s\n\n", buffer, glt(), inet_ntoa(their_addr.sin_addr));
				garb_to_file(str_garb);
				wrong_packet++;
				//printf("GARBAGE:%d %s\n",wrong_packet,buffer);
				iridium_log("Пакет НЕ соответствует протоколу IRIDIUM!!!");
				if (insertGarbage(mycon, buffer, n, inet_ntoa(their_addr.sin_addr), 1, DEF_TIME_ZONE_SECS) > 0)
					iridium_log("Пакет записан в таблицу сырых данных, с пометкой GARBAGE!!!");
				// Это будет id который пойдет в DATA (ссылка на пакет)
				sprintf(str_garb, "%s In time: %s from %s\n\n", Dec_HEX(buffer, n,DEF_TIME_ZONE_SECS), glt(), inet_ntoa(their_addr.sin_addr));
				iridium_log(str_garb);
			}
			break;
		}
		close(newsd);
		if (n < 0)
			perror("ERROR reading from socket");
	}
	// Завершаем процесс сервера
}

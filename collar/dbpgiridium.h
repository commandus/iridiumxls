/*
 * dbpgiridium.h
 *
 *  Created on: 05.11.2015
 *      Author: andrei
 */

#ifndef DBPGIRIDIUM_H_
#define DBPGIRIDIUM_H_

#include "iridiumpacket.h"

typedef enum {
	SQL_CT_COLNAMES = 0,
	SQL_CT_VALUES = 1,
	SQL_CT_SELECT = 2,
	SQL_CT_INSERT = 3,
	SQL_CT_HEX = 4,
	SQL_CT_TEXT = 5,
	SQL_CT_JSON = 6,
	SQL_CT_CONVERT = 7	// convert message from network big-endian to little-endian
} SQL_CLAUSE_TYPE;

/*
 *  Return to result allocated buffer with values as string
 *  This call is destructive!
 *  style:
 *  	SQL_CT_COLNAMES (0)- column names
 *  	SQL_CT_VALUES   (1)- values
 *  	SQL_CT_SELECT   (2)- select clause
 *  	SQL_CT_INSERT   (3)- insert clause
 *	SQL_CT_HEX      (4)- hex
 *	SQL_CT_TEXT	(5)- as string
 *	SQL_CT_JSON	(6)- as JSON
 *
 *  ret can be NULL, otherwise MUST have 16 bytes!
 *  flags
 *  	1- realloc
 *  Usage:
 *      #include "iridiumpacketattr.h"
 *      raw_id = ...
 *   	int columns[] = {
 *			DATA_PROTVER, DATA_PROTSIZE, DATA_DIRECT_ID, DATA_DIRECT_SIZE,
 *			DATA_CDRREF, DATA_IMEI, DATA_DIRECT_STAT, DATA_MOMSN, DATA_MTMSN,
 *			DATA_DIRECT_DATE, DATA_LOC_ID, DATA_LOC_SIZE, DATA_LOC_DIR,
 *			DATA_LATITUDE, DATA_LONGITUDE, DATA_CEPRADIUS, DATA_PAYLOAD_ID,
 *			DATA_PAYLOAD_SIZE, DATA_PAYLOAD_TYPE,
 *			DATA_LATF, DATA_LONF, DATA_HF, DATA_ID, DATA_PACKETDATE,
 *			DATA_TEMPERATURE, DATA_TEMPERATUREIN, DATA_TEMPERATUREOUT
 *			DATA_END};
 *	// get size to allocate
 *	int sz = message2PCharClause(NULL, 0, SQL_CT_INSERT, columns,
 *		bb.data(), bb.size(), raw_id);
 *
 */
CALLC int message2PCharClause(void *value, size_t size, int style, int *columns,
	MessageDescription* ret, void *result, size_t dsz, int id, int tz,
	const char *timestamp
);

/**
 * safe version
 */
CALLC int message2PCharClause2(void *value, size_t size, int style, int *columns,
	MessageDescription* ret, void *result, size_t dsz, int id, int tz,
	const char *timestamp
);


/**
 * Parse app command message, '|' delimited.
 * Parameters:
 * 	imei: 15 characters and \0
 * 	commandidentifier 4 chars.
 * 	value- char[81]
 * 	size = 81
 * 	cmds:
 *		cmd1|cmd2|
 *	pars:
 *		cmd1par1:cmd1par2|cmd2par1|
 * Note:
 *	If first parameter(s) empty and others not, set ';' delimiter
 *	to avoid skipping empty paramaters (strok() trimming).
 */
CALLC int appCommands2PChar(void *value, size_t size,
		const char *imei, const char *commandidentifier,
		char *cmds, char* pars);

#endif /* DBPGIRIDIUM_H_ */

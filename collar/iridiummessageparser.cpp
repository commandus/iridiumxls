/*
 * iridiummessageparser.cpp
 *
 *  Created on: 03.11.2015
 *      Author: andrei
 */
#include <sstream>
#include <string.h>
#include "iridiummessageparser.h"
#include "dump.h"

int iridiummessageparsebuffer(void *data, size_t size, int* packetid, packet_t **result, int tz)
{
	TIridiumMessage m(data, size, tz);
	TIEPayload *p = m.getPayload();
	if (p == NULL)
		return -1;
	*packetid = p->getId();
	packet_t *ppckt = p->getPayload();
	if (ppckt == NULL)
		return -1;
	if (result != NULL)
		*result = ppckt;
	size_t sz = p->getSize() + 3;
	if (sz > size)
		return -1;
	return (int) sz;
}

/**
 * Return hex formatted data.
 * ret can be NULL
 * Return size of buffer
 * insert into raw(data, who, garbage) values('%s', '%s', true) RETURNING raw_id", data, who);}
 */
int iridiummessage2hex(void *data, size_t size, char* ret, size_t retsize)
{
	std::stringstream ss;
	bufferPrintHex(ss, data, size);
	std::string s(ss.str());
	size_t sz = s.length();
	if (ret == NULL)
		return (int) sz + 1;
	if (retsize > sz)
	{
		memmove(ret, s.c_str(), sz);
		ret[sz] = 0;
	}
	return (int) sz + 1;
}

/*
 * geolocation.h
 *
 *  Created on: 30.10.2015
 *      Author: andrei
 */

#ifndef GEOLOCATION_H_
#define GEOLOCATION_H_

#include "iridiumpacket.h"
#include <map>

#define PRECISION       8

/**
  *	GPS outputs geo location coordinate using radians, gpsc outputs using polar coordinates
  *	\sa GeoLocation
  */
typedef enum {
	GC_RADIANS = 0,		//<	radian
	RM_DEGREES = 1		//<	polar
} geoCoordType;

/**
  *	geo location  does not keep measure unit. You must to know what coordinate system is used.
  *	2 extra constructors translate from radian to polar degrees coordinates.
  *	\sa	geoCoordType
  */
class GeoLocation
{
public:
	double lat;
	double lon;
	double alt;
	uint8_t hdop;
	uint8_t pdop;
	bool hasAlt;
	GeoLocation() : lat(.0), lon(.0), alt(.0), hdop(0), pdop(0), hasAlt(false) {};
	GeoLocation(double alat, double alon, double aalt) : lat(alat), lon(alon), alt(aalt), hdop(0), pdop(0), hasAlt(true) {};
	GeoLocation(double alat, double alon) : lat(alat), lon(alon), alt(.0), hdop(0), pdop(0), hasAlt(false) {};
	GeoLocation(gps_coord_t *value);
	GeoLocation(geo3 *value);

	/// translate coordinate system
	GeoLocation(geoCoordType toCoord, double alat, double alon);
	GeoLocation(geoCoordType toCoord, const GeoLocation& value);
	std::string toString();
	std::string toJson();
	void toStrings(std::map <std::string, std::string> &map);
	void putGps_coord_t(gps_coord_t *r);
	void putGeo3(geo3 *r);
	void putGeo(geo *r);

};

#endif /* GEOLOCATION_H_ */

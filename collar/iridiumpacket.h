/*
 * @file iridiumpacket.h
 *
 *  Created on: 22.10.2015
 *      Author: andrei
 * 
 * 
 * 
Iridium packet
------------------------------------------------------------------------------------------------------PTgfSV------------GC------------------bbTTr2FFkkkk----T5----
01004e01001c86cbdf1133303032333430363930303639383000001700005a897ca903000b003e043781a3950000000202001e08900200154600000030002b0d130000300000228a008708ec24520d1010
01004e01001c86ec5a9233303032333430363930303639383000001800005a89d36703000b003e08f68195cb0000000202001e08900b3e019513000031812b7f160000320006228a00870dda2272131a15

Payload only
PTgfSV------------GC------------------bbTTr2FFkkkk----T5----
08900b3e019513000031812b7f160000320006228a00870dda2272131a15

PT  packettype (8)
gf  GPS flags 
	 gpsolddata	GPS old data, GPS not read
	 gpsencoded 	GPS data encoded
	 gpsfrommemory	data got from the memory
	 gpsnoformat	memory is not formatted
	 gpsnosats	No visible GPS salellites
	 gpsbadhdop	GPS HDOP = 99 or > 50
	 gpstime		1 бит 1-0  GPS.valid=0x01 или 0;     GPS.time_valid=0b0000 0010 или 0;
	 gpsnavdata	1 бит 1-0  GPS.valid=0x01 или 0;     GPS.time_valid=0b0000 0010 или 0;
SV   sats            кол. видимых спутников
GC   gps_coord        16 байт
bb   bat             бортовое напряжение fixed point (32=3.2 вольта) (от 2.0 до 4.0)
	 alarmlow         < 2V
	 alarmhigh        > 4V
TT   temperature     температура
r2   r2              номер пакета?? - он же индекс в таблице кодировки
FF   failurepower    device status power loss
	 failureeep      EEPROM failure
	 failureclock    clock not responding
	 failurecable    MAC not responding
	 failureint0     clock int failure
	 failurewatchdog software failure
	 failurenoise    blocks in memory found
	 failureworking  device was
kkkk key             packet_key
T5   time5           time 5 bytes

 */

#ifndef IRIDIUMPACKET_H_
#define IRIDIUMPACKET_H_

#include <stdint.h>
#include <time.h>
#include <sys/types.h>
#include <string.h>

#ifndef _MSC_VER
#define TMSIZE sizeof(struct tm)
#define localtime_s(tm, time) memmove(tm, localtime(time), TMSIZE)
#endif

///	Caution: pi = 3.1415926535898 (see ICDGPS-200)
#define	GPS_PI	3.1415926535898
// Maximum text message size
#define	SIZETEXTCHAR 20
#ifdef _MSC_VER
#define ALIGN	__declspec(align(1))
#define PACKED	
#else
#define ALIGN	
#define PACKED	__attribute__((aligned(1), packed))
#endif


// 01004e01001c485b59b63330303233343031303435313433300048d90000530ed2e303000b003de327818cc10000004a
// 02 001e 014e293ff1209f77a91bfc4002a157eb1a3fc440560000e7261c1105350d
//
// 01 004e
// 01 001c 485b59b633 3030323334 3031303435 3134333000 48d9000053 0ed2e3
// 03 000b 003de32781 8cc1000000 4a
// 02 001e 014e293ff1209f77a91bfc4002a157eb1a3fc440560000e7261c1105350d

#ifdef _MSC_VER
#pragma pack(push,1)
#endif

typedef ALIGN struct {
	uint8_t id;	// Iridium version or 1- I/O, 2- payload, 3- location
	uint16_t size;
} PACKED InfoElementHeader;

typedef ALIGN struct {
	InfoElementHeader elementheader;
#ifdef _MSC_VER
	uint8_t elementdata[1];				// AI: was [0] to MS VS be happy
#else
	uint8_t elementdata[0];				// AI: was [0] to MS VS be happy
#endif
} PACKED InfoElement;

#ifdef _MSC_VER
#pragma warning(disable : 4200)
#endif
typedef ALIGN struct {
	InfoElementHeader packetheader;
	InfoElement element[0];
} PACKED IridiumPacket;

typedef enum {
	IE_NONE = 0,		///< No element present
	IE_IO = 1,			///< MO Direct Header IO
	IE_PAYLOAD = 2,		///< MO payload element
	IE_LOCATION = 3,	///< MO location
	IE_MT_IO = 4,		///< MT direct IP header (destination IMEI)
	IE_MT_PAYLOAD = 5,	///< MT payload
	IE_MT_CONFIRM = 6	///< MT confirmation
} IridiumInfoElementType;

#define ELEMENTTYPE_COUNT	7

typedef enum {
	IOS_OK = 0,			///< I/O status is fine
	IOS_TIMEOUT = 10,	///< I/O operation time out
	IOS_NO_LINK = 13	///< I/O no link
} IOStatus;

typedef ALIGN struct {
	uint8_t b[15];
} PACKED IMEI;

// IE_IO 0x01, 28
typedef ALIGN struct {
	uint32_t cdrref;	///< Iridium message identifier					4
	IMEI imei;			///< see IMEI							15
	uint8_t status;		///< see IOStatus						1
	uint16_t sentno;	///< momsn modem sent out packet number				2
	uint16_t recvno;	///< mtmsn modem received out packet number. Not used		2
	uint32_t recvtime;	///< stime Unix epoch time, seconds since 1970			4
} PACKED IEIOHeader;

// IE_MT mobile termination direct ip header 0x41, 21
typedef ALIGN struct {
	char msgid[4];		///< unique client message identifier				4
	IMEI imei;			///< see IMEI							15
	uint16_t flags;		///< disposition flags 						2
						/// 1- delete all MT payloads in queue
						/// 2- send ring alert - with no associated payload
} PACKED IEMTIOHeader;

// MT Confirmation Message header 0x44, 25
typedef ALIGN struct {
	uint32_t msgid;		///< unique client message identifier				4
	IMEI imei;			///< see IMEI							15
	uint32_t idref;		///< auto id reference						4
	int16_t status;		///< 1-50: order in queue 0- no payload or error	(ERR_MT_...)	2
} PACKED IEMTConfirmation;

#define ERR_MT_INVALID_IMEI	-1
#define ERR_MT_UNKNOWN_IMEI	-2
#define ERR_MT_TOO_BIG		-3
#define ERR_MT_TOO_SMALL	-4
#define ERR_MT_QUEUE_FULL	-5
#define ERR_MT_UNAVAIL		-6
#define ERR_MT_VIOLATION	-7
#define ERR_MT_RING_DISABLED	-8
#define ERR_MT_NO_ATTACH	-9

/**
 * MO Location data IE_LOCATION 0x03 11 bytes long
 * Dir byte
 * Bits 0-3 reserved
 * Bits 4 5 format code
 * Bit 6 0- North, 1- South
 * Bit 7 0- East, 1- West
 */
typedef ALIGN struct {
	uint8_t direwi : 1;
	uint8_t dirnsi : 1;
	uint8_t dirformat : 2;
	uint8_t dirreserved : 4;
	uint8_t lat;		///< 1 Latitude
	uint16_t lat1000;	///< 2 1/1000 latitude
	uint8_t lon;		///< 4 Longitude
	uint16_t lon1000;	///< 5 1/1000 longitude
	uint32_t cepradius;	///< 7 CEP radius: 80% probability radius
} PACKED IELocation;

// 5 bytes time
// 2 байта год/месяц/день
//	7 бит год           0b xxxx xxx0
//	4 бита месяц        0b x xxx0 0000
//	5 бит день          0b 000x xxxx
typedef ALIGN struct {
	uint16_t day: 5;
	uint16_t month: 4;
	uint16_t year: 7;

	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} PACKED time5;

typedef ALIGN struct //16 bytes  - int key наложить??
{
	uint8_t latitude_g;		// 0	    [3](0-90)	добавить бит N or S 0х80 (0х80 - данные корректны)
	uint8_t latitude_m;		// 1        [4](0-60)	или добавить бит N or S 0х80, E or W (East or West) 0х40
	//unsigned long int latitude_s;       //3  (6 bytes (48 bit) число до 281 474 976 710 656) size=32
	uint16_t latitude_s;	// 2-3      [5-6] 2 байта
	//   unsigned long ?? size =   long int 32 -2147483648 to 2147483647    unsigned long int size=32 0 to 4 294 967 295
	uint16_t latitude_s1;	// 4-5      [7-8]7 reserved
	uint8_t latitude_ns;	// 6 N/S    [9]   0x4E или 0x53
	uint8_t longitude_g;	// 7(0-180) [10]
	uint8_t longitude_m;	// 8(0-60)  [11]
	//unsigned long int longitude_s;     //11  (6 bytes (48 bit) число до 281 474 976 710 656) unsigned long int size=32
	uint16_t longitude_s;	// 9-10      [12-13]
	uint16_t longitude_s1;	// 11-12     [14-15]
	uint8_t longitude_ew;	// 13 E or W    0x45 или 0x57
	uint8_t hdop;			// 14 HDOP, m, 0-99
	uint8_t pdop;			// 15 PDOP, m, 0-99
} PACKED gps_coord_t;

// 16
typedef struct
{
	double lat;
	double lon;
	// double alt;
	uint8_t hdop;
	uint8_t pdop;
} geo;

// 24
typedef struct
{
	double lat;
	double lon;
	double alt;
} geo3;

typedef struct
{
	double ve, az, vh;
} speed3;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х0 -
} PACKED packet0_t;

typedef ALIGN struct {
	uint8_t packettype;	// 0 0х01 -
	geo3 coord;		// 1
	time5 time;		// 25
} PACKED packet1_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х02 -
	geo3 coord;
	time5 time;
} PACKED packet2_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х03 -
	geo3 coord;
	time5 time;
} PACKED packet3_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х04 -
	speed3 speed;
	time5 time;
} PACKED packet4_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х05 -
} PACKED packet5_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х06 -
} PACKED packet6_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х07 -
} PACKED packet7_t;

typedef ALIGN struct {
	uint8_t packettype;		// 0) 1 байт 0х08 - координаты с NMEA приемников LAT, LON,
	uint8_t gpsolddata:1;		// 1)GPS old data, GPS not read
	uint8_t gpsencoded:1;		// GPS data encoded
	uint8_t gpsfrommemory:1;	// data got from the memory
	uint8_t gpsnoformat:1;		// memory is not formatted
	uint8_t gpsnosats:1;		// No visible GPS salellites
	uint8_t gpsbadhdop:1;		// GPS HDOP = 99 or > 50
	uint8_t gpstime:1;		// 1 бит 1-0  GPS.valid=0x01 или 0;     GPS.time_valid=0b0000 0010 или 0;
	uint8_t gpsnavdata:1;		// 1 бит 1-0  GPS.valid=0x01 или 0;     GPS.time_valid=0b0000 0010 или 0;
	uint8_t sats;			// 2) 1 байт  кол. видимых спутников
	gps_coord_t coord;		// 3) 16 байт (или 10 байт??? ) hex ?
	uint8_t bat:6;			// 19) байт бортовое напряжение fixed point (32=3.2 вольта) (от 2.0 до 4.0)
	uint8_t alarmlow:1;		// bit 6 < 2V, bit 7 > 4V
	uint8_t alarmhigh:1;		// bit 6 < 2V, bit 7 > 4V
	int8_t temperature;		// 20) 1 байт температура
	uint8_t r2;			// 21) 1 байт номер пакета?? - он же индекс в таблице кодировки
	uint8_t failurepower:1;		//< 22) device status power loss
	uint8_t failureeep:1;		//< EEPROM failure
	uint8_t failureclock:1;		//< clock not responding
	uint8_t failurecable:1;		//< MAC not responding
	uint8_t failureint0:1;		//< clock int failure
	uint8_t failurewatchdog:1;	//< software failure
	uint8_t failurenoise:1;		//< blocks in memory found
	uint8_t failureworking:1;	//< device was
	uint16_t key;			// 23) 2 байт     volatile unsigned int packet_key;  младшие 16 бит
	// uint8_t		 res[3];
	time5 time;			// 25 5 байт
	// uint32_t crc;
} PACKED packet8_t;

typedef ALIGN struct {
	uint8_t packettype;	// 1 байт 0х9 -
} PACKED packet9_t;

typedef ALIGN struct {
	uint8_t packettype;	// 0 0х10 -
	uint32_t mask;		// 1
	float gdop;		// 5
	float pdop;		// 9
	uint8_t alarmlow:1;	// 13 bit 6 < 2V, bit 7 > 4V
	uint8_t alarmhigh:1;	// 13 bit 6 < 2V, bit 7 > 4V
	uint8_t bat:6;		// 13 бортовое напряжение fixed point (32=3.2 вольта) (от 2.0 до 4.0)
	uint8_t gpsreserved:6;	// 14
	uint8_t gpstime:1;	// 14
	uint8_t gpsnavdata:1;	// 14
	uint16_t powerminus:1;	// 15 power -
	uint16_t tempinside:15;// 15 1/10 C. >2000- below zero e.g. 2597 -> -59,7C
	uint16_t status:2;	// 17
	uint16_t powerminus2:1;	// 17 power -
	uint16_t tempoutside:13;// 17 1/10 C. >2000- below zero e.g. 2597 -> -59,7C
	uint16_t batstatus:4;	// 19
	uint16_t batext: 12;	// 19 1/10 V
	uint16_t ampers;	// 21 1/100 A
	uint16_t reserved;	// 23
	time5 time;		// 25
} PACKED packet10_t;

typedef ALIGN struct {
	uint8_t packettype;	// 0 0х11
	uint8_t msgid:4;	// 1
	uint8_t msgchunk:4;	//
	char text[SIZETEXTCHAR];		// 2
	uint8_t codepage;	// 22
	uint8_t reserved1;	// 23
	uint8_t reserved2;	// 24
	time5 time;		// 25
} PACKED packet11_t;

#define SIZE_BLOCK_CMD	6

// 1 команда 8 bytes
typedef ALIGN struct {
	uint8_t len;		// 1 байт длина команды с самой командой!
	uint8_t command;	// 1 байт сама команда
	uint8_t args[SIZE_BLOCK_CMD];	// 6 bytes аргументы
} cmdmessage;

typedef ALIGN struct
{
	uint8_t packettype;		// 1 byte всегда 0x88
	uint8_t keyMessageType[3]; 	// = {0x8C, 0xAE, 0xF0}; // массив байт - ключ контроля  0x8C ( М)  0xAE (о)  0xF0( Ё )
	uint8_t cmdcount;		// кол.команд всего в пакете
	uint8_t lencmd;			// 1 byte длина всех команд = 30 - 3 - 3 = менее равно 24 байта
	cmdmessage cmd[3];		// массив до 3 команд по 8 байт
} packet88_t;

typedef ALIGN union {
	packet0_t p0;
	packet1_t p1;
	packet2_t p2;
	packet3_t p3;
	packet4_t p4;
	packet5_t p5;
	packet6_t p6;
	packet7_t p7;
	packet8_t p8;
	packet9_t p9;
	// packet types A, B, C, D, E, F reserved
	packet10_t p10;
	packet11_t p11;
	// hole....
	packet88_t p88;
} PACKED packet_t;

#ifdef _MSC_VER
#pragma pack(pop)
#else
#endif


// точность 16 бит это 0-65536 те 1/65536 = примерно 0.000 015 2588 минуты
//  1851.9 * 0.0000152588 = примерно 0.029 м - вполне достаточно!!!!
//  4 знака долей минуты 0.0001 - 0.9999 или 1851.9 * 0.0001 = 0.18519 м

#ifdef __cplusplus
#define CALLC extern "C"
#else
#define CALLC
#endif

/**
 * Structure used in iridiummessage.h dbpgiridium.h
 */
typedef struct
{
	uint64_t id;		// reserved for external processing
	uint8_t packettype;	// 8- animal
	time_t gpstime;		// in local time zone
	time_t utc;		// Iridium UTC time stamp
	char imei[16];
	geo location;		// GPS location
} MessageDescription;

CALLC time_t time52time_t(time5 *src);
CALLC void time_t2time5(time5 *dest, time_t src);

CALLC void gps_coord_t2geo(geo *dest, gps_coord_t *src);
CALLC void geo2gps_coord_t(gps_coord_t *dest, geo *src);

#endif /* IRIDIUMPACKET_H_ */

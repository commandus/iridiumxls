/*
 * IridiumMessage.cpp
 *
 *  Created on: 28.10.2015
 *      Author: andrei
 */

#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sstream>
#ifdef _MSC_VER
#include <Winsock2.h>
#else
#include <netinet/in.h>
#endif

#include "iridiumpacket.h"
#include "iridiummessage.h"
#include "iridiumpacketattr.h"
#include "dump.h"

inline static int fromTemperature(int v)
{
	if (v > 0)
		return v;
	else
		return -(v & 0x7f);
}

inline static int toTemperature(int v)
{
	if (v > 0)
		return v;
	else
		return (v & 0x7f) | 0x80;
}
template <typename T>
inline static std::string to_string(T v)
{
	std::ostringstream s;
	s << std::fixed << std::setprecision(PRECISION) << v;
	return s.str();
}

const char* IridiumInfoElementTypeString[] = {"none", "io", "payload", "location", "mt_io", "mt_payload", "mt_confirmation"};

#define PACKETTYPE_SIZE	19
const char* PayloadTypeChar[PACKETTYPE_SIZE] = {
	"reserved0",
	"reserved1",
	"reserved2",
	"reserved3",
	"reserved4",
	"reserved5",
	"reserved6",
	"reserved7",
	"collar",
	"reserved9",
	"A",
	"B",
	"C",
	"D",
	"E",
	"F",
	"service",
	"reserved11",
	// ......
	"cmdmessage"
};

const char *dateformat[] = {"%FT%T", "%F %T"};

#define PAYLOAD_SIZE	12


#define VALIDATE_NULL(v) (v.length() ? v : DEFVAL)

// TODO initialize empty
static packet_t DEFPAYLOAD;
static IEIOHeader DEFIOHEADER;
static IELocation DEFLOCATION;

static IEMTIOHeader DEFMTHEADER;
static IEMTConfirmation DEFMTCONFIRMATION;

const std::string DEFVAL = "NULL";

void initIridium()
{
	memset(&DEFPAYLOAD, 0, sizeof(packet_t));
	memset(&DEFIOHEADER, 0, sizeof(IEIOHeader));
	memset(&DEFLOCATION, 0, sizeof(IELocation));
	memset(&DEFMTHEADER, 0, sizeof(IEMTIOHeader));
	memset(&DEFMTCONFIRMATION, 0, sizeof(IEMTConfirmation));
}

/**
 * Payload has fixed size.
 * There are 10..15 hole in the offset array
 */
static const int payloadsizes[PAYLOAD_SIZE] = {
	sizeof(packet0_t), sizeof(packet1_t), sizeof(packet2_t), sizeof(packet3_t), sizeof(packet4_t),
	sizeof(packet5_t), sizeof(packet6_t), sizeof(packet7_t), sizeof(packet8_t), sizeof(packet9_t),

	sizeof(packet10_t), sizeof(packet11_t)
};

/**
 * offset of two bytes word of payload
 */
static const int ofs20[] = {-1};
static const int ofs21[] = {25, -1};
static const int ofs22[] = {25, -1};
static const int ofs23[] = {25, -1};
static const int ofs24[] = {25, -1};
static const int ofs25[] = {-1};
static const int ofs26[] = {-1};
static const int ofs27[] = {-1};

//static const int ofs28[] = {5, 7, 12, 14, 25, -1};
// check is minutes/1000 is not in network order
static const int ofs28[] = {7, 14, 25, -1};

static const int ofs29[] = {-1};
static const int ofs210[] = {15, 17, 19, 21, 23, 25, -1};
static const int ofs211[] = {25, -1};
static const int *payload2[PAYLOAD_SIZE] = {
	ofs20, ofs21, ofs22, ofs23, ofs24,
	ofs25, ofs26, ofs27, ofs28, ofs29,
	ofs210, ofs211
};

/**
 * offset of four bytes word of payload
 */
static const int ofs40[] = {-1};
static const int ofs41[] = {-1};
static const int ofs42[] = {-1};
static const int ofs43[] = {-1};
static const int ofs44[] = {-1};
static const int ofs45[] = {-1};
static const int ofs46[] = {-1};
static const int ofs47[] = {-1};
static const int ofs48[] = {-1};
static const int ofs49[] = {-1};
static const int ofs410[] = {1, 5, 9, -1};
static const int ofs411[] = {-1};
static const int *payload4[PAYLOAD_SIZE] = {
	ofs40, ofs41, ofs42, ofs43, ofs44,
	ofs45, ofs46, ofs47, ofs48, ofs49,
	ofs410, ofs411
};

/**
 * offset of eight bytes word of payload
 */
static const int ofs80[] = {-1};
static const int ofs81[] = {1, 9, 17, -1};
static const int ofs82[] = {1, 9, 17, -1};
static const int ofs83[] = {1, 9, 17, -1};
static const int ofs84[] = {1, 9, 17, -1};
static const int ofs85[] = {-1};
static const int ofs86[] = {-1};
static const int ofs87[] = {-1};
static const int ofs88[] = {-1};
static const int ofs89[] = {-1};
static const int ofs810[] = {-1};
static const int ofs811[] = {-1};
static const int *payload8[PAYLOAD_SIZE] = {
	ofs80, ofs81, ofs82, ofs83, ofs84,
	ofs85, ofs86, ofs87, ofs88, ofs89,
	ofs810, ofs811
};

// 2014-02-27T15:53:39
std::istream &operator>>(std::istream &istr, time5 &v)
{
    char c;
    memset(&v, 0, sizeof(time5));
    int year, month, day, h, mm, s;
    istr >> year >> c
    	    >> month >> c
    	    >> day >> c
    	    >> h >> c
    	    >> mm >> c
    	    >> s;

    v.year = year - 2000;
    v.month = month;
    v.day = day;
    v.hour = h;
    v.minute = mm;
    v.second = s;

    v.month--;
    return istr;
};

const char *IridiumInfoElementType2String(IridiumInfoElementType value)
{
	std::string r;
	int idx = (int) value;
	if (idx >= ELEMENTTYPE_COUNT)
		idx = idx - 0x40 + 3;
	if ((idx >= ELEMENTTYPE_COUNT) || (idx < 0))
		return IridiumInfoElementTypeString[0];
	return IridiumInfoElementTypeString[idx];
}

const char *payloadTypeChar(int value)
{
	std::string r;
	if (value == 0x88)
		value = PACKETTYPE_SIZE - 1;
	if (value >= PACKETTYPE_SIZE)
		value = 0;
	return PayloadTypeChar[value];
}

/**
 * TInfoElement keep pointers to the data buffer.
 * Do not destroy buffer while TInfoElement exists!
 */
TInfoElement::TInfoElement()
{
	data = NULL;
}

TInfoElement::TInfoElement(void *value)
{
	data = (InfoElement *) value;
}

TInfoElement::TInfoElement(const void *buffer, size_t size)
{
	set(buffer, size);
}

TInfoElement::TInfoElement(const PacketRawBuffer &b)
{
	setVector(b);
}

TInfoElement::~TInfoElement()
{
}

/**
 * Return type of the element
 */
IridiumInfoElementType TInfoElement::getType()
{
	return (IridiumInfoElementType) getVersion();
}

int TInfoElement::getVersion()
{
	if (data == NULL)
		return 0;
	return data->elementheader.id;
}

/**
 * Return type of the element
 */
std::string TInfoElement::getTypeString()
{
	return IridiumInfoElementType2String(getType());
}

/**
 * Return type of the payload
 */
std::string TInfoElement::getPayloadTypeString(int v)
{
	return payloadTypeChar(v);
}

/**
 * getSize return 0 if no element data.
 * Method set() put element data
 * \sa set()
 */
uint16_t TInfoElement::getSize()
{
	if (data == NULL)
		return 0;
	else
		return (IridiumInfoElementType) data->elementheader.size;
}

uint16_t TInfoElement::getContainerSize()
{
	uint16_t r = getSize();
	if (r)
		r += 3;
	return r;
}

void TInfoElement::setId(uint8_t value)
{
	if (data == NULL)
		return;
	data->elementheader.id = value;
}

void TInfoElement::setSize(uint16_t value)
{
	if (data == NULL)
		return;
	data->elementheader.size = value;
}

void TInfoElement::setData(const void *value)
{
	data = (InfoElement *) value;
}

/**
 * Set element data. Each info element has length descriptor.
 * 1. Checks is buffer has enough space for length descriptor.
 * 2. Checks size descriptor. If provided data buffer size smaller than info element size, no data is set.
 * \sa getSize()
 */
uint8_t TInfoElement::set(const void *buffer, size_t size)
{
	if (buffer && (size >= 3))
	{
		data = (InfoElement*) buffer;
		convert();
		if (data->elementheader.size > size - 3)
		{
			// Wrong element size
			data = NULL;
			return 0;
		}
		return (uint8_t) data->elementheader.size;
	}
	else
	{
		data = NULL;
		return 0;
	}
}

void TInfoElement::set(TInfoElement &value)
{
	data = value.data;
}

bool TInfoElement::readString(const std::string &value)
{
	std::stringstream ss(value);
	return read(ss);
}

bool TInfoElement::read(std::istream &value)
{
	return true;
}

uint8_t TInfoElement::setVector(const PacketRawBuffer &b)
{
	return set(&b[0], b.size());
}

bool TInfoElement::convert()
{
	data->elementheader.size = ntohs(data->elementheader.size);
	return true;
}

void TInfoElement::clear()
{
	set(NULL, 0);
}

void htonWrite(std::ostream &result, void *v, int size, const bool noconversion = false)
{
	if (noconversion)
	{
		result.write((const char*) v, size);
		return;
	}
	switch (size)
	{
	case 1:
		result.write((const char*) v, 1);
		break;
	case 2:
		{
			uint16_t p = htons(*(uint16_t*)v);
			result.write((const char*) &p, 2);
		}
		break;
	case 4:
		{
			uint32_t p2 = htonl(*(uint32_t*)v);
			result.write((const char*) &p2, 4);
		}
		break;
	}
}

void write32(std::ostream &result, uint16_t v)
{
	uint32_t p = htonl(v);
	result.write((const char*) &p, 4);
}

void TInfoElement::toStream(std::ostream &result, const bool noconversion)
{
	if (data == NULL)
		return;
	htonWrite(result, &data->elementheader.id, 1, noconversion);
	htonWrite(result, &data->elementheader.size, 2, noconversion);
}

std::string TInfoElement::toHexString()
{
	std::stringstream ss;
	toStream(ss);
	return Dump::hexString(ss);
}

std::string TInfoElement::toString()
{
	std::stringstream ss;
	ss << getTypeString() << ":" << getSize();
	return ss.str();
}

std::string TInfoElement::toJson()
{
	std::stringstream ss;
	ss << "\"" << getTypeString() << "\":" << getSize();
	return ss.str();
}

void TInfoElement::toStrings(std::map<std::string, std::string> &map)
{
}

// ------------------------- TIOHeader ------------------------

TIOHeader::TIOHeader(void *value) : TInfoElement(value)
{
	data->elementheader.id = 1;
	data->elementheader.size = 28;
};

TIOHeader::TIOHeader(TInfoElement &element) : TInfoElement()
{
	set(element);
	convert();
}

IEIOHeader *TIOHeader::getIOHeader()
{
	if (data == NULL)
		return NULL;
	return (IEIOHeader*) &(data->elementdata);
}

std::string TIOHeader::getImeiString()
{
	return TIMEI::toString(getImei());
}

void TIOHeader::toStream(std::ostream &result, const bool noconversion)
{
	TInfoElement::toStream(result, noconversion);
	IEIOHeader *p = getIOHeader();
	if (p == NULL)
		return;
	htonWrite(result, &(p->cdrref), 4, noconversion);
	result.write((const char *) &p->imei, 15);
	htonWrite(result, &p->status, 1, noconversion);
	htonWrite(result, &p->sentno, 2, noconversion);
	htonWrite(result, &p->recvno, 2, noconversion);
	htonWrite(result, &p->recvtime, 4, noconversion);
}

std::string TIOHeader::toString()
{
	std::stringstream ss;
	IEIOHeader *p = getIOHeader();
	if (p == NULL)
		return "";
	ss << TInfoElement::toString() << " ";
	char dt[32];
	time_t t = p->recvtime;
	struct tm tm;
	localtime_s(&tm, &t);
	strftime(dt, sizeof(dt), dateformat[0], &tm);
	ss << "cddref:" << p->cdrref << ","
		<< "imei:" << TIMEI::toString((IMEI*) &p->imei) << ","
		<< "status:" << (int) p->status << ","
		<< "sentno:" << p->sentno << ","
		<< "recvno:" << p->recvno << ","
		<< "recvtime:" << dt;
	return ss.str();
}

std::string TIOHeader::toJson()
{
	std::stringstream ss;
	IEIOHeader *p = getIOHeader();
	if (p == NULL)
		return "";
	ss << "\"" << getTypeString() <<"\":{";
	char dt[32];
	time_t t = p->recvtime;
	struct tm tm;
	localtime_s(&tm, &t);
	strftime(dt, sizeof(dt), dateformat[0], &tm);
	ss << "\"cddref\":" << p->cdrref << ","
		<< "\"imei\":\"" << TIMEI::toString((IMEI*) &p->imei) << "\","
		<< "\"status\":" << (int) p->status << ","
		<< "\"sentno\":" << p->sentno << ","
		<< "\"recvno\":" << p->recvno << ","
		<< "\"recvtime\":\"" << dt << "\","
		<< "\"time\":" << p->recvtime << "}";
	return ss.str();
}

void TIOHeader::toStrings(std::map<std::string, std::string> &map)
{
	IEIOHeader *p = getIOHeader();
	if (p == NULL)
		return ;
	map["cddref"] = to_string(p->cdrref );
	map["imei"] = to_string(TIMEI::toString((IMEI*) &p->imei));
	map["status"] = to_string((int) p->status );
	map["sentno"] = to_string(p->sentno);
	map["recvno"] = to_string(p->recvno);

	char dt[32];
	time_t t = p->recvtime;
	struct tm tm;
	localtime_s(&tm, &t);
	strftime(dt, sizeof(dt), dateformat[0], &tm);
	map["recvtime"] = to_string(dt);

	map["time"] = to_string(p->recvtime);\
}

bool TIOHeader::convert()
{
	if (getSize() < sizeof(IEIOHeader))
		return false;
	TInfoElement::convert();
	IEIOHeader *p = getIOHeader();
	p->cdrref = ntohl(p->cdrref);
	p->sentno = ntohs(p->sentno);		//< modem sent out packet number
	p->recvno = ntohs(p->recvno);		//< modem received out packet number. Not used
	p->recvtime = ntohl(p->recvtime);	//< Unix epoch time, seconds since 1970
	return true;
}

bool TIOHeader::read(std::istream &value)
{
	IEIOHeader *p = getIOHeader();
	if (p == NULL)
		return false;
	uint32_t cdrref;
	std::string imei;
	uint16_t sentno;	//< momsn modem sent out packet number			2
	uint16_t recvno;	//< mtmsn modem received out packet number. Not used		2
	time5 t;
	int v;

	value >> cdrref
		>> imei
		>> v
		>> sentno
		>> recvno
		>> t;
	p->status = v;
	p->cdrref = cdrref;
	memmove(&p->imei, TIMEI(imei).get(), sizeof(IMEI));
	p->sentno = sentno;
	p->recvno = recvno;
	p->recvtime = (uint32_t) time52time_t(&t);
	return true;
}

// Iridium message identifier4
uint32_t TIOHeader::getCdrref()
{
	return getIOHeader()->cdrref;
}

// see IMEI						15
IMEI *TIOHeader::getImei()
{
	return &(getIOHeader()->imei);
}

// see IOStatus					1
uint8_t TIOHeader::getStatus()
{
	return getIOHeader()->status;
}

// momsn modem sent out packet number			2
uint16_t TIOHeader::getSentno()
{
	return getIOHeader()->sentno;
}

// mtmsn modem received out packet number. Not used		2
uint16_t TIOHeader::getRecvno()
{
	return getIOHeader()->recvno;
}

// stime Unix epoch time, seconds since 1970			4
uint32_t TIOHeader::getRecvtime()
{
	return getIOHeader()->recvtime;
}

// Iridium message identifier				4
void TIOHeader::setCdrref(uint32_t v)
{
	getIOHeader()->cdrref = v;
}

// see IMEI						15
void TIOHeader::setImei(IMEI *v)
{
	memmove(&getIOHeader()->imei, v, sizeof(IMEI));
}

// see IOStatus					1
void TIOHeader::setStatus(uint8_t v)
{
	getIOHeader()->status = v;
}

// momsn modem sent out packet number			2
void TIOHeader::setSentno(uint16_t v)
{
	getIOHeader()->sentno = v;
}

// mtmsn modem received out packet number. Not used		2
void TIOHeader::setRecvno(uint16_t v)
{
	getIOHeader()->recvno = v;
}

// stime Unix epoch time, seconds since 1970			4
void TIOHeader::setRecvtime(uint32_t v)
{
	getIOHeader()->recvtime = v;
}

// ------------------------- TMTIOHeader ------------------------

TMTIOHeader::TMTIOHeader(void *value) : TInfoElement(value)
{
	data->elementheader.id = 0x41;
	data->elementheader.size = 21;
};

TMTIOHeader::TMTIOHeader(TInfoElement &element) : TInfoElement()
{
	set(element);
	convert();
}

IEMTIOHeader *TMTIOHeader::getMTIOHeader()
{
	if (data == NULL)
		return NULL;
	return (IEMTIOHeader*) &(data->elementdata);
}

void TMTIOHeader::toStream(std::ostream &result, const bool noconversion)
{
	TInfoElement::toStream(result, noconversion);
	IEMTIOHeader *p = getMTIOHeader();
	if (p == NULL)
		return;
	result.write((const char *) &p->msgid, 4);
	result.write((const char *) &p->imei, 15);
	htonWrite(result, &p->flags, 2, noconversion);
}

std::string TMTIOHeader::msgIdAsString(IEMTIOHeader *header)
{
	char m[5];
	memmove(m, header->msgid, 4);
	m[4] = '\0';
	return std::string(m);
}

void TMTIOHeader::setMsgId(IEMTIOHeader *header, const std::string &v)
{
	int len = v.size();
	if (len > 4)
		len = 4;
	memmove(header->msgid, v.c_str(), len);
	if (len < 4)
		memset(&header->msgid[len], 0, 4 - len);

}

std::string TMTIOHeader::toString()
{
	std::stringstream ss;
	IEMTIOHeader *p = getMTIOHeader();
	if (p == NULL)
		return "";
	ss << TInfoElement::toString() << " ";
	ss << "msgid:" << msgIdAsString(p) << ","
		<< "imei:" << TIMEI::toString((IMEI*) &p->imei) << ","
		<< "flags:" << (int) p->flags;
	return ss.str();
}

std::string TMTIOHeader::toJson()
{
	std::stringstream ss;
	IEMTIOHeader *p = getMTIOHeader();
	if (p == NULL)
		return "";
	ss << "\"" << getTypeString() <<"\":{";
	ss << "\"msgid\":\"" << msgIdAsString(p) << "\","
		<< "\"imei\":\"" << TIMEI::toString((IMEI*) &p->imei) << "\","
		<< "\"flags\":" << (int) p->flags << "}";
	return ss.str();
}

void TMTIOHeader::toStrings(std::map<std::string, std::string> &map)
{
	IEMTIOHeader *p = getMTIOHeader();
	if (p == NULL)
		return;
	map["msgid"] = to_string(msgIdAsString(p));
	map["imei"] = to_string(TIMEI::toString((IMEI*) &p->imei));
	map["flags"] = to_string((int) p->flags);
}

bool TMTIOHeader::convert()
{
	if (getSize() < sizeof(IEMTIOHeader))
		return false;
	TInfoElement::convert();
	IEMTIOHeader *p = getMTIOHeader();
	p->flags = ntohs(p->flags);
	return true;
}

bool TMTIOHeader::read(std::istream &value)
{
	IEMTIOHeader *p = getMTIOHeader();
	if (p == NULL)
		return false;
	std::string imei;
	int f;
	std::string mid;
	value >> mid >> imei >> f;
	setMsgid(mid);
	p->flags = f;
	memmove(&p->imei, TIMEI(imei).get(), sizeof(IMEI));
	return true;
}

// message identifier	4
std::string TMTIOHeader::getMsgid()
{
	return msgIdAsString(getMTIOHeader());
}

// see IMEI						15
IMEI *TMTIOHeader::getImei()
{
	return &(getMTIOHeader()->imei);
}

std::string TMTIOHeader::getImeiString()
{
	return TIMEI::toString(getImei());
}

/**
 * get disposition flags:
 * 	1- delete all MT payloads in queue,
 * 	2- send ring alert - with no associated payload
 */
uint16_t TMTIOHeader::getFlags()
{
	return getMTIOHeader()->flags;
}

// Iridium message identifier				4
void TMTIOHeader::setMsgid(const std::string &v)
{
	setMsgId(getMTIOHeader(), v);
}

// see IMEI						15
void TMTIOHeader::setImei(IMEI *v)
{
	memmove(&getMTIOHeader()->imei, v, sizeof(IMEI));
}

//
void TMTIOHeader::setFlags(uint16_t v)
{
	getMTIOHeader()->flags = v;
}

//----------------------------- TMTPayload

TMTPayload::TMTPayload(void *value) : TInfoElement(value)
{
	data->elementheader.id = 0x42;
	data->elementheader.size = 30;
}

TMTPayload::TMTPayload(TInfoElement &element)
{
	set(element);
	convert();
}

packet_t *TMTPayload::getPayload()
{
	if (data == NULL)
		return &DEFPAYLOAD;
	return (packet_t*) &(data->elementdata);
}

bool TMTPayload::convert()
{
	if (!valid())
		return false;
	TInfoElement::convert();
	return true;
}

bool TMTPayload::read(std::istream &value)
{
	packet_t *p = getPayload();
	if (p == NULL)
		return false;
	int id;
	value >> id;
	setPayloadId(id);
	switch (id)
	{
		case 0x88:
			{
				packet88_t *p88 = get88();
				if (!p88)
					break;
				p88->packettype = 0x88;
				// command_count command agrument_count argument1...
				value >> p88->cmdcount;
				p88->lencmd = p88->cmdcount * sizeof(cmdmessage);
				for (int c = 0; c < p88->cmdcount; c++)
				{
					if (c >= 3)
						continue;
					int argumentcount;
					value >> p88->cmd[c].command >> argumentcount;
					p88->cmd[c].len = argumentcount + 1;

					for (int a = 0; a < argumentcount; a++)
					{
						if (a >= 6)
							continue;
						value >> p88->cmd[c].args[a];
					}
				}
			}
			break;
	}
	return true;
}

/**
* Retutn -1 if invalid
*/
int TMTPayload::getId()
{
	if (data == NULL)
		return -1;
	return data->elementdata[0];
}

void TMTPayload::setPayloadId(int id)
{
	if (data == NULL)
		return;
	data->elementdata[0] = id;
}

uint16_t TMTPayload::getRequiredSize(int id)
{
	return 30;
}

bool TMTPayload::valid()
{
	int id = getId();
	return (id == 0x88);
}

void TMTPayload::toStream(std::ostream &result, const bool noconversion)
{
	TInfoElement::toStream(result, noconversion);
	int id = getId();
	switch (id)
	{
	case 0x88:
		packet88_t *p88 = &(getPayload()->p88);
		result.write((const char*) p88, sizeof(packet88_t));
		break;
	};
}

std::string TMTPayload::toString()
{
	std::stringstream ss;
	int id = getId();
	ss << TInfoElement::toString() << " ";
	switch (id)
	{
	case 0x88:
		{
			packet88_t *p88 = &(getPayload()->p88);
			for (int i = 0; i < p88->cmdcount; i++)	// кол.команд всего в пакете
			{
				cmdmessage *c = &p88->cmd[i];
				ss << "command:" << (int) c->command << ", argruments: ";
				for (int a = 0; a < c->len - 1; a++)	// кол. аргументов + 1 command
				{
					ss << (int) c->args[a];
					if (a < c->len - 2)
						ss << ",";
				}
			}
		}
		break;
	};
	return ss.str();
}

std::string TMTPayload::toJson()
{
	std::stringstream ss;
	int id = getId();
	ss << "\"mtpayload\":{";
	ss << "\"type\":\"" << getPayloadTypeString(id) << "\",\"packet\":" << id;
	switch (id)
	{
		case 0x88:
		{
			packet88_t *p88 = &(getPayload()->p88);
			ss << ",\"commands\":[";
			for (int i = 0; i < p88->cmdcount; i++)	// кол.команд всего в пакете
			{
				cmdmessage *c = &p88->cmd[i];
				ss << "{" << "\"command\":" << (int) c->command << ", " << "\"arguments\":[";
				for (int a = 0; a < c->len - 1; a++)	// кол. аргументов + 1 command
				{
					ss << (int) c->args[a];
					if (a < c->len - 2)
						ss << ", ";
				}
				ss << "]}";
				if (i < p88->cmdcount - 1)
					ss << ", ";
			}
			ss << "]";
		}
		break;
	};
	ss << "}";
	return ss.str();
}

void TMTPayload::toStrings(std::map<std::string, std::string> &map)
{
	int id = getId();
	map["type"] = to_string(getPayloadTypeString(id));
	map["packet"] = to_string(id);

	switch (id)
	{
		case 0x88:
		{
			packet88_t *p88 = &(getPayload()->p88);
			for (int i = 0; i < p88->cmdcount; i++)	// кол.команд всего в пакете
			{
				cmdmessage *c = &p88->cmd[i];
				map["command"] = to_string((int) c->command);
				for (int a = 0; a < c->len - 1; a++)	// кол. аргументов + 1 command
				{
					map["arg"] = to_string((int) (int) c->args[a]);
				}
			}
		}
		break;
	};
}

// check packet identifier, if not return default empty packet
packet_t *TMTPayload::getPacket(int id)
{
	if (getId() == id)
		return getPayload();
	else
		return &DEFPAYLOAD;
}

//----------------------------- TMTConfirmation

TMTConfirmation::TMTConfirmation(void *value)
{
	data->elementheader.id = 0x44;
	data->elementheader.size = 25;
}


TMTConfirmation::TMTConfirmation(TInfoElement &element)
{
	set(element);
	convert();
}

IEMTConfirmation *TMTConfirmation::getMTConfirmation()
{
	if (data == NULL)
		return NULL;
	return (IEMTConfirmation*) &(data->elementdata);
}

bool TMTConfirmation::convert()
{
	if (getSize() < sizeof(IEMTConfirmation))
		return false;
	TInfoElement::convert();
	IEMTConfirmation *p = getMTConfirmation();
	// p->msgid = ntohl(p->msgid);
	p->idref = ntohl(p->idref);
	p->status = ntohs(p->status);
	return true;

}

bool TMTConfirmation::read(std::istream &value)
{
	IEMTConfirmation *p = getMTConfirmation();
	if (p == NULL)
		return false;
	std::string imei;
	int v, v2, v3;
	value >> v >> imei >> v2 >> v3;
	p->msgid = v;
	p->idref = v2;
	p->status = v3;
	memmove(&p->imei, TIMEI(imei).get(), sizeof(IMEI));
	return true;

}

uint16_t TMTConfirmation::getRequiredSize(int id)
{
	return 25;
}

bool TMTConfirmation::valid()
{
	return true;
}

void TMTConfirmation::toStream(std::ostream &result, const bool noconversion)
{
	TInfoElement::toStream(result, noconversion);
	IEMTConfirmation *p = getMTConfirmation();
	if (p == NULL)
		return;
	// htonWrite(result, &p->msgid, 4);
	result.write((const char *) &p->msgid, 4);
	result.write((const char *) &p->imei, 15);
	htonWrite(result, &p->idref, 4, noconversion);
	htonWrite(result, &p->status, 2, noconversion);
}

std::string TMTConfirmation::toString()
{
	std::stringstream ss;
	IEMTConfirmation *p = getMTConfirmation();
	if (p == NULL)
		return "";
	ss << TInfoElement::toString() << " "
			<< "msgid: " << p->msgid
			<< ", imei: " << TIMEI::toString(&p->imei)
			<< ", idref: " << p->idref
			<< ", status: " << p->status;

	return ss.str();
}

std::string TMTConfirmation::toJson()
{
	std::stringstream ss;
	ss << "\"confirmation\":{";
	IEMTConfirmation *p = getMTConfirmation();
	if (p != NULL)
	{
		ss << "\"msgid\":\"" << p->msgid << "\",\"imei\":\""
				<< TIMEI::toString(&p->imei) << "\", \"idref\":"
				<< p->idref << ", \"status\":" << p->status << "}";
	}
	ss << "}";
	return ss.str();
}

void TMTConfirmation::toStrings(std::map<std::string, std::string> &map)
{
	IEMTConfirmation *p = getMTConfirmation();
	if (p == NULL)
		return;
	map["msgid"] = to_string(p->msgid);
	map["imei"] = to_string(TIMEI::toString(&p->imei));
	map["idref"] = to_string(p->idref);
	map["status"] = to_string(p->status );
}

uint32_t TMTConfirmation::getMsgid()
{
	return getMTConfirmation()->msgid;
}

IMEI *TMTConfirmation::getImei()
{
	return &(getMTConfirmation()->imei);
}

std::string TMTConfirmation::getImeiString()
{
	return TIMEI::toString(getImei());
}

uint32_t TMTConfirmation::getIdref()
{
	return getMTConfirmation()->idref;
}

int16_t TMTConfirmation::getStatus()
{
	return getMTConfirmation()->status;
}

void TMTConfirmation::setMsgid(uint32_t v)
{
	getMTConfirmation()->msgid = v;
}

void TMTConfirmation::setImei(IMEI *v)
{
	memmove(&getMTConfirmation()->imei, v, sizeof(IMEI));
}

void TMTConfirmation::setIdref(uint32_t v)
{
	getMTConfirmation()->idref = v;
}

void TMTConfirmation::setStatus(int16_t v)
{
	getMTConfirmation()->status = v;
}

//----------------------------- TIMEI

TIMEI::TIMEI(std::string &v)
{
	size_t sz = v.length();
	if (sz > sizeof(IMEI))
		sz = sizeof(IMEI);
	else
		memset(&data, 0, sizeof(IMEI));
	if (sz > 0)
		memmove(&data, v.c_str(), sz);
}

IMEI *TIMEI::get()
{
	return &data;
}

std::string TIMEI::toString(IMEI *element)
{
	char b[16];
	copy(b, element);
	return std::string((char*) b);
}

void TIMEI::copy(char* dest, IMEI *element)
{
	if ((dest == NULL) || (element == NULL))
		return;
	memmove(dest, element, 15);
	dest[15] = 0;
}

void TIMEI::copy(IMEI* dest, IMEI *element)
{
	if ((dest == NULL) || (element == NULL))
		return;
	memmove(dest, element, 15);
}

void TIMEI::copy(IMEI* dest, const char *element)
{
	if ((dest == NULL) || (element == NULL))
		return;
	int len = strlen(element);
	if (len < 15)
	{
		if (len)
			memmove(dest, element, len);
		memset(&dest[len], 0, 15 - len);
	}
	else
		memmove(dest, element, 15);
}

//------------------- TIELocation

TIELocation::TIELocation(void *value) : TInfoElement(value)
{
	data->elementheader.id = 3;
	data->elementheader.size = 11;
};

TIELocation::TIELocation(TInfoElement &element) : TInfoElement()
{
	set(element);
	convert();
}

IELocation *TIELocation::getLocation()
{
	if (data == NULL)
		return NULL;
	return (IELocation*) &(data->elementdata);
}

void TIELocation::toStream(std::ostream &result, const bool noconversion)
{
	TInfoElement::toStream(result, noconversion);
	IELocation *p = getLocation();
	if (p == NULL)
		return;
	htonWrite(result, p, 1, noconversion);	// dir
	htonWrite(result, &p->lat, 1, noconversion);
	htonWrite(result, &p->lat1000, 2, noconversion);
	htonWrite(result, &p->lon, 1, noconversion);
	htonWrite(result, &p->lon1000, 2, noconversion);
	htonWrite(result, &p->cepradius, 4, noconversion);
}

double TIELocation::getLatitude()
{
	IELocation *p = getLocation();
	if (p == NULL)
		return 0.0;
	double r = p->lat + (p->lat1000 / 60000.0);
	if (p->dirnsi)
		r = -r;
	return r;
}

double TIELocation::getLongitude()
{
	IELocation *p = getLocation();
	if (p == NULL)
		return 0.0;
	double r = p->lon + (p->lon1000 / 60000.0);
	if (p->direwi)
		r = -r;
	return r;
}

uint32_t TIELocation::getCEPRadius()
{
	IELocation *p = getLocation();
	if (p == NULL)
		return 0;
	return p->cepradius;
}

void TIELocation::setLatitude(double v)
{
	IELocation *p = getLocation();
	if (p == NULL)
		return;
	double a = fabs(v);
	p->lat = (uint8_t) a;
	p->lat1000 = (uint16_t) (a - p->lat) * 60000;
	p->dirnsi = v < 0 ? 1 : 0;
}

void TIELocation::setLongitude(double v)
{
	IELocation *p = getLocation();
	if (p == NULL)
		return;
	double a = fabs(v);
	p->lon = (uint8_t) a;
	p->lon1000 = (uint16_t) (a - p->lon) * 60000;
	p->direwi = v < 0 ? 1 : 0;
}

void TIELocation::setCEPRadius(uint32_t v)
{
	IELocation *p = getLocation();
	if (p == NULL)
		return;
	p->cepradius = v;
}

std::string TIELocation::toString()
{
	std::stringstream ss;
	IELocation *p = getLocation();
	if (p == NULL)
		return "";
	ss << TInfoElement::toString() << " ";
	ss << std::fixed << std::setprecision(PRECISION)
		<< "lat: " << getLatitude() << ", "
		<< "lon: " << getLongitude() << ", "
		<< "cepradius: " << p->cepradius;
	return ss.str();
}

std::string TIELocation::toJson()
{
	std::stringstream ss;
	IELocation *p = getLocation();
	if (p == NULL)
		return "";
	ss << "\"" << getTypeString() <<"\":{";
	ss << std::fixed << std::setprecision(PRECISION)
		<< "\"lat\":" << getLatitude() << ","
		<< "\"lon\":" << getLongitude() << ","
		<< "\"cepradius\":" << p->cepradius << "}";
	return ss.str();
}

void TIELocation::toStrings(std::map<std::string, std::string> &map)
{
	IELocation *p = getLocation();
	if (p == NULL)
		return ;
	map["lat"] = to_string(getLatitude());
	map["lon"] = to_string(getLongitude());
	map["cepradius"] = to_string(p->cepradius);
}

bool TIELocation::read(std::istream &value)
{
	IELocation *p = getLocation();
	if (p == NULL)
		return false;
	double lat, lon;
	uint32_t cepradius;
	value >> lat >> lon >> cepradius;
	setLatitude(lat);
	setLongitude(lon);
	setCEPRadius(cepradius);
	return true;
}

bool TIELocation::convert()
{
	if (getSize() < sizeof(IELocation))
		return false;
	TInfoElement::convert();
	IELocation *p = getLocation();
	p->lat1000 = ntohs(p->lat1000);
	p->lon1000 = ntohs(p->lon1000);
	p->cepradius = ntohl(p->cepradius);
	return true;
}

// --------------------------- TIEPayload -----------------------------

TIEPayload::TIEPayload(void *value) : TInfoElement(value)
{
	data->elementheader.id = 2;
	data->elementheader.size = 30;
}

TIEPayload::TIEPayload(TInfoElement &element) : TInfoElement()
{
	set(element);
	convert();
}

packet_t* TIEPayload::getPayload()
{
	if (data == NULL)
		return &DEFPAYLOAD;
	return (packet_t*) &(data->elementdata);
}

/**
 * Retutn -1 if invalid
 */
int TIEPayload::getId()
{
	if (data == NULL)
		return -1;
	return data->elementdata[0];
}

void TIEPayload::setPayloadId(int id)
{
	if (data == NULL)
		return;
	data->elementdata[0] = id;
}

void TIEPayload::toStream(std::ostream &result, const bool noconversion)
{
	TInfoElement::toStream(result, noconversion);
	int id = getId();
	switch (id)
	{
	case 1:
	case 2:
	case 3:
	case 4:
		{
			packet1_t *p1 = &(getPayload()->p1);
			htonWrite(result, &p1->packettype, 1, noconversion);
			htonWrite(result, &p1->coord.lat, 8, noconversion);
			htonWrite(result, &p1->coord.lon, 8, noconversion);
			htonWrite(result, &p1->coord.alt, 8, noconversion);
			htonWrite(result, &p1->time, 2, noconversion);
			result.write((const char *) &p1->time.hour, 3);
		}
		break;
	case 8:
		{
			packet8_t *p8 = &(getPayload()->p8);
			char *p = (char*) &p8->packettype;
			htonWrite(result, p, 1, noconversion);
			p++;
			htonWrite(result, p, 1, noconversion);
			htonWrite(result, &p8->sats, 1, noconversion);

			// gps_coord_t
			htonWrite(result, &p8->coord.latitude_g, 1, noconversion);
			htonWrite(result, &p8->coord.latitude_m, 1, noconversion);
			htonWrite(result, &p8->coord.latitude_s, 2, noconversion);
			htonWrite(result, &p8->coord.latitude_s1, 2, noconversion);
			htonWrite(result, &p8->coord.latitude_ns, 1, noconversion);

			htonWrite(result, &p8->coord.longitude_g, 1, noconversion);
			htonWrite(result, &p8->coord.longitude_m, 1, noconversion);
			htonWrite(result, &p8->coord.longitude_s, 2, noconversion);
			htonWrite(result, &p8->coord.longitude_s1, 2, noconversion);
			htonWrite(result, &p8->coord.longitude_ew, 1, noconversion);
			htonWrite(result, &p8->coord.hdop, 1, noconversion);
			htonWrite(result, &p8->coord.pdop, 1, noconversion);

			p = (char*) &p8->coord;
			p += 16;
			htonWrite(result, p, 1, noconversion);	// battery
			
			int t = toTemperature(p8->temperature);	// 2018
			htonWrite(result, &t, 1, noconversion);
			htonWrite(result, &p8->r2, 1, noconversion);

			p = (char*) &p8->r2;
			p++;
			htonWrite(result, p, 1, noconversion);

			htonWrite(result, &p8->key, 2, noconversion);

			htonWrite(result, &p8->time, 2, noconversion);
			result.write((const char *) &p8->time.hour, 3);
		}
		break;
	case 0x10:
		{
			packet10_t *p10 = &(getPayload()->p10);
			htonWrite(result, &p10->packettype, 1, noconversion);
			htonWrite(result, &p10->mask, 4, noconversion);
			htonWrite(result, &p10->gdop, 4, noconversion);
			htonWrite(result, &p10->pdop, 4, noconversion);

			char *p = (char*) &p10->pdop;
			p += 4;
			htonWrite(result, p, 1, noconversion);	// alarmlow:1 alarmhigh:1 battery:6
			p++;
			htonWrite(result, p, 1, noconversion);	// gpsreserved:6 gpstime:1 gpsnavdata:1
			p++;
			htonWrite(result, p, 2, noconversion);	// powerminus:1 tempinside:15
			p += 2;
			htonWrite(result, p, 2, noconversion);	// status:2 powerminus2:1 tempoutside:13
			p += 2;
			htonWrite(result, p, 2, noconversion);	// batstatus:4 batext: 12

			htonWrite(result, &p10->ampers, 2, noconversion);
			htonWrite(result, &p10->reserved, 2, noconversion);

			htonWrite(result, &p10->time, 2, noconversion);
			result.write((const char *) &p10->time.hour, 3);
		}
		break;
	case 0x11:
		{
			packet11_t *p11 = &(getPayload()->p11);
			htonWrite(result, &p11->packettype, 1, noconversion);

			char *p = (char*) &p11->packettype;
			p++;
			htonWrite(result, p, 1, noconversion);	// msgid:4 msgchunk:4

			for (int i = 0; i < SIZETEXTCHAR; i++)
				htonWrite(result, &(p11->text[i]), 1);
			htonWrite(result, &p11->codepage, 1, noconversion);
			htonWrite(result, &p11->reserved1, 1, noconversion);
			htonWrite(result, &p11->reserved2, 1, noconversion);

			htonWrite(result, &p11->time, 2, noconversion);
			result.write((const char *) &p11->time.hour, 3);
		}
		break;
	case 0x88:
		packet88_t *p88 = &(getPayload()->p88);
		result.write((const char*) p88, sizeof(packet88_t));
		break;
	};
}

std::string TIEPayload::toString()
{
	std::stringstream ss;
	int id = getId();
	ss << TInfoElement::toString() << " ";
	char dt[32];
	switch (id)
	{
	case 1:
	case 2:
	case 3:
	case 4:
		{
			packet1_t *p1 = &(getPayload()->p1);
			time_t t = time52time_t(&(p1->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss 	<< "geo: (" << GeoLocation(&(p1->coord)).toString() << "), "
				<< "time: " << dt;

		}
		break;
	case 8:
		{
			packet8_t *p8 = &(getPayload()->p8);
			time_t t = time52time_t(&(p8->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss
				<< "gpstime: " << (int) p8->gpstime << ", "
				<< "gpsnavdata: " << (int) p8->gpsnavdata << ", "
				<< "gpsolddata: " << (int) p8->gpsolddata << ", "
				<< "gpsencoded: " << (int) p8->gpsencoded << ", "
				<< "gpsfrommemory: " << (int) p8->gpsfrommemory << ", "
				<< "gpsnoformat: " << (int) p8->gpsnoformat << ", "
				<< "gpsnosats: " << (int) p8->gpsnosats << ", "
				<< "gpsbadhdop: " << (int) p8->gpsbadhdop << ", "
				<< "satellites: " << (int) p8->sats << ", "
				<< "geo: (" << GeoLocation(&(p8->coord)).toString() << "), "
				<< "alarmlow: " << (int) p8->alarmlow << ", "
				<< "alarmhigh: " << (int) p8->alarmhigh << ", "
				<< "battery: " << p8->bat * 0.1 << ", "
				<< "temperature: " << (int) p8->temperature << ", "
				<< "r2: " << (int) p8->r2 << ", "
				<< "failurepower: " << (int) p8->failurepower << ", "		///< 22 device status power loss
				<< "failureeep: " << (int) p8->failureeep << ", "			///< EEPROM failure
				<< "failureclock: " << (int) p8->failureclock << ", "		///< clock not responding
				<< "failurecable: " << (int) p8->failurecable << ", "		///< MAC not responding
				<< "failureint0: " << (int) p8->failureint0 << ", "			///< clock int failure
				<< "failurewatchdog: " << (int) p8->failurewatchdog << ", "	///< software failure
				<< "failurenoise: " << (int) p8->failurenoise << ", "		///< blocks in memory found
				<< "failureworking: " << (int) p8->failureworking << ", "	///< device was
				<< "key: " << (int) p8->key << ", "
				<< "time: " << dt;
				//  << " " << "CRC: " << p8->crc;
		}
		break;
	case 0x10:
		{
			packet10_t *p10 = &(getPayload()->p10);
			time_t t = time52time_t(&(p10->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss
				<< "mask: " << p10->mask << ", "
				<< "gdop: " << p10->gdop << ", "
				<< "pdop: " << p10->pdop << ", "
				<< "alarmlow: " << (int) p10->alarmlow << ", "
				<< "alarmhigh: " << (int) p10->alarmhigh << ", "
				<< "battery: " << p10->bat * 0.1 << ", "
				<< "gpsreserved: " << (int) p10->gpsreserved << ", "
				<< "gpstime: " << (int) p10->gpstime << ", "
				<< "gpsnavdata: " << (int) p10->gpsnavdata << ", "
				<< "powerminus: " << (int) p10->powerminus << ", "
				<< "tempinside: " << (p10->tempinside >= 2000 ? (200 - p10->tempinside * 0.1) : p10->tempinside * 0.1) << ", "
				<< "status: " << (int) p10->status << ", "
				<< "powerminus2: " << (int) p10->powerminus2 << ", "
				<< "tempoutside: " << (p10->tempoutside >= 2000 ? (200 - p10->tempoutside * 0.1) : p10->tempoutside * 0.1) << ", "
				<< "batstatus: " << p10->batstatus << ", "
				<< "batext: " << p10->batext * 0.1 << ", "
				<< "current: " << p10->ampers * 0.01 << ", "
				<< "reserved: " << p10->reserved << ", "
				<< "time: " << dt;
		}
		break;
	case 0x11:
		{
			packet11_t *p11 = &(getPayload()->p11);
			time_t t = time52time_t(&(p11->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss	<< "msgid: " << p11->msgid << ", "
				<< "msgchunk: " << p11->msgchunk << ", "
				<< "text: " << p11->text << ", "
				<< "codepage: " << (int) p11->codepage << ", "
				<< "reserved1: " << (int) p11->reserved1 << ", "
				<< "reserved2: " << (int) p11->reserved2 << ", "
				<< "time: " << dt;
		}
		break;
	case 0x88:
		{
			packet88_t *p88 = &(getPayload()->p88);
			for (int i = 0; i < p88->cmdcount; i++)	// кол.команд всего в пакете
			{
				cmdmessage *c = &p88->cmd[i];
				ss << "command:" << (int) c->command << ", argruments: ";
				for (int a = 0; a < c->len - 1; a++)	// кол. аргументов + 1 command
				{
					ss << (int) c->args[a];
					if (a < c->len - 2)
						ss << ",";
				}
			}
		}
		break;
	};
	return ss.str();
}

std::string TIEPayload::toJson()
{
	std::stringstream ss;
	int id = getId();
	ss << "\"payload\":{";
	ss << "\"type\":\"" << getPayloadTypeString(id) << "\",\"packet\":" << id;
	char dt[32];
	switch (id)
	{
	case 1:
	case 2:
	case 3:
	case 4:
		{
			packet1_t *p1 = &(getPayload()->p1);
			time_t t = time52time_t(&(p1->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss << "," << GeoLocation(&(p1->coord)).toJson() << "), "
				<< "\"time\":\"" << dt
				<< "\",\"utc\":" << t;

		}
		break;
	case 8:
		{
			packet8_t *p8 = &(getPayload()->p8);
			time_t t = time52time_t(&(p8->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss << ","
				<< "\"gpstime\":" << (int) p8->gpstime << ","
				<< "\"gpsnavdata\":" << (int) p8->gpsnavdata << ","
				<< "\"gpsolddata\":" << (int) p8->gpsolddata << ","
				<< "\"gpsencoded\":" << (int) p8->gpsencoded << ","
				<< "\"gpsfrommemory\":" << (int) p8->gpsfrommemory << ","
				<< "\"gpsnoformat\":" << (int) p8->gpsnoformat << ","
				<< "\"gpsnosats\":" << (int) p8->gpsnosats << ","
				<< "\"gpsbadhdop\":" << (int) p8->gpsbadhdop << ","
				<< "\"satellites\":" << (int) p8->sats << ","
				<< GeoLocation(&(p8->coord)).toJson() << ","
				<< "\"alarmlow\":" << (int) p8->alarmlow << ","
				<< "\"alarmhigh\":" << (int) p8->alarmhigh << ","
				<< "\"battery\":" << (float) p8->bat * 0.1 << ","
				<< "\"temperature\":" << (int) p8->temperature << ","
				<< "\"r2\":" << (int) p8->r2 << ","
				<< "\"failurepower\":" << (int) p8->failurepower << ","		//< 22 device status power loss
				<< "\"failureeep\":" << (int) p8->failureeep << ","		//< EEPROM failure
				<< "\"failureclock\":" << (int) p8->failureclock << ","		//< clock not responding
				<< "\"failurecable\":" << (int) p8->failurecable << ","		//< MAC not responding
				<< "\"failureint0\":" << (int) p8->failureint0 << ","		//< clock int failure
				<< "\"failurewatchdog\":" << (int) p8->failurewatchdog << ","	//< software failure
				<< "\"failurenoise\":" << (int) p8->failurenoise << ","		//< blocks in memory found
				<< "\"failureworking\":" << (int) p8->failureworking << ","		//< device was
				<< "\"key\":" << (int) p8->key << ","
				<< "\"time\":\"" << dt
				<< "\",\"utc\":" << t;
				//  << ",\"crc\":" << p8->crc;
		}
		break;
	case 0x10:
		{
			packet10_t *p10 = &(getPayload()->p10);
			time_t t = time52time_t(&(p10->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss << ","
				<< "\"mask\":" << p10->mask << ","
				<< "\"gdop\":" << p10->gdop << ","
				<< "\"pdop\":" << p10->pdop << ","
				<< "\"alarmlow\":" << (int) p10->alarmlow << ","
				<< "\"alarmhigh\":" << (int) p10->alarmhigh << ","
				<< "\"battery\":" << p10->bat * 0.1 << ","
				<< "\"gpsreserved\":" << (int) p10->gpsreserved << ","
				<< "\"gpstime\":" << (int) p10->gpstime << ","
				<< "\"gpsnavdata\":" << (int) p10->gpsnavdata << ","
				<< "\"powerminus\":" << (int) p10->powerminus << ","
				<< "\"tempinside\":" << (p10->tempinside >= 2000 ? (200 - p10->tempinside * 0.1) : p10->tempinside * 0.1) << ","
				<< "\"status\":" << (int) p10->status << ","
				<< "\"powerminus2\":" << (int) p10->powerminus2 << ","
				<< "\"tempoutside\":" << (p10->tempoutside >= 2000 ? (200 - p10->tempoutside * 0.1) : p10->tempoutside * 0.1) << ","
				<< "\"batstatus\": " << p10->batstatus << ","
				<< "\"batext\":" << p10->batext * 0.1 << ","
				<< "\"current\":" << p10->ampers * 0.01 << ","
				<< "\"reserved\":" << p10->reserved << ","
				<< "\"time:\"" << dt
				<< "\",\"utc\":" << t;
		}
		break;
	case 0x11:
		{
			packet11_t *p11 = &(getPayload()->p11);
			time_t t = time52time_t(&(p11->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			ss << ","
				<< "\"msgid\":" << p11->msgid << ", "
				<< "\"msgchunk\":" << p11->msgchunk << ", "
				<< "\"text\":" << p11->text << ", "
				<< "\"codepage\":" << (int) p11->codepage << ", "
				<< "\"reserved1\":" << (int) p11->reserved1 << ", "
				<< "\"reserved2\":" << (int) p11->reserved2 << ", "
				<< "\"time\":" << dt
				<< "\",\"utc\":" << t;
		}
		break;
	case 0x88:
		{
			packet88_t *p88 = &(getPayload()->p88);
			ss << ",\"commands\":[";
			for (int i = 0; i < p88->cmdcount; i++)	// кол.команд всего в пакете
			{
				cmdmessage *c = &p88->cmd[i];
				ss << "{" << "\"command\":" << (int) c->command << ", " << "\"arguments\":[";
				for (int a = 0; a < c->len - 1; a++)	// кол. аргументов + 1 command
				{
					ss << (int) c->args[a];
					if (a < c->len - 2)
						ss << ", ";
				}
				ss << "]}";
				if (i < p88->cmdcount - 1)
					ss << ", ";
			}
			ss << "]";
		}
		break;
	};
	ss << "}";
	return ss.str();
}

void TIEPayload::toStrings(std::map<std::string, std::string> &map)
{
	int id = getId();
	map["type"] = to_string(getPayloadTypeString(id));
	map["packet"] = to_string(id);
	char dt[32];
	switch (id)
	{
		case 1:
		case 2:
		case 3:
		case 4:
		{
			packet1_t *p1 = &(getPayload()->p1);
			time_t t = time52time_t(&(p1->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			GeoLocation(&(p1->coord)).toStrings(map);
			map["time"] = to_string(dt);
			map["utc"] = to_string(t);
		}
			break;
		case 8:
		{
			packet8_t *p8 = &(getPayload()->p8);
			time_t t = time52time_t(&(p8->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);

			map["gpstime"] = to_string((int) p8->gpstime);
			map["gpsnavdata"] = to_string((int) p8->gpsnavdata);
			map["gpsolddata"] = to_string((int) p8->gpsolddata);
			map["gpsencoded"] = to_string((int) p8->gpsencoded);
			map["gpsfrommemory"] = to_string((int) p8->gpsfrommemory);
			map["gpsnoformat"] = to_string((int) p8->gpsnoformat);
			map["gpsnosats"] = to_string((int) p8->gpsnosats);
			map["gpsbadhdop"] = to_string((int) p8->gpsbadhdop);
			map["sats"] = to_string((int) p8->sats);

			GeoLocation(&(p8->coord)).toStrings(map);

			map["alarmlow"] = to_string((int) p8->alarmlow);
			map["alarmhigh"] = to_string((int) p8->alarmhigh);
			map["battery"] = to_string(p8->bat * 0.1);
			map["temperature"] = to_string((int) p8->temperature);
			map["r2"] = to_string((int) p8->r2);
			map["failurepower"] = to_string((int) p8->failurepower);
			map["failureeep"] = to_string((int) p8->failureeep);
			map["failureclock"] = to_string((int) p8->failureclock);
			map["failurecable"] = to_string((int) p8->failurecable);
			map["failureint0"] = to_string((int) p8->failureint0);
			map["failurewatchdog"] = to_string((int) p8->failurewatchdog);
			map["failurenoise"] = to_string((int) p8->failurenoise);
			map["failureworking"] = to_string((int) p8->failureworking);
			map["key"] = to_string((int) p8->key);
			map["time"] = to_string(dt);
			map["utc"] = to_string(t);
		}
			break;
		case 0x10:
		{
			packet10_t *p10 = &(getPayload()->p10);
			time_t t = time52time_t(&(p10->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			
			   map["mask"] = to_string(p10->mask);
			   map["gdop"] = to_string(p10->gdop);
			   map["pdop"] = to_string(p10->pdop);
			   map["alarmlow"] = to_string((int) p10->alarmlow);
			   map["alarmhigh"] = to_string((int) p10->alarmhigh);
			   map["battery"] = to_string(p10->bat * 0.1);
			   map["gpsreserved"] = to_string((int) p10->gpsreserved);
			   map["gpstime"] = to_string((int) p10->gpstime);
			   map["gpsnavdata"] = to_string((int) p10->gpsnavdata);
			   map["powerminus"] = to_string((int) p10->powerminus);
			   map["tempinside"] = to_string((p10->tempinside >= 2000 ? (200 - p10->tempinside * 0.1) : p10->tempinside * 0.1));
			   map["status"] = to_string((int) p10->status);
			   map["powerminus2"] = to_string((int) p10->powerminus2);
			   map["tempoutside"] = to_string((p10->tempoutside >= 2000 ? (200 - p10->tempoutside * 0.1) : p10->tempoutside * 0.1));
			   map["batstatus"] = to_string(p10->batstatus);
			   map["batext"] = to_string(p10->batext * 0.1);
			   map["current"] = to_string(p10->ampers * 0.01);
			   map["reserved"] = to_string(p10->reserved);
			   map["time"] = to_string(dt);
			   map["utc"] = to_string(t);
		}
			break;
		case 0x11:
		{
			packet11_t *p11 = &(getPayload()->p11);
			time_t t = time52time_t(&(p11->time));
			t += timezone;
			struct tm tm;
			localtime_s(&tm, &t);
			strftime(dt, sizeof(dt), dateformat[0], &tm);
			map["msgid"] = to_string(p11->msgid);
			map["msgchunk"] = to_string(p11->msgchunk);
			map["text"] = to_string(p11->text);
			map["codepage"] = to_string((int) p11->codepage);
			map["reserved1"] = to_string((int) p11->reserved1);
			map["reserved2"] = to_string((int) p11->reserved2);
			map["time"] = to_string(dt);
			map["utc"] = to_string(t);
		}
			break;
		case 0x88:
		{
			packet88_t *p88 = &(getPayload()->p88);
			for (int i = 0; i < p88->cmdcount; i++)	// кол.команд всего в пакете
			{
				cmdmessage *c = &p88->cmd[i];
				map["command"] = to_string((int) c->command);
				for (int a = 0; a < c->len - 1; a++)	// кол. аргументов + 1 command
				{
					map["arg"] = to_string((int) c->args[a]);
				}
			}
		}
			break;
	};
}

bool TIEPayload::read(std::istream &value)
{
	packet_t *p = getPayload();
	if (p == NULL)
		return false;
	int id;
	value >> id;
	setPayloadId(id);
	switch (id)
	{
		case 1:
		case 2:
		case 3:
		case 4:
			{
				GeoLocation lla;
				value >> lla.lat;
				value >> lla.lon;
				setLLA(&lla);

				time5 t5;
				value >> t5;
				setTime(&t5);
			}
			break;
		case 8:
			{
				int v8[9];

				value >> v8[0] >> v8[1] >> v8[2] >> v8[3] >> v8[4] >> v8[5] >> v8[6] >> v8[7] >> v8[8];
				setIsGpsOldData(v8[0] != 0);
				setIsGpsEncoded(v8[1] != 0);
				setIsGpsFromMemory(v8[2] != 0);
				setIsGpsNoFormat(v8[3] != 0);
				setIsGpsNoSats(v8[4] != 0);
				setIsGpsBadHdop(v8[5] != 0);
				setIsGpsTime(v8[6] != 0);
				setIsGpsNavData(v8[7] != 0);
				setSats(v8[8]);

				GeoLocation lla;
				value >> lla.lat;
				value >> lla.lon;
				value >> lla.hdop;
				value >> lla.pdop;
				setLLA(&lla);

				value >> v8[0] >> v8[1];
				setIsAlarmLow(v8[0] != 0);
				setIsAlarmHigh(v8[1] != 0);

				float f;
				value >> f >> v8[0] >> v8[1];
				setBattery(f);
				setTemperature(v8[0]);
				setR2(v8[1]);

				value >> v8[0] >> v8[1] >> v8[2] >> v8[3] >> v8[4] >> v8[5] >> v8[6] >> v8[7];
				setIsFailurepower(v8[0] != 0);
				setIsFailureeep(v8[1] != 0);
				setIsFailureclock(v8[2] != 0);
				setIsFailurecable(v8[3] != 0);
				setIsFailureint0(v8[4] != 0);
				setIsFailurewatchdog(v8[5] != 0);
				setIsFailurenoise(v8[6] != 0);
				setIsFailureworking(v8[7] != 0);

				value >> v8[0];
				setKey(v8[0]);

				time5 t5;
				value >> t5;
				setTime(&t5);
			}
			break;
		case 0x10:
			{
				uint32_t u;
				value >> u;
				setMask(u);

				float f;
				value >> f;
				setGDOP(f);
				value >> f;
				setPDOP(f);

				int v8[4];
				value >> v8[0] >> v8[1] >> f;
				setIsAlarmLow(v8[0] != 0);
				setIsAlarmHigh(v8[1] != 0);
				setBattery(f);

				value >> v8[0] >> v8[1] >> v8[2] >> v8[3];
				setGpsReserved(v8[0]);
				setIsGpsTime(v8[1] != 0);
				setIsGpsNavData(v8[2] != 0);
				setIsPowerMinus(v8[3] != 0);

				value >> f;
				setTemperatureIn(f);

				value >> v8[0] >> v8[1];
				setStatus(v8[0]);
				setIsPowerMinus2(v8[1] != 0);

				value >> f;
				setTemperatureOut(f);

				value >> v8[0];
				setBatteryStatus(v8[0]);
				value >> f;
				setBatteryExt(f);

				value >> f;
				setCurrent(f);

				value >> v8[0];
				setReserved(v8[0]);
				time5 t5;
				value >> t5;
				setTime(&t5);
			}
			break;
		case 0x11:
			{
				int v8[2];
				value >> v8[0] >> v8[1];
				setTextId(v8[0]);
				setTextChunk(v8[0]);
				std::string s;
				value >> s;
				setText(s);
				value >> v8[0] >> v8[1];
				setTextCodepage(v8[0]);
				time5 t5;
				value >> t5;
				setTime(&t5);
			}
			break;
		case 0x88:
			{
				packet88_t *p88 = get88();
				if (!p88)
					break;
				p88->packettype = 0x88;
				// command_count command agrument_count argument1...
				value >> p88->cmdcount;
				p88->lencmd = p88->cmdcount * sizeof(cmdmessage);
				for (int c = 0; c < p88->cmdcount; c++)
				{
					if (c >= 3)
						continue;
					int argumentcount;
					value >> p88->cmd[c].command >> argumentcount;
					p88->cmd[c].len = argumentcount + 1;

					for (int a = 0; a < argumentcount; a++)
					{
						if (a >= 6)
							continue;
						value >> p88->cmd[c].args[a];
					}
				}
			}
			break;
	}
	return true;
}

bool TIEPayload::hasTime()
{
	int id = getId();
	return (((id > 0) && (id < 4)) || (id == 8));
}

// getters
// TODO 111
time_t TIEPayload::getTime()
{
	int id = getId();
	if ((id > 0) && (id < 4))
		return time52time_t(&get1()->time) + timezone;
	else if (id == 8)
		return time52time_t(&get8()->time) + timezone;
	return 0;
}

bool TIEPayload::hasLLA()
{
	int id = getId();
	return (((id > 0) && (id < 4)) || (id == 8));
}

GeoLocation TIEPayload::getLLA()
{
	int id = getId();

	if ((id > 0) && (id < 4))
	{
		GeoLocation l(&(get1()->coord));
		return l;
	}
	else if (id == 8)
	{
		GeoLocation l(&(get8()->coord));
		return l;
	}
	GeoLocation dl;
	return dl;
}

void TIEPayload::setLLA(GeoLocation *v)
{
	//
	int id = getId();

	if ((id > 0) && (id < 4))
	{
		v->putGeo3(&(get1()->coord));
		return;
	}
	else if (id == 8)
	{
		v->putGps_coord_t(&(get8()->coord));
		return;
	}
}

void TIEPayload::setTime(time_t v)
{
	time5 t5;
	time_t2time5(&t5, v);
	setTime(&t5);
}

void TIEPayload::setTime(time5 *v)
{
	int id = getId();
	if (((id > 0) && (id < 4)) || (id == 8) ||  (id == 0x10) || (id == 0x11))
	{
		memmove(&get1()->time, v, sizeof(time5));
	}
}

uint16_t TIEPayload::getRequiredSize(int id)
{
	if ((id < 0) || (id >= PAYLOAD_SIZE))
		return 0;
	return payloadsizes[id];
}

bool TIEPayload::valid()
{
	int id = getId();
	return (id > 0) && ((id == 0x88) || (id < PACKETTYPE_SIZE));
}

double convertDouble(char *v){
	union {
		double v;
		unsigned char b[8];
	} u;
	u.b[0] = v[5];
	u.b[1] = v[4];
	u.b[2] = v[7];
	u.b[3] = v[6];
	u.b[4] = v[1];
	u.b[5] = v[0];
	u.b[6] = v[3];
	u.b[7] = v[2];
	return u.v;
}

bool TIEPayload::convert()
{
	if (!valid())
		return false;
	TInfoElement::convert();
	int id = getId();
	if (id == 0x88)
		return true;
	int ofsid = id;
	if (ofsid >= 0x10)	// there are 10..15 hole in the offset array
		ofsid -= 6;
	if (ofsid >= PAYLOAD_SIZE)
		return false;

	char *p = (char *) getPayload();

	const int *o = payload2[ofsid];
	while (*o != -1)
	{
		*(uint16_t*)(p + *o) = ntohs(*(uint16_t*)(p + *o));
		o++;
	}

	o = payload4[ofsid];
	while (*o != -1)
	{
		*(uint32_t*)(p + *o) = ntohl(*(uint32_t*)(p + *o));
		o++;
	}
	o = payload8[ofsid];
	while (*o != -1)
	{
		// *(uint64_t*)(p + *o) = be64toh(*(uint64_t*)(p + *o));
		*(double*)(p + *o) = convertDouble(p + *o);
		o++;
	}

	// radian to degrees
	switch (id)
	{
		case 1:
		case 2:
		case 3:
			{
				packet1_t *v = (packet1_t *) p;
				v->coord.lat = v->coord.lat * 180 / GPS_PI;
				v->coord.lon = v->coord.lon * 180 / GPS_PI;
			}
	}
	// 2018
	if (id == 8) {
		packet8_t *v = (packet8_t *) p;
		v->temperature = fromTemperature(v->temperature);
	}
	return true;
}

packet_t *TIEPayload::getPacket(int id)
{
	if (getId() == id)
		return getPayload();
	else
		return &DEFPAYLOAD;
}

double TIEPayload::getLatitude()
{
	return getLLA().lat;
}

double TIEPayload::getLongitude()
{
	return getLLA().lon;
}

double TIEPayload::getAltitude()
{
	return getLLA().alt;
}

double TIEPayload::getVelocity()
{
	return getLLA().lat;
}

double TIEPayload::getAzimuth()
{
	return getLLA().lon;
}

double TIEPayload::getVelocityH()
{
	return getLLA().alt;
}

void TIEPayload::setLatitude(double v)
{
	GeoLocation r = getLLA();
	r.lat = v;
	setLLA(&r);
}

void TIEPayload::setLongitude(double v)
{
	GeoLocation r = getLLA();
	r.lon = v;
	setLLA(&r);
}

bool TIEPayload::isGpsOldData()
{
	if (getId() == 8)
		return getPacket(8)->p8.gpsolddata != 0;
	else
		return 0;
}

bool TIEPayload::isGpsEncoded()
{
	if (getId() == 8)
		return getPacket(8)->p8.gpsencoded != 0;
	else
		return 0;
}

bool TIEPayload::isGpsFromMemory()
{
	if (getId() == 8)
		return getPacket(8)->p8.gpsfrommemory != 0;
	else
		return 0;
}

bool TIEPayload::isGpsNoFormat()
{
	if (getId() == 8)
		return getPacket(8)->p8.gpsnoformat != 0;
	else
		return 0;
}

bool TIEPayload::isGpsNoSats()
{
	if (getId() == 8)
		return getPacket(8)->p8.gpsnosats != 0;
	else
		return 0;
}


bool TIEPayload::isGpsBadHdop()
{
	if (getId() == 8)
		return getPacket(8)->p8.gpsbadhdop != 0;
	else
		return false;
}

bool TIEPayload::isGpsTime()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.gpstime != 0;
	case 0x10:
		return getPacket(0x10)->p10.gpstime != 0;
	default:
			return false;
	}
}

bool TIEPayload::isGpsNavData()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.gpsnavdata != 0;
	case 0x10:
		return getPacket(0x10)->p10.gpsnavdata != 0;
	default:
			return false;
	}
}

bool TIEPayload::isAlarmLow()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.alarmlow != 0;
	case 0x10:
		return getPacket(0x10)->p10.alarmlow != 0;
	default:
			return false;
	}
}

bool TIEPayload::isAlarmHigh()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.alarmhigh != 0;
	case 0x10:
		return getPacket(0x10)->p10.alarmhigh != 0;
	default:
			return false;
	}
}

// 22 device status power loss
bool TIEPayload::isFailurepower()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failurepower;
	default:
			return 0;
	}
}

// EEPROM failure
bool TIEPayload::isFailureeep()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failureeep;
	default:
			return 0;
	}
}

// clock not responding
bool TIEPayload::isFailureclock()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failureclock;
	default:
			return 0;
	}
}

// MAC not responding
bool TIEPayload::isFailurecable()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failurecable;
	default:
			return 0;
	}
}

// clock int failure
bool TIEPayload::isFailureint0()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failureint0;
	default:
			return 0;
	}
}

// software failure
bool TIEPayload::isFailurewatchdog()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failurewatchdog;
	default:
			return 0;
	}
}

// blocks in memory found
bool TIEPayload::isFailurenoise()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failurenoise;
	default:
			return 0;
	}
}

// device was
bool TIEPayload::isFailureworking()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.failureworking;
	default:
			return 0;
	}
}

int TIEPayload::getSats()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.sats;
	default:
			return 0;
	}
}

float TIEPayload::getBattery()
{
	switch (getId())
	{
	case 8:
		return (float) (getPacket(8)->p8.bat * 0.1);
	case 0x10:
		return (float) (getPacket(0x10)->p10.bat * 0.1);
	default:
			return 0.0;
	}
}

int TIEPayload::getTemperature()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.temperature;
	case 0x10:
		return (int) (getPacket(0x10)->p10.tempinside * 0.1);
		// return getPacket(0x10)->p10.tempoutside * 0.1;
	default:
			return 0;
	}
}

int TIEPayload::getR2()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.r2;
	default:
			return 0;
	}
}

int TIEPayload::getKey()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.key;
	default:
			return 0;
	}
}

void TIEPayload::setAltitude(double v)
{
	GeoLocation r = getLLA();
	r.alt = v;
	setLLA(&r);
}

void TIEPayload::setVelocity(double v)
{
	setLatitude(v);
}

void TIEPayload::setAzimuth(double v)
{
	setLongitude(v);
}

void TIEPayload::setVelocityH(double v)
{
	setAltitude(v);
}

// 0x10 getters

int TIEPayload::getMask()
{
	switch (getId())
	{
	case 0x10:
		return getPacket(0x10)->p10.mask;
	default:
		return 0;
	}
}

int TIEPayload::getGpsReserved()
{
	switch (getId())
	{
	case 0x10:
		return getPacket(0x10)->p10.gpsreserved;
	default:
		return 0;
	}
}

float TIEPayload::getHDOP()
{
	return getGDOP();
}

float TIEPayload::getGDOP()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.coord.hdop; //?!!
	case 0x10:
		return getPacket(0x10)->p10.gdop;
	default:
		return 0;
	}
}

float TIEPayload::getPDOP()
{
	switch (getId())
	{
	case 8:
		return getPacket(8)->p8.coord.pdop;
	case 0x10:
		return getPacket(0x10)->p10.pdop;
	default:
		return 0;
	}
}

float TIEPayload::getVoltage()
{
	return (float) (getPacket(0x10)->p10.bat * 0.1);
}

bool TIEPayload::isVoltageLow()
{
	return getPacket(0x10)->p10.alarmlow != 0;
}

bool TIEPayload::isVoltageHigh()
{
	return getPacket(0x10)->p10.alarmhigh != 0;
}

bool TIEPayload::isTimeCorrect()
{
	return getPacket(0x10)->p10.gpstime != 0;
}

bool TIEPayload::isNavDataCorrect()
{
	return getPacket(0x10)->p10.gpsnavdata != 0;
}

bool TIEPayload::isPowerMinus()
{
	return getPacket(0x10)->p10.powerminus != 0;
}

bool TIEPayload::isPowerMinus2()
{
	return getPacket(0x10)->p10.powerminus2 != 0;
}

float TIEPayload::getVoltageExt()
{
	return (float) (getPacket(0x10)->p10.batext * 0.1);
}

float TIEPayload::getCurrent()
{
	return (float) (getPacket(0x10)->p10.ampers * 0.1);
}

float TIEPayload::getTemperatureIn()
{
	return (float) (getPacket(0x10)->p10.tempinside * 0.1);
}

float TIEPayload::getTemperatureOut()
{
	return (float) (getPacket(0x10)->p10.tempoutside * 0.1);
}

int TIEPayload::getStatus()
{
	return getPacket(0x10)->p10.status;
}

std::string TIEPayload::getText()
{
	return std::string(getPacket(0x11)->p11.text);
}

int TIEPayload::getTextId()
{
	return getPacket(0x11)->p11.msgid;
}

int TIEPayload::getTextChunk()
{
	return getPacket(0x11)->p11.msgchunk;
}

int TIEPayload::getTextCodepage()
{
	return getPacket(0x11)->p11.codepage;
}

int TIEPayload::getTextReserved()
{
	return getPacket(0x11)->p11.reserved1;
}

// 0x8 setters
void TIEPayload::setIsGpsOldData(bool v)
{
	if (getId() == 8)
		getPacket(8)->p8.gpsolddata  = v ? 1 : 0;
}

void TIEPayload::setIsGpsEncoded(bool v)
{
	if (getId() == 8)
		getPacket(8)->p8.gpsencoded = v ? 1 : 0;
}

void TIEPayload::setIsGpsFromMemory(bool v)
{
	if (getId() == 8)
		getPacket(8)->p8.gpsfrommemory = v ? 1 : 0;
}

void TIEPayload::setIsGpsNoFormat(bool v)
{
	if (getId() == 8)
		getPacket(8)->p8.gpsnoformat = v ? 1 : 0;;
}

void TIEPayload::setIsGpsNoSats(bool v)
{
	if (getId() == 8)
		getPacket(8)->p8.gpsnosats = v ? 1 : 0;
}

void TIEPayload::setIsGpsBadHdop(bool v)
{
	if (getId() == 8)
		getPacket(8)->p10.gpsreserved = v ? 1 : 0;
}

void TIEPayload::setGpsReserved(int v)
{
	if (getId() == 0x10)
		getPacket(0x10)->p10.gpsreserved = v;
}

void TIEPayload::setIsGpsTime(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.gpstime = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.gpstime = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setIsGpsNavData(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.gpsnavdata = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.gpsnavdata = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setIsFailurepower(bool v)
{
	getPacket(8)->p8.failurepower = v ? 1 : 0;
}

void TIEPayload::setIsFailureeep(bool v)
{
	getPacket(8)->p8.failureeep = v ? 1 : 0;
}

void TIEPayload::setIsFailureclock(bool v)
{
	getPacket(8)->p8.failureclock = v ? 1 : 0;
}

void TIEPayload::setIsFailurecable(bool v)
{
	getPacket(8)->p8.failurecable = v ? 1 : 0;
}

void TIEPayload::setIsFailureint0(bool v)
{
	getPacket(8)->p8.failureint0 = v ? 1 : 0;
}

void TIEPayload::setIsFailurewatchdog(bool v)
{
	getPacket(8)->p8.failurewatchdog = v ? 1 : 0;
}

void TIEPayload::setIsFailurenoise(bool v)
{
	getPacket(8)->p8.failurenoise = v ? 1 : 0;
}

void TIEPayload::setIsFailureworking(bool v)
{
	getPacket(8)->p8.failureworking = v ? 1 : 0;
}

void TIEPayload::setIsPowerMinus(bool v)
{
	getPacket(0x10)->p10.powerminus = v ? 1 : 0;
}

void TIEPayload::setIsPowerMinus2(bool v)
{
	getPacket(0x10)->p10.powerminus2 = v ? 1 : 0;
}

void TIEPayload::setSats(int v)
{
	getPacket(8)->p8.sats = v;
}

void TIEPayload::setIsAlarmLow(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.alarmlow = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.alarmlow = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setIsAlarmHigh(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.alarmhigh = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.alarmhigh = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setBattery(float v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.bat = (uint8_t) v * 10;
		break;
	case 0x10:
		getPacket(0x10)->p10.bat = (uint16_t) v * 10;
		break;
	}
}

void TIEPayload::setBatteryExt(float v)
{
	getPacket(0x10)->p10.batext = (uint16_t) v * 10;
}

void TIEPayload::setReserved(int v)
{
	getPacket(0x10)->p10.reserved = v;
}

void TIEPayload::setTemperature(int v)
{
	switch (getId())
	{
	case 8:
		{
			int t = fromTemperature(v);
			getPacket(8)->p8.temperature = t;
		}
		break;
	case 0x10:
		getPacket(0x10)->p10.tempinside  = v * 10;
		// getPacket(0x10)->p10.tempoutside * 10;
		break;
	}
}

void TIEPayload::setR2(int v)
{
	getPacket(8)->p8.r2 = v;
}

void TIEPayload::setKey(int v)
{
	getPacket(8)->p8.key = v;
}

// 0x10 setters

void TIEPayload::setMask(uint32_t v)
{
	getPacket(0x10)->p10.mask = v;
}

void TIEPayload::setGDOP(float v)
{
	getPacket(0x10)->p10.gdop = v;
}

void TIEPayload::setPDOP(float v)
{
	getPacket(0x10)->p10.pdop = v;
}

void TIEPayload::setVoltage(float v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.bat = (uint8_t) v * 10;
		break;
	case 0x10:
		getPacket(0x10)->p10.bat = (uint8_t) v * 10;
		break;
	}
}

void TIEPayload::setIsVoltageLow(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(0x8)->p8.alarmlow = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.alarmlow = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setIsVoltageHigh(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(0x8)->p8.alarmhigh = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.alarmhigh = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setIsTimeCorrect(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.gpstime = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.gpstime = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setIsNavDataCorrect(bool v)
{
	switch (getId())
	{
	case 8:
		getPacket(8)->p8.gpsnavdata = v ? 1 : 0;
		break;
	case 0x10:
		getPacket(0x10)->p10.gpsnavdata = v ? 1 : 0;
		break;
	}
}

void TIEPayload::setVoltageExt(float v)
{
	getPacket(0x10)->p10.batext = (uint16_t) v * 10;
}

void TIEPayload::setCurrent(float v)
{
	getPacket(0x10)->p10.ampers = (uint16_t) v * 10;
}

void TIEPayload::setTemperatureIn(float v)
{
	getPacket(0x10)->p10.tempinside = (uint16_t) (v < 0 ? (-v * 10) + 2000 : v * 10);
}

void TIEPayload::setTemperatureOut(float v)
{
	getPacket(0x10)->p10.tempoutside = (uint16_t) (v < 0 ? (-v * 10) + 2000 : v * 10);
}

void TIEPayload::setStatus(int v)
{
	getPacket(0x10)->p10.status = v;
}

void TIEPayload::setBatteryStatus(int v)
{
	getPacket(0x10)->p10.batstatus = v;
}

void TIEPayload::setText(std::string &v)
{
	size_t sz = v.length();
	char *p = getPacket(0x11)->p11.text;
	// fill zeroes
	if (sz < SIZETEXTCHAR)
		memset(p + sz, 0, SIZETEXTCHAR - sz);
	// cipy text
	if (sz > SIZETEXTCHAR)
		sz = SIZETEXTCHAR;
	memmove(p, v.c_str(), sz);
}

void TIEPayload::setTextId(int v)
{
	getPacket(0x11)->p11.msgid = v;
}

void TIEPayload::setTextChunk(int v)
{
	getPacket(0x11)->p11.msgchunk = v;
}

void TIEPayload::setTextCodepage(int v)
{
	getPacket(0x11)->p11.codepage = v;
}

void TIEPayload::setTextReserved(int v)
{
	getPacket(0x11)->p11.reserved1 = v;
	getPacket(0x11)->p11.reserved2 = v;
}

// --------------------------- TIridiumMessage -------------------------------

TIridiumMessage::TIridiumMessage()
{
	packet = NULL;
	ioheader = NULL;
	location = NULL;
	payload = NULL;
	mtioheader = NULL;
	mtconfirmation = NULL;
	mtpayload = NULL;
	gpsTimeZone = 0;
}

TIridiumMessage::TIridiumMessage(const void *buffer, size_t size, int tz)
{
	set(buffer, size);
	gpsTimeZone = tz;
}

TIridiumMessage::TIridiumMessage(const PacketRawBuffer &b, int tz)
{
	setVector(b);
	gpsTimeZone = tz;
}

uint8_t TIridiumMessage::set(const void *buffer, size_t size)
{
	packet = new TInfoElement(buffer, size);
	uint16_t sz = packet->getSize();

	ioheader = NULL;
	location = NULL;
	payload = NULL;

	mtioheader = NULL;
	mtpayload = NULL;
	mtconfirmation = NULL;

	if (sz == 0)		// packet has incomplete data
	{
		delete packet;
		packet = NULL;
		return 0;
	}

	uint16_t i = 3;
	char *p = (char*) buffer + 3;
	// read embedded info elements
	int count = 0;
	while ((i < sz) && (count < MAX_ELEMENTS))
	{
		TInfoElement *e = new TInfoElement(p, size - i);	// check info element does not cross buffer border
		uint8_t elindex = (uint8_t) e->getType();
		uint16_t sz = e->getSize();
		if (sz == 0)
			break;
		// MUST! TInfoElement() call convert(), return back
		e->convert();
		switch (elindex)
		{
			case 1:
				ioheader = new TIOHeader(*e);
				break;
			case 2:
				payload = new TIEPayload(*e);
				break;
			case 3:
				location = new TIELocation(*e);
				break;
			case 0x41:
				mtioheader = new TMTIOHeader(*e);
				break;
			case 0x42:
				mtpayload = new TMTPayload(*e);
				break;
			case 0x44:
				mtconfirmation = new TMTConfirmation(*e);
				break;
		}
		i += 3 + sz;
		p += 3 + sz;
		count++;
	}
	return (uint8_t) sz;
}

// -1 no message received 0- OK, 13- Incomplete Transfer
int TIridiumMessage::getSessionStatus()
{
	IEIOHeader *h = ioheader->getIOHeader();
	if (h == NULL)
		return -1;
	return h->status;
}

TIridiumMessage::~TIridiumMessage()
{
}

void TIridiumMessage::toStream(std::ostream &result, const bool noconversion)
{
	if (packet == NULL)
		return;
	packet->toStream(result, noconversion);
	if (ioheader != NULL)
		ioheader->toStream(result, noconversion);
	if (mtioheader != NULL)
		mtioheader->toStream(result, noconversion);
	if (location != NULL)
		location->toStream(result, noconversion);
	if (payload != NULL)
		payload->toStream(result, noconversion);
	if (mtpayload != NULL)
		mtpayload->toStream(result, noconversion);
	if (mtconfirmation != NULL)
		mtconfirmation->toStream(result, noconversion);

}

std::string TIridiumMessage::toString()
{
	std::stringstream ss;
	if (packet == NULL)
		return "";
	ss << "version: " << packet->getVersion() << ", size: " << packet->getSize() << std::endl;

	if (ioheader)
		ss << ioheader->toString() << std::endl;
	if (mtioheader)
		ss << mtioheader->toString() << std::endl;
	if (location)
		ss << location->toString() << std::endl;
	if (payload)
		ss << payload->toString() << std::endl;
	if (mtpayload)
		ss << mtpayload->toString() << std::endl;
	if (mtconfirmation)
		ss << mtconfirmation->toString() << std::endl;
	return ss.str();
}

std::string TIridiumMessage::toHex()
{
	if (packet == NULL)
		return "";
	return Dump::hexString(packet, packet->getData()->elementheader.size);
}

std::string TIridiumMessage::toJson()
{
	std::stringstream ss;
	if (packet == NULL)
		return "";
	ss << "{\"version\":" << packet->getVersion() << ",\"size\":" << packet->getSize();

	if (ioheader)
	{
		ss << ",";
		ss << ioheader->toJson();
	}
	if (mtioheader)
	{
		ss << ",";
		ss << mtioheader->toJson();
	}
	if (location)
	{
		ss << ",";
		ss << location->toJson();
	}
	if (payload)
	{
		ss << ",";
		ss << payload->toJson();
	}
	if (mtpayload)
	{
		ss << ",";
		ss << mtpayload->toJson();
	}
	if (mtconfirmation)
	{
		ss << ",";
		ss << mtconfirmation->toJson();
	}

	ss <<  "}";

	return ss.str();
}

void TIridiumMessage::toStrings(std::map<std::string, std::string> &map)
{
	if (packet == NULL)
		return;
	map["version"] = packet->getVersion();
	map["size"] = packet->getSize();

	if (ioheader)
	{
		ioheader->toStrings(map);
	}
	if (mtioheader)
	{
		mtioheader->toStrings(map);
	}
	if (location)
	{
		location->toStrings(map);
	}
	if (payload)
	{
		payload->toStrings(map);
	}
	if (mtpayload)
	{
		mtpayload->toStrings(map);
	}
	if (mtconfirmation)
	{
		mtconfirmation->toStrings(map);
	}
}

TInfoElement *TIridiumMessage::getMainPacket()
{
	if (packet)
		return packet;
	else
		return NULL;
}

IEIOHeader *TIridiumMessage::getIEIOHeader()
{
	if (ioheader)
		return ioheader->getIOHeader();
	else
		return &DEFIOHEADER;
}

IELocation *TIridiumMessage::getIELocation()
{
	if (location)
		return location->getLocation();
	else
		return &DEFLOCATION;
}

static time_t readTime(const std::string &defval)
{
	time_t r;
	if (defval.empty())
		r = time(NULL);
	else {
		struct tm tm;
		memset(&tm, 0, sizeof(struct tm));
		strptime(defval.c_str(), dateformat[0], &tm);
		r = mktime(&tm);
	}
	return r;
}

std::string TIridiumMessage::attrToString(int idx, const std::string &defval)
{
	std::stringstream ss;
	char dt[32];
	switch (idx)
	{
		case DATA_LONGITUDE:
			{
				TIELocation *l = getLocation();
				if (l == NULL)
					ss << DEFVAL;
				else
					ss << std::fixed << std::setprecision(PRECISION)
						<< l->getLongitude();
			}
			break;
		case DATA_DATA_PK:
			ss << VALIDATE_NULL(defval);
			break;
		case DATA_ID: 		// RAW(RAW_ID)
			ss << VALIDATE_NULL(defval);
			break;
		case DATA_DIRECT_ID:
			ss << "\'01\'";	// Identifier of IEIOHeader as char(2). Don't know what is it for.
			break;
		case DATA_PAYLOAD_ID:
			ss << "\'02\'";	// ;)
			break;
		case DATA_LOC_ID:
			ss << "\'03\'";	// ;)
			break;
		case DATA_SERVERTIME:
			{
				time_t t = readTime(defval);
				struct tm tm;
				localtime_s(&tm, &t);
				strftime(dt, sizeof(dt), dateformat[0], &tm);
				ss << "\'" << dt << "\'";
			}
			break;
		case DATA_PACKETDATE:
			{
				time_t t = readTime(defval);
				struct tm tm;
				localtime_s(&tm, &t);
				strftime(dt, sizeof(dt), dateformat[1], &tm);
				ss << "\'" << dt << "\'";
			}
			break;
		case DATA_IMEI:		// DEVICE(D_IMEI)
			ss << "\'" << TIMEI::toString(&getIEIOHeader()->imei) << "\'";
			break;
		case DATA_CDRREF:
			ss << getIEIOHeader()->cdrref;
			break;
		case DATA_DIRECT_DATE:
			ss << "(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + " << getIEIOHeader()->recvtime << " * INTERVAL '1 second')";
			break;
		case DATA_DIRECT_STAT:
			{
				uint8_t v = getIEIOHeader()->status;
				ss << "\'";
				bufferPrintHex(ss, &v, 1);
				ss << "\'";
			}
			break;
		case DATA_MOMSN:
			ss << getIEIOHeader()->sentno;
			break;
		case DATA_MTMSN:
			ss << getIEIOHeader()->recvno;
			break;
		case DATA_DIRECT_SIZE:
			if (getIOHeader() == NULL)
				ss << DEFVAL;
			else
				ss << getIOHeader()->getSize();
			break;
		case DATA_LOC_DIR:
			ss << "\'";
			bufferPrintHex(ss, getIELocation(), 1);
			ss << "\'";
			break;
		case DATA_CEPRADIUS:
			ss << getIELocation()->cepradius;
			break;
		case DATA_ASJSON:
			ss << "'" << toJson() << "'";
			break;
		case DATA_PROTVER:
			{
				TInfoElement *m = getMainPacket();
				int v;
				if (m)
					v = m->getVersion();
				else
					v = -1;
				ss << v;
			}
			break;
		case DATA_PROTSIZE:
			{
				TInfoElement *m = getMainPacket();
				int v;
				if (m)
					v = m->getSize();
				else
					v = -1;
				ss << v;
			}
			break;
		// -- location
		case DATA_LOC_SIZE:
			{
				TIELocation *l = getLocation();
				if (l == NULL)
					ss << DEFVAL;
				else
					ss << l->getSize();
			}
			break;
		case DATA_LATITUDE:
			{
				TIELocation *l = getLocation();
				if (l == NULL)
					ss << DEFVAL;
				else
					ss << std::fixed << std::setprecision(PRECISION)
						<< l->getLatitude();
			}
			break;
	}


	if (ss.rdbuf()->in_avail())
		return ss.str();

	TIEPayload *p = getPayload();

	if (p == NULL)
	{
		ss << DEFVAL;
		return ss.str();
	}

	switch (idx)
	{
		case DATA_SIZE:
			ss << p->getSize();
			break;
		case DATA_PAYLOAD_SIZE:
			ss << p->getSize();
			break;
		case DATA_PAYLOAD_TYPE:
			{
				uint8_t v = p->getPayload()->p1.packettype;
				ss << "\'";
				bufferPrintHex(ss, &v, 1);
				ss << "\'";
			}
			break;
		case DATA_PAYLOAD_1:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLatitude();	// p->getPayload()->p1.coord.lat;
			break;
		case DATA_PAYLOAD_2:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLongitude(); // p->getPayload()->p1.coord.lon;
			break;
		case DATA_PAYLOAD_3:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getAltitude(); // p->getPayload()->p1.coord.alt;
			break;
		case DATA_MSG_ID:
			{
				uint8_t v = p->getId();
				ss << "\'";
				bufferPrintHex(ss, &v, 1);
				ss << "\'";
			}
			break;
		case DATA_PAYLOAD_DATE:
			// Never used
			ss << "(SELECT TIMESTAMP WITH TIME ZONE 'epoch' + " << getIEIOHeader()->recvtime << " * INTERVAL '1 second')";
			break;
		case DATA_LAT:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLatitude();
			break;
		case DATA_LON:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLongitude();
			break;
		case DATA_H:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getAltitude();
			break;
		case DATA_VEF:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getVelocity();
			break;
		case DATA_AZF:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getAzimuth();
			break;
		case DATA_VHF:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getVelocityH();
			break;
		case DATA_LATF:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLatitude();
			break;
		case DATA_LONF:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLongitude();
			break;
		case DATA_HF:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getAltitude();
			break;
		case DATA_VE:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getVelocity();
			break;
		case DATA_AZ:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getAzimuth();
			break;
		case DATA_VH:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getVelocityH();
			break;
		case DATA_GDOP:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getGDOP();
			break;
		case DATA_PDOP:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getPDOP();
			break;
		case DATA_V:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getVoltage();
			break;
		case DATA_STATUS:
			ss << p->getStatus();
			break;
		case DATA_MCH:
			{
				int v = p->getMask();
				ss << "\'";
				bufferPrintHex(ss, &v, sizeof(v));
				ss << "\'";
			}
			break;
		case DATA_MCH_B:
			{
				int v = p->getMask();
				ss << "\'";
				bufferPrintBin(ss, &v, sizeof(v));
				ss << "\'";
			}
			break;
		case DATA_GDOP_H:
			{
				float v = p->getPacket(0x10)->p10.gdop;
				ss << "\'";
				bufferPrintHex(ss, &v, sizeof(float));
				ss << "\'";
			}
			break;
		case DATA_PDOP_H:
			{
				float v = p->getPacket(0x10)->p10.pdop;
				ss << "\'";
				bufferPrintHex(ss, &v, sizeof(float));
				ss << "\'";
			}
			break;
		case DATA_TIME_OK:
			ss << (p->isTimeCorrect() ? "\'OK\'" : DEFVAL);
			break;
		case DATA_GPS_OK:
			ss << (p->isNavDataCorrect() ? "\'OK\'" : DEFVAL);
			break;
		case DATA_V_H:
			{
				uint8_t v = p->getPacket(0x10)->p10.bat;
				ss << "\'";
				bufferPrintHex(ss, &v, sizeof(uint8_t));
				ss << "\'";
			}
			break;
		case DATA_TEXT_MESSAGE:
			ss << "'" << p->getText() << "'";
			break;
		case DATA_TEXT_NS:
			ss << "'" << p->getTextChunk() << "'";
			break;
		case DATA_TEXT_RESERV:
			ss << "'" << p->getTextReserved() << "'";
			break;
		case DATA_LAT1:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getLatitude();
			break;
		case DATA_TEMPERATURE:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getTemperature();
			break;
		case DATA_TEMPERATUREIN:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getTemperatureIn();
			break;
		case DATA_TEMPERATUREOUT:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getTemperatureOut();
			break;
		case DATA_BATTERY:
			ss << std::fixed << std::setprecision(PRECISION)
				<< p->getBattery();
			break;
	}
	return ss.str();
}

bool TIridiumMessage::valid()
{
	return (ioheader != NULL) || (mtioheader != NULL);	// && (location != NULL) && (payload != NULL)
}

bool TIridiumMessage::read(std::istream &value)
{
	TInfoElement *m = getMainPacket();
	if (m)
		m->read(value);
	if (ioheader)
		ioheader->read(value);
	if (location)
		location->read(value);
	if (payload)
		payload->read(value);
	return true;
}

bool TIridiumMessage::convert()
{
	TInfoElement::convert();
	if (ioheader)
		ioheader->convert();
	if (location)
		location->convert();
	if (payload)
		payload->convert();
	if (mtpayload)
		mtpayload->convert();
	if (mtioheader)
		mtioheader->convert();
	if (mtconfirmation)
		mtconfirmation->convert();
	return true;
}

void TIridiumMessage::extractMessageDescription(MessageDescription *buf)
{
	if (!buf)
		return;
	// DO not change buf->id = 0;
	TIOHeader *h = getIOHeader();
	if (h)
	{
		TIMEI::copy(buf->imei, h->getImei());
		buf->utc = h->getRecvtime();
	}

	TIELocation *l = getLocation();
	if (l)
	{
		buf->location.lat = l->getLatitude();
		buf->location.lon = l->getLongitude();
		// TODO		l->getCEPRadius();
		buf->location.hdop = 0;
		buf->location.pdop = 0;
	}

	TIEPayload *p = getPayload();
	if (p)
	{
		buf->packettype = p->getId();
		if (p->hasLLA())
		{
			p->getLLA().putGeo(&buf->location);
		}
		if (p->hasTime())
		{
			buf->gpstime = p->getTime();
		}
	}
}

TIEPayload *TIridiumMessage::getPayload()
{
	return payload;
}

TIOHeader *TIridiumMessage::getIOHeader()
{
	return ioheader;
}

TIELocation *TIridiumMessage::getLocation()
{
	return location;
}

//------------------- TIridiumMessageBuild

// for compatibity with older c++ compilers
void TIridiumMessageBuild::init
(
	IEIOHeader *header,
	IEMTIOHeader *mtheader,
	IEMTConfirmation *mtconfirm,
	IELocation *loc,
	packet_t *data
)
{
	buffer.resize(81);
	setData(&buffer[0]);

	packet = new TInfoElement(&buffer[0]);
	packet->setId(1);

	int ofs = 3;

	if (header)
	{
		ioheader = new TIOHeader(&buffer[ofs]);		// 3
		IEIOHeader *io = ioheader->getIOHeader();
		memmove(io, header, sizeof(IEIOHeader));	// +28
		ofs += 28 + 3;
	}

	if (loc)
	{
		location = new TIELocation(&buffer[ofs]);	// 34
		IELocation *l = location->getLocation();
		memmove(l, loc, sizeof(IELocation));		// +11
		ofs += 11 + 3;
	}

	if (mtheader)
	{
		mtioheader = new TMTIOHeader(&buffer[ofs]);		// 3
		IEMTIOHeader *mtio = mtioheader->getMTIOHeader();
		memmove(mtio, mtheader, sizeof(IEMTIOHeader));		// +21
		ofs += 21 + 3;
	}

	if (mtconfirm)
	{
		mtconfirmation = new TMTConfirmation(&buffer[ofs]);		// 3
		IEMTConfirmation *mtconf = mtconfirmation->getMTConfirmation();
		memmove(mtconf, mtconfirm, sizeof(IEMTConfirmation));		// +25
		ofs += 25 + 3;
	}

	// Different on MO or MT message
	if (data)
	{
		packet_t *p;
		if (mtheader)
		{
			mtpayload = new TMTPayload(&buffer[ofs]);		// 48
			p = mtpayload->getPayload();
		}
		else
		{
			payload = new TIEPayload(&buffer[ofs]);			// 48
			p = payload->getPayload();
		}
		memmove(p, data, sizeof(packet_t));
		ofs += 30 + 3;
	}

	packet->setSize(ofs - 3);				// 28 + 11 + 30 = 69 + 3 * 3 = 78
}

TIridiumMessageBuild::TIridiumMessageBuild() : TIridiumMessage()
{
	init((IEIOHeader *)NULL, (IEMTIOHeader *)NULL, (IEMTConfirmation *)NULL, (IELocation *)NULL, (packet_t *) NULL);
}

void TIridiumMessageBuild::copy(const PacketRawBuffer &b)
{
	copy(&b[0], b.size());
}

void TIridiumMessageBuild::copy
(
	const void *b,
	size_t size
)
{
	if (size <= 0)
		return;
	buffer.resize(size < 81 ? 81 : size );
	data = (InfoElement *) &buffer[0];
	memcpy(data, b, size);
	set(data, size);
}

TIridiumMessageBuild::TIridiumMessageBuild(const void *buffer, size_t size)
{
	copy(buffer, size);
}

TIridiumMessageBuild::TIridiumMessageBuild(const PacketRawBuffer &b)
	: TIridiumMessage()
{
	copy(b);
}

TIridiumMessageBuild::TIridiumMessageBuild(const char* hexpacket)
	: TIridiumMessage()
{
	PacketRawBuffer b = Dump::readHexStr(hexpacket);
	copy(b);
}

TIridiumMessageBuild::TIridiumMessageBuild(
	IEIOHeader *header,
	IELocation *loc,
	packet_t *data
) : TIridiumMessage()
{
	init(header, NULL, NULL, loc, data);
}

TIridiumMessageBuild::TIridiumMessageBuild(
	IEMTIOHeader *header,
	packet_t *data
) : TIridiumMessage()
{
	init((IEIOHeader *) NULL, header, (IEMTConfirmation *) NULL, (IELocation *) NULL, data);
}

TIridiumMessageBuild::TIridiumMessageBuild(
	TIOHeader *header,
	TIELocation *loc,
	TIEPayload *payload
) : TIridiumMessage()
{
	init(header->getIOHeader(), (IEMTIOHeader*) NULL, (IEMTConfirmation *) NULL, loc->getLocation(), payload->getPayload());
}

TIridiumMessageBuild::TIridiumMessageBuild(
		const std::string &v
) : TIridiumMessage()
{
	init((IEIOHeader *)NULL, (IEMTIOHeader*) NULL, (IEMTConfirmation *) NULL, (IELocation *)NULL, (packet_t *) NULL);
	readString(v);
}

TIridiumMessageBuild::TIridiumMessageBuild(
		const std::vector<std::string> &v
) : TIridiumMessage()
{
	std::stringstream pars;
	for (std::vector<std::string>::const_iterator it = v.begin(); it != v.end(); ++it)
		pars << *it << " ";
	init((IEIOHeader *)NULL, (IEMTIOHeader*) NULL, (IEMTConfirmation *) NULL, (IELocation *)NULL, (packet_t *) NULL);
	readString(pars.str());
}

TIridiumMessageBuild::~TIridiumMessageBuild()
{

}

/*
 * iridiummessageparser.h
 *
 *  Created on: 03.11.2015
 *      Author: andrei
 */

#ifndef IRIDIUMMESSAGEPARSER_H_
#define IRIDIUMMESSAGEPARSER_H_

#include "stdlib.h"
#include "iridiummessage.h"

/**
 * Return hex formatted data.
 * ret can be NULL
 * Return size of buffer
 * insert into raw(data, who, garbage) values('%s', '%s', true) RETURNING raw_id", data, who);}
 *
 * Usage:
 *
 *	PacketRawBuffer b = ...
 *	// get size
 *	int sz1 = iridiummessage2hex(b.data(), b.size(), NULL, 0);
 *	// TODO check sz != -1
 *	// allocate memory
 *	char *pp = (char*)malloc(sz1);
 *	// get hex string into allocated buffer
 *	iridiummessage2hex(b.data(), b.size(), pp, sz1);
 *	// TODO do smth
 *	// free memory
 *	free(pp);
 *
 */
CALLC int iridiummessage2hex(void *data, size_t size, char* ret, size_t retsize);

/**
 * Parse buffer. Return payload only.
 * Return -1 if message is incomplete or damaged.
 * result can be NULL
 * Return size of buffer
 * Important note: This function is destructive even result is NULL!
 * Make copy of the buffer if you want use them later.
 * Changes data buffer content by swapping bytes in 2, 4, 8 bytes words.
 *
 * Usage:
 *  	char *bs;
 *	int columns[] = {
 *			DATA_PROTVER, DATA_PROTSIZE, DATA_DIRECT_ID, DATA_DIRECT_SIZE,
 *			DATA_CDRREF, DATA_IMEI, DATA_DIRECT_STAT, DATA_MOMSN, DATA_MTMSN,
 *			DATA_DIRECT_DATE, DATA_LOC_ID, DATA_LOC_SIZE, DATA_LOC_DIR,
 *			DATA_LATITUDE, DATA_LONGITUDE, DATA_CEPRADIUS, DATA_PAYLOAD_ID,
 *			DATA_PAYLOAD_SIZE, DATA_PAYLOAD_TYPE,
 *			DATA_LATF, DATA_LONF, DATA_HF, DATA_ID, DATA_PACKETDATE,
 *			DATA_END};
 *	// get size to allocate
 *	int sz = message2PCharClause(NULL, 0, 3, columns,
 *		bb.data(), bb.size(), 999999);
 *	// TODO check sz != -1
 *	// allocate memory
 *	bs = (char *) malloc(sz);
 *	// get INSERT clause
 *	// TODO get copy of bb into bb2
 *	// TODO get id of raw
 *	message2PCharClause(bs, sz, 3, columns,
 *			bb2.data(), bb2.size(), id);
 *	// TODO do smth
 *	// free memory
 *	free(bs);
 */
CALLC int iridiummessageparsebuffer(void *data, size_t size, int* packetid, packet_t **result);

#endif /* IRIDIUMMESSAGEPARSER_H_ */

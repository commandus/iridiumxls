/*
 * IridiumMessage.h
 *
 *  Created on: 28.10.2015
 *      Author: andrei
 */

#ifndef IRIDIUMMESSAGE_H_
#define IRIDIUMMESSAGE_H_

#include <vector>
#include <iostream>
#include <istream>
#include <ostream>
#include <map>

#include "iridiumpacket.h"
#include "dump.h"
#include "geolocation.h"

#define MAX_ELEMENTS 4

/**
 * Initialize default Iridium message elements in case of received message is invalid.
 * If you don't, get garbage ;)
 */
void initIridium();

/**
 * Helper function return Iridium message header human readable name
 */
const char *IridiumInfoElementType2String(IridiumInfoElementType value);

class TIMEI
{
private:
	IMEI data;
public:
	TIMEI(std::string &v);
	IMEI *get();
	static std::string toString(IMEI *element);
	static void copy(char* dest, IMEI *element);
	static void copy(IMEI* dest, IMEI *element);
	static void copy(IMEI* dest, const char *element);
};

class TInfoElement
{
protected:
	InfoElement *data;
public:
	virtual bool convert();
	TInfoElement();
	TInfoElement(void *value);
	TInfoElement(const void *buffer, size_t size);
	TInfoElement(const PacketRawBuffer &b);
	virtual ~TInfoElement();
	uint8_t setVector(const PacketRawBuffer &b);
	void set(TInfoElement &value);
	bool readString(const std::string &value);
	virtual bool read(std::istream &value);
	virtual uint8_t set(const void *buffer, size_t size);
	void clear();
	IridiumInfoElementType getType();
	int getVersion();
	std::string getTypeString();
	std::string getPayloadTypeString(int v);
	uint16_t getSize();
	uint16_t getContainerSize();
	void setSize(uint16_t value);
	void setData(const void *value);
	void setId(uint8_t value);
	InfoElement *getData() { return data; };
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	std::string toHexString();
	virtual std::string toString();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
};

class TIOHeader : public TInfoElement
{
public:
	TIOHeader(void *value);
	TIOHeader(TInfoElement &element);
	IEIOHeader *getIOHeader();
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toString();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
	virtual bool read(std::istream &value);
	virtual bool convert();
	// getters
	uint32_t getCdrref();		//< Iridium message identifier				4
	IMEI *getImei();		//< see IMEI						15
	std::string getImeiString();
	uint8_t getStatus();		//< see IOStatus					1
	uint16_t getSentno();		//< momsn modem sent out packet number			2
	uint16_t getRecvno();		//< mtmsn modem received out packet number. Not used		2
	uint32_t getRecvtime();		//< stime Unix epoch time, seconds since 1970			4
	// setters
	void setCdrref(uint32_t v);	//< Iridium message identifier				4
	void setImei(IMEI *v);		//< see IMEI						15
	void setStatus(uint8_t v);	//< see IOStatus					1
	void setSentno(uint16_t v);	//< momsn modem sent out packet number			2
	void setRecvno(uint16_t v);	//< mtmsn modem received out packet number. Not used		2
	void setRecvtime(uint32_t v);	//< stime Unix epoch time, seconds since 1970			4
};

class TMTIOHeader : public TInfoElement
{
public:
	TMTIOHeader(void *value);
	TMTIOHeader(TInfoElement &element);
	IEMTIOHeader *getMTIOHeader();
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toString();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
	virtual bool read(std::istream &value);
	virtual bool convert();
	// getters
	std::string getMsgid();		//< get unique client message identifier			4
	IMEI *getImei();		//< get IMEI, see IMEI						15
	std::string getImeiString();
	uint16_t getFlags();		//< get disposition flags: 1- delete all MT payloads in queue, 2- send ring alert - with no associated payload
	// setters
	void setMsgid(const std::string & v);	//< unique client message identifier				4
	void setImei(IMEI *v);		//< set IMEI, see IMEI						15
	void setFlags(uint16_t v);	//< set disposition flags: 1- delete all MT payloads in queue, 2- send ring alert - with no associated payload
	static std::string msgIdAsString(IEMTIOHeader *header);
	static void setMsgId(IEMTIOHeader *header, const std::string &v);	//< unique client message identifier				4
};

class TMTPayload : public TInfoElement
{
public:
	TMTPayload(void *value);
	TMTPayload(TInfoElement &element);
	packet_t *getPayload();
	virtual bool convert();
	virtual bool read(std::istream &value);
	int getId();
	void setPayloadId(int id);
	static uint16_t getRequiredSize(int id);
	bool valid();
	packet88_t *get88() { if (valid()) return (packet88_t*) getPayload(); else return NULL; };
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toString();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
	// check packet identifier, if not return default empty packet
	packet_t *getPacket(int id);
};

class TMTConfirmation: public TInfoElement
{
public:
	TMTConfirmation(void *value);
	TMTConfirmation(TInfoElement &element);
	IEMTConfirmation *getMTConfirmation();
	virtual bool convert();
	virtual bool read(std::istream &value);
	static uint16_t getRequiredSize(int id);
	bool valid();
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toString();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
	// getters
	uint32_t getMsgid();		//< get unique client message identifier			4
	IMEI *getImei();		//< get IMEI, see IMEI						15
	std::string getImeiString();
	uint32_t getIdref();		//< get reference identifier					4
	int16_t getStatus();		//< get disposition flags: 1- delete all MT payloads in queue, 2- send ring alert - with no associated payload
	// setters
	void setMsgid(uint32_t v);	//< unique client message identifier				4
	void setImei(IMEI *v);		//< set IMEI, see IMEI						15
	void setIdref(uint32_t v);	//< set reference identifier
	void setStatus(int16_t v);	//< set status
};

class TIELocation : public TInfoElement
{
public:
	TIELocation(void *value);
	TIELocation(TInfoElement &element);
	IELocation *getLocation();
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
	virtual std::string toString();
	virtual bool convert();
	virtual bool read(std::istream &value);
	// getters
	double getLatitude();
	double getLongitude();
	uint32_t getCEPRadius();
	// setters
	void setLatitude(double v);
	void setLongitude(double v);
	void setCEPRadius(uint32_t v);
};

class TIEPayload : public TInfoElement
{
public:
	TIEPayload(void *value);
	TIEPayload(TInfoElement &element);
	packet_t *getPayload();
	virtual bool convert();
	virtual bool read(std::istream &value);
	int getId();
	void setPayloadId(int id);
	static uint16_t getRequiredSize(int id);
	bool valid();
	packet0_t *get0() { if (valid()) return (packet0_t*) getPayload(); else return NULL; };
	packet1_t *get1() { if (valid()) return (packet1_t*) getPayload(); else return NULL; };
	packet2_t *get2() { if (valid()) return (packet2_t*) getPayload(); else return NULL; };
	packet3_t *get3() { if (valid()) return (packet3_t*) getPayload(); else return NULL; };
	packet4_t *get4() { if (valid()) return (packet4_t*) getPayload(); else return NULL; };
	packet5_t *get5() { if (valid()) return (packet5_t*) getPayload(); else return NULL; };
	packet6_t *get6() { if (valid()) return (packet6_t*) getPayload(); else return NULL; };
	packet7_t *get7() { if (valid()) return (packet7_t*) getPayload(); else return NULL; };
	packet8_t *get8() { if (valid()) return (packet8_t*) getPayload(); else return NULL; };
	packet9_t *get9() { if (valid()) return (packet9_t*) getPayload(); else return NULL; };
	packet10_t *get10() { if (valid()) return (packet10_t*) getPayload(); else return NULL; };
	packet88_t *get88() { if (valid()) return (packet88_t*) getPayload(); else return NULL; };
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toString();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);

	// check packet identifier, if not return default empty packet
	packet_t *getPacket(int id);

	// is
	bool hasTime();
	bool hasLLA();
	// getters
	time_t getTime();
	GeoLocation getLLA();
	// setters
	void setLLA(GeoLocation *v);
	void setTime(time_t v);
	void setTime(time5 *v);

	// payload getters
	double getLatitude();
	double getLongitude();
	double getAltitude();
	double getVelocity();
	double getAzimuth();
	double getVelocityH();

	void setLatitude(double v);
	void setLongitude(double v);
	void setAltitude(double v);
	void setVelocity(double v);
	void setAzimuth(double v);
	void setVelocityH(double v);

	// 8
	bool isGpsOldData();
	bool isGpsEncoded();
	bool isGpsFromMemory();
	bool isGpsNoFormat();
	bool isGpsNoSats();
	bool isGpsBadHdop();
	bool isGpsTime();
	bool isGpsNavData();
	bool isAlarmLow();
	bool isAlarmHigh();

	bool isFailurepower();		//< 22 device status power loss
	bool isFailureeep();		//< EEPROM failure
	bool isFailureclock();		//< clock not responding
	bool isFailurecable();		//< MAC not responding
	bool isFailureint0();		//< clock int failure
	bool isFailurewatchdog();	//< software failure
	bool isFailurenoise();		//< blocks in memory found
	bool isFailureworking();	//< device was

	int getSats();
	float getBattery();
	int getTemperature();
	int getR2();
	int getKey();

	float getGDOP();
	float getHDOP();
	float getPDOP();

	// 0x10
	int getMask();
	int getGpsReserved();
	float getVoltage();
	bool isVoltageLow();
	bool isVoltageHigh();
	bool isTimeCorrect();
	bool isNavDataCorrect();
	bool isPowerMinus();
	bool isPowerMinus2();
	float getVoltageExt();
	float getCurrent();
	float getTemperatureIn();
	float getTemperatureOut();
	int getStatus();

	// 0x11
	std::string getText();
	int getTextId();
	int getTextChunk();
	int getTextCodepage();
	int getTextReserved();

	// 8
	void setIsGpsOldData(bool v);
	void setIsGpsEncoded(bool v);
	void setIsGpsFromMemory(bool v);
	void setIsGpsNoFormat(bool v);
	void setIsGpsNoSats(bool v);
	void setIsGpsBadHdop(bool v);
	void setIsGpsTime(bool v);
	void setIsGpsNavData(bool v);

	void setIsFailurepower(bool v);
	void setIsFailureeep(bool v);
	void setIsFailureclock(bool v);
	void setIsFailurecable(bool v);
	void setIsFailureint0(bool v);
	void setIsFailurewatchdog(bool v);
	void setIsFailurenoise(bool v);
	void setIsFailureworking(bool v);

	void setSats(int v);
	void setIsAlarmLow(bool v);
	void setIsAlarmHigh(bool v);
	void setBattery(float v);
	void setTemperature(int v);
	void setR2(int v);
	void setKey(int v);

	// 0x10
	void setGpsReserved(int v);
	void setMask(uint32_t v);
	void setGDOP(float v);
	void setPDOP(float v);
	void setVoltage(float v);
	void setIsVoltageLow(bool v);
	void setIsVoltageHigh(bool v);
	void setIsTimeCorrect(bool v);
	void setIsNavDataCorrect(bool v);
	void setIsPowerMinus(bool v);
	void setIsPowerMinus2(bool v);
	void setVoltageExt(float v);
	void setCurrent(float v);
	void setTemperatureIn(float v);
	void setTemperatureOut(float v);
	void setStatus(int v);
	void setBatteryStatus(int v);
	void setBatteryExt(float v);
	void setReserved(int v);
	// 0x11
	void setText(std::string &v);
	void setTextId(int v);
	void setTextChunk(int v);
	void setTextCodepage(int v);
	void setTextReserved(int v);
};

class TIridiumMessage : public TInfoElement
{
private:
	int gpsTimeZone;
protected:
	TInfoElement *packet;
	TIOHeader *ioheader;
	TIELocation *location;
	TIEPayload *payload;

	TMTIOHeader *mtioheader;
	TMTPayload *mtpayload;
	TMTConfirmation *mtconfirmation;
public:
	TIridiumMessage();
	TIridiumMessage(const void *buffer, size_t size, int tz);
	TIridiumMessage(const PacketRawBuffer &b, int tz);
	virtual ~TIridiumMessage();
	virtual uint8_t set(const void *buffer, size_t size);
	virtual void toStream(std::ostream &result, const bool noconversion = false);
	virtual std::string toString();
	std::string toHex();
	virtual std::string toJson();
	virtual void toStrings(std::map<std::string, std::string>&map);
	std::string attrToString(int idx, const std::string &defval);
	int getSessionStatus();	// 0- OK, 13- Incomplete Transfer
	TIEPayload *getPayload();
	TIOHeader *getIOHeader();
	TIELocation *getLocation();

	TMTPayload *getMTPayload();
	TMTIOHeader *getMTIOHeader();
	TMTConfirmation *getMTConfirmation();

	// get Iridium "main" packet
	TInfoElement *getMainPacket();
	IEIOHeader *getIEIOHeader();
	IELocation *getIELocation();

	IEMTIOHeader *getIEMTIOHeader();
	IEMTConfirmation *getIEConfirmation();

	bool valid();
	virtual bool read(std::istream &value);
	virtual bool convert();

	void extractMessageDescription(MessageDescription *buf);
};

class TIridiumMessageBuild : public TIridiumMessage
{
private:
	PacketRawBuffer buffer;
	void copy(const void *b, size_t size);
	void copy(const PacketRawBuffer &b);
public:
	TIridiumMessageBuild();
	TIridiumMessageBuild(const void *buffer, size_t size);
	TIridiumMessageBuild(const PacketRawBuffer &b);
	TIridiumMessageBuild(IEIOHeader *header, IELocation *loc, packet_t *data);
	TIridiumMessageBuild(IEMTIOHeader *header, packet_t *data);
	TIridiumMessageBuild(TIOHeader *header, TIELocation *loc, TIEPayload *payload);
	/// from space delimited string
	TIridiumMessageBuild(const std::string &v);
	/// from packet in hex string
	TIridiumMessageBuild(const char *hexstr);
	TIridiumMessageBuild(const std::vector<std::string> &v);
	void init(IEIOHeader *header, IEMTIOHeader *mtheader, IEMTConfirmation *mtconfirm, IELocation *loc, packet_t *data);
	virtual ~TIridiumMessageBuild();
};

#endif /* IRIDIUMMESSAGE_H_ */


/*
 * checkuptime.h
 *
 *  Created on: 26.10.2015
 *      Author: andrei
 */

#ifndef CHECKUPTIME_H_
#define CHECKUPTIME_H_

#include <stdlib.h>

size_t mkPacketUptimeRequest(char *buf, size_t size, int id);

#endif /* CHECKUPTIME_H_ */

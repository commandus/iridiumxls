/*
 * geolocation.cpp
 *
 *  Created on: 30.10.2015
 *      Author: andrei
 */

#include <string>
#include <iomanip>
#include <sstream>
#include "geolocation.h"

GeoLocation::GeoLocation(geoCoordType toCoord, double alat, double alon)
{
	switch (toCoord)
	{
		case GC_RADIANS:	// rad to degree
			lat = alat * GPS_PI / 180;
			lon = alon * GPS_PI / 180;
			break;
		default:
			lat = alat * 180 / GPS_PI;
			lon = alon * 180 / GPS_PI;
	}
}

GeoLocation::GeoLocation(geoCoordType toCoord, const GeoLocation& value)
{
	switch (toCoord)
	{
		case GC_RADIANS:	// rad to degree
			lat = value.lat * GPS_PI / 180;
			lon = value.lon * GPS_PI / 180;
			break;
		default:
			lat = value.lat * 180 / GPS_PI;
			lon = value.lon * 180 / GPS_PI;
	}
	hdop = value.hdop;
	pdop = value.pdop;
}

GeoLocation::GeoLocation(gps_coord_t *value)
{
	geo v;
	gps_coord_t2geo(&v, value);
	lat = v.lat;
	lon = v.lon;
	hdop = v.hdop;
	pdop = v.pdop;
	hasAlt = false;
}

GeoLocation::GeoLocation(geo3 *v)
{
	lat = v->lat;
	lon = v->lon;
	alt = v->alt;
	hdop = 0;
	pdop = 0;
	hasAlt = true;
}

std::string GeoLocation::toString()
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision(PRECISION)
		<< lat << ", " << lon;
	if (hasAlt)
		ss << std::fixed << std::setprecision(PRECISION) << ", " << alt;
	ss << ", " << (int) hdop << ", " << (int) pdop;

	return ss.str();
}

std::string GeoLocation::toJson()
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision(PRECISION)
		<< "\"lat\":" << lat << ",\"lon\":" << lon;
	if (hasAlt)
		ss << std::fixed << std::setprecision(PRECISION) << ",\"alt\":" << alt;
	ss << ",\"hdop\":" << (int) hdop << ",\"pdop\":" << (int) pdop;
	return ss.str();
}

template <typename T>
inline static std::string to_string(T v)
{
	std::ostringstream s;
	s << std::fixed << std::setprecision(PRECISION) << v;
	return s.str();
}

void GeoLocation::toStrings(std::map<std::string, std::string> &map)
{
	map["lat"] = to_string(lat);
	map["lon"] = to_string(lon);
	map["alt"] = to_string(alt);
	map["hdop"] = to_string(hdop);
	map["pdop"] = to_string(pdop);
}

void GeoLocation::putGps_coord_t(gps_coord_t *r)
{
	if (r == NULL)
		return;
	geo g;
	g.lat = lat;
	g.lon = lon;
	g.hdop = hdop;
	g.pdop = pdop;
	geo2gps_coord_t(r, &g);
}

void GeoLocation::putGeo3(geo3 *r)
{
	if (r == NULL)
		return;
	r->lat = lat;
	r->lon = lon;
	r->alt = alt;
}

void GeoLocation::putGeo(geo *r)
{
	if (r == NULL)
		return;
	r->lat = lat;
	r->lon = lon;
	r->pdop = pdop;
	r->hdop = hdop;
}

/*
 * iridiumpacket.c
 *
 *  Created on: 30.10.2015
 *      Author: andrei
 */

#include <math.h>
#include "iridiumpacket.h"

time_t time52time_t(time5 *src)
{
	struct tm info;
	info.tm_year = (src->year) + 2000 - 1900;
	info.tm_mon = src->month - 1;	// 1..12->0..11
	info.tm_mday = src->day;	// 1..31
	info.tm_hour = src->hour;
	info.tm_min = src->minute;
	info.tm_sec = src->second;
	info.tm_isdst = -1;
	return mktime(&info);
}

void time_t2time5(time5 *dest, time_t src)
{
	struct tm tm;
	localtime_s(&tm, &src);
	dest->year = tm.tm_year - 100;
	dest->month = tm.tm_mon + 1;
	dest->day = tm.tm_mday;
	dest->hour = tm.tm_hour;
	dest->minute = tm.tm_min;
	dest->second = tm.tm_sec;
}

void gps_coord_t2geo(geo *dest, gps_coord_t *src)
{
	int lats = src->latitude_s > 9999 ? 9999 : src->latitude_s;
	int lons = src->longitude_s > 9999 ? 9999 : src->longitude_s;
	dest->lat = src->latitude_g + ((src->latitude_m + (lats * 0.0001)) / 60.);
	if (src->latitude_ns == 'S')
		dest->lat = - dest->lat;

	dest->lon = src->longitude_g + ((src->longitude_m + (lons * 0.0001)) / 60.);
	if (src->longitude_ew == 'W')
		dest->lon = - dest->lon;
	dest->hdop = src->hdop;
	dest->pdop = src->pdop;
}

void geo2gps_coord_t(gps_coord_t *dest, geo *src)
{
	double la = fabs(src->lat);
	dest->latitude_g = (uint8_t) la;
	dest->latitude_m = (uint8_t) la * 60 - dest->latitude_g * 60;
	dest->latitude_s = (uint16_t) la * 60 * 1000 - dest->latitude_g * 60 * 1000 - dest->latitude_m * 1000;
	dest->latitude_s1 = 0;	// reserved
	dest->latitude_ns = src->lat > 0 ? 'N' : 'S';

	double lo = fabs(src->lon);
	dest->longitude_g = (uint8_t) lo;
	dest->longitude_m = (uint8_t) lo * 60 - dest->longitude_g * 60;
	dest->longitude_s = (uint16_t) lo * 60 * 1000 - dest->longitude_g * 60 * 1000 - dest->longitude_m * 1000;
	dest->longitude_s1 = 0;	// reserved
	dest->longitude_ew = src->lon > 0 ? 'E' : 'W';

	dest->hdop = src->hdop;
	dest->pdop = src->pdop;
}

/*
 * checkuptime.cpp
 *
 *  Created on: 26.10.2015
 *      Author: andrei
 */

#include <stdio.h>
#include "checkuptime.h"

size_t mkPacketUptimeRequest(char *buf, size_t size, int id)
{
	size_t r;
	r = snprintf(buf, size, "UPTIME:%d.%d.%d.%d:%d", 84, 237, 111, 10, id);
	return r;
}

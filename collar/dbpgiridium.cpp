/*
 * dbpgiridium.cpp
 *
 *  Created on: 06.11.2015
 *      Author: andrei
 */

#include <string>
#include <sstream>
#include <map>
#include <string.h>
#include <cstdlib>
#include "dump.h"
#include "dbpgiridium.h"
#include "iridiummessage.h"
#include "iridiumpacketattr.h"

#ifdef _MSC_VER
#else
#include <alloca.h>
#define strtok_s(str, separators, nexttoken) strtok(str, separators)
#endif

// Iridium raw message table "raw"
/*
static const char* columnNamesRaw[] = {
	"raw_id",	//< identifier
	"when",
	"data",
	"is_full_processed",
	"test",
	"garbage",
	"loctime",
	"who"
};
*/
// Iridium parsed message table "data"
#define COUNT_RAW_COLUMNS	60
static const char* columnNamesData[COUNT_RAW_COLUMNS] = {
	"data_pk",	//< 0 message identifier
	"id", 		//< 1 raw(raw_id)
	"imei",		//< 2 device(d_imei)
	"size",		//< 3
	"direct_id",	//< 4
	"direct_size",	//< 5
	"cdrref",	//< 6
	"direct_stat",	//< 7
	"momsn", 	//< 8
	"mtmsn", 	//< 9
	"direct_date",	//< 10
	"loc_id",	//< 11
	"loc_size", 	//< 12
	"loc_dir", 	//< 13
	"latitude", 	//< 14
	"longitude", 	//< 15
	"cepradius",	//< 16
	"payload_id",	//< 17
	"payload_size",	//< 18
	"payload_type",	//< 19
	"payload_1",	//< 20
	"payload_2",	//< 21
	"payload_3",	//< 22
	"msg_id",	//< 23
	"payload_date",	//< 24
	"lat",		//< 25
	"lon",		//< 26
	"h",		//< 27
	"vef",		//< 28
	"azf",		//< 29
	"vhf",		//< 30
	"latf",		//< 31
	"lonf",		//< 32
	"hf",		//< 33
	"servertime",	//< 34
	"packetdate",	//< 35
	"protver",	//< 36
	"protsize",	//< 37
	"ve",		//< 38
	"az",		//< 39
	"vh",		//< 40
	"mch",		//< 41
	"gdop",		//< 42
	"pdop",		//< 43
	"v",		//< 44
	"status",	//< 45
	"mch_b",	//< 46
	"gdop_h",	//< 47
	"pdop_h",	//< 48
	"time_ok",	//< 49
	"gps_ok",	//< 50
	"v_h",		//< 51
	"text_message",	//< 52
	"text_ns", 	//< 53
	"text_reserv",	//< 54
	"lat1",		//< 55
	"temperature",	//< 56
	"temperaturein",//< 57
	"temperatureout", //< 58
	"battery" //< 59
};

#define VALID_COLUMN_DATA(p)	((p != NULL) && (*p >= 0) && (*p < COUNT_RAW_COLUMNS))
/*
 *  Return to result allocated buffer with values as string
 *  retimei can be NULL. Return IMEI to
 *  style:
 *  	0- column names
 *  	1- values
 *  	2- select clause
 *  	3- insert clause
 *	4- hex
 *	5- string
 *	6- JSON
 *
 * columns can be NULL
 * defvals can not NULL!
 *	Return [required] size of buffer, <0 - error code
 *
 */
std::string message2Clause(
	MessageDescription* ret,
	int style, int *columns, void *value, size_t sz,
	std::map<int, std::string> &defvals, int tz)
{
	std::stringstream r;
	int *p = columns;

	switch (style)
	{
		case SQL_CT_SELECT:
			r << "SELECT ";
			break;
		case SQL_CT_INSERT:
			r << "INSERT INTO data(";
			break;
	}

	// column names
	switch (style)
	{
		case SQL_CT_COLNAMES:
		case SQL_CT_SELECT:
		case SQL_CT_INSERT:
		{
			while (VALID_COLUMN_DATA(p))
			{

				r << "\"" << columnNamesData[*p] << "\"";
				p++;
				if (VALID_COLUMN_DATA(p))
					r << ", ";
			}
		}
		break;
	}

	switch (style)
	{
		case SQL_CT_SELECT:
			r << " FROM data";
			break;
		case SQL_CT_INSERT:
			r << ") VALUES (";
			break;
	}

	// values
	switch (style)
	{
		case SQL_CT_VALUES:
		case SQL_CT_INSERT:
			{
				TIridiumMessage m(value, sz, tz);
				m.extractMessageDescription(ret);
				p = columns;
				int i = 0;
				while (VALID_COLUMN_DATA(p))
				{
					if (m.valid())
						r << m.attrToString(*p, defvals[*p]);
					else
						r << "NULL";
					p++;
					i++;
					if (VALID_COLUMN_DATA(p))
						r << ", ";
				}
				r << ")";
			}
			break;
		case SQL_CT_HEX:
			{
				// first dump!
				r << Dump::hexString(value, sz);
				// (it is destructive)
				TIridiumMessage m(value, sz, tz);
				m.extractMessageDescription(ret);
			}
			break;
		case SQL_CT_TEXT:
			{
				TIridiumMessage m(value, sz, tz);
				m.extractMessageDescription(ret);
				r << m.toString();
			}
			break;
		case SQL_CT_JSON:
			{
				TIridiumMessage m(value, sz, tz);
				m.extractMessageDescription(ret);
				r << m.toJson();
			}
			break;
		case SQL_CT_CONVERT:
			{
				TIridiumMessage m(value, sz, tz);
				m.extractMessageDescription(ret);
				m.toStream(r, true);
			}
			break;
	}
	return r.str();
}

/*
 *  Return to result allocated buffer with values as string
 *  style:
 *  	0- column names
 *  	1- values
 *  	2- select clause
 *  	3- insert clause
 *	4- hex
 *	5- text
 *	6- JSON
 *
 *	Return [required] size of buffer, <0 - error code
 *		-1 insufficient space
 */
CALLC int message2PCharClause(void *value, size_t size, int style, int *columns,
	MessageDescription* ret, void *result, size_t dsz, int id, int tz,
	const char *t
)
{
	std::map<int, std::string> defvals;
	defvals[DATA_ID] = itoa(id);
	defvals[DATA_DIRECT_SIZE] = "0";
	defvals[DATA_LOC_SIZE] = "0";
	defvals[DATA_PAYLOAD_SIZE] = "0";
	std::string timestamp(t);
	if (!timestamp.empty()) {
		defvals[DATA_SERVERTIME] = timestamp;
		defvals[DATA_PACKETDATE] = timestamp;
	}
	

	// DEFVAL
	std::string r = message2Clause(ret, style, columns, value, size, defvals, tz);
	size_t sz = r.length() + 1; // plus trailing \0

	// std::cerr << "Required size: " << sz << " avaliable: " << dsz << std::endl;
	if (result != NULL)
	{
		if (sz <= dsz)
		{
			memmove(result, r.c_str(), r.length());
			((char *)result)[r.length()] = 0;
			return (int) sz;
		}
		else
			return -1;
	}
	else
		return (int) sz;
}

CALLC int message2PCharClause2(void *value, size_t size, int style, int *columns,
	MessageDescription* ret, void *result, size_t dsz, int id, int tz,
	const char *timestamp
)
{
	if (size == 0)
		return -1;
	void *p = alloca(size);
	memmove(p, value, size);
	return message2PCharClause(p, size, style, columns, ret, result, dsz, id, tz, timestamp); // no free(p);
}

const char *MESG_ARRAY_DELIM = "|";
const char *PARAM_DELIM = ";";

CALLC int appCommands2Packet88(packet88_t *ret, char *cmds, char* pars)
{
	// header
	memset(ret, 0, sizeof(packet88_t));
	ret->packettype = 0x88;
	// массив байт - ключ контроля  0x8C ( М)  0xAE (о)  0xF0( Ё )
	ret->keyMessageType[0] = 0x8C;
	ret->keyMessageType[1] = 0xAE;
	ret->keyMessageType[2] = 0xF0;

	// set commands & set command count
	if (cmds == NULL)
		ret->cmdcount = 0;
	else
	{
#ifdef _MSC_VER
		char *next_token = NULL;
#endif
		char *p = strtok_s(cmds, MESG_ARRAY_DELIM, &next_token);
		while ((ret->cmdcount < 3) && (p != NULL))
		{
			cmdmessage *c = &ret->cmd[ret->cmdcount];
			c->command = atoi(p);				// 1 байт сама команда
			ret->cmdcount++;
			p = strtok_s(NULL, MESG_ARRAY_DELIM, &next_token);
		}
	}
	// set lencmd
	ret->lencmd = ret->cmdcount * sizeof(cmdmessage);	// 1 byte длина всех команд = 30 - 3 - 3 = менее равно 24 байта

	// get parameters
	if (pars != NULL)
	{
		char *parameters[3];
#ifdef _MSC_VER
		char *next_token = NULL;
#endif

		char *pa = strtok_s(pars, MESG_ARRAY_DELIM, &next_token);
		int cc = 0;
		while ((cc < ret->cmdcount) && (pa != NULL))
		{
			parameters[cc] = pa;
			pa = strtok_s(NULL, MESG_ARRAY_DELIM, &next_token);
			cc++;
		}
		for (; cc < 3; cc++)
		{
			parameters[cc] = NULL;
		}

		int argcount;
		for (cc = 0; cc < ret->cmdcount; cc++ )
		{
			cmdmessage *c = &ret->cmd[cc];
			if (parameters[cc] == NULL)
			{
				// command only, no parameters
				c->len = sizeof(uint8_t);
				continue;
			}
#ifdef _MSC_VER
			char *next_token = NULL;
#endif

			char *p = strtok_s(parameters[cc], PARAM_DELIM, &next_token);
			argcount = 0;
			while (p != NULL)
			{
				c->args[argcount] = atoi(p);
				argcount++;
				p = strtok_s(NULL, PARAM_DELIM, &next_token);
			}
			c->len = (uint8_t) sizeof(uint8_t) + argcount * sizeof(uint8_t);	// 1 байт длина команды с самой командой!
		}
	}
	return sizeof(packet88_t);
}

/**
 * Parameters:
 * 	commandidentifier 4 chars (truncates if longer)
 */
CALLC int appCommands2PChar(void *value, size_t size,
		const char *imei, const char *commandidentifier,
		char *cmds, char* pars)
{
	IEMTIOHeader header;
	TMTIOHeader::setMsgId(&header, commandidentifier);
	TIMEI::copy(&header.imei, imei);
	header.flags = 0;

	packet88_t p88;
	appCommands2Packet88(&p88, cmds, pars);

	TIridiumMessageBuild b(&header, (packet_t*) &p88);
	int r = b.getContainerSize();
	b.convert();
	int sz = ((unsigned int)r > size) ? size : r;
	memmove(value, b.getData(), sz);
	return r;
}

/**
  * iridiumxls options
  * @file iridiumxls-config.h
  **/

#ifndef IRIDIUMXLS_CONFIG_H
#define IRIDIUMXLS_CONFIG_H

#include <string>
#include <vector>
#include <libpq-fe.h>

#define FMT_HEX		0
#define FMT_JSON	1
#define FMT_XLSX	2
/**
 * Command line interface (CLI) tool configuration structure
 */
class IridiumxlsConfig
{
private:
	/**
	* Parse command line into IridiumxlsConfig class
	* Return 0- success
	*        1- show help and exit, or command syntax error
	*        2- output file does not exists or can not open to write
	**/
	int parseCmd
	(
		int argc,
		char* argv[]
	);
	int errorcode;
public:
	int format;
	std::string proxy_host;
	std::string file_name;							///< output file name
	std::string login;								///< login
	std::string passwd;								///< password 
	int start;
	int finish;
	std::vector<std::string> imeis;
	
	std::string sql_condition;
	
	// PostgreSQL
	std::string dbconn;
	std::string dboptionsfile;
	std::string dbname;
	std::string dbuser;
	std::string dbpassword;
	std::string dbhost;
	std::string dbport;
	std::string dbsocket;
	std::string dbcharset;
	int dbclientflags;
	
	int verbosity;
	
	IridiumxlsConfig();
	IridiumxlsConfig
	(
		int argc,
		char* argv[]
	);
	int error();
};

/**
 * Establish configured database connection
 */
PGconn *dbconnect(const IridiumxlsConfig *config);

#endif

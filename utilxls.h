#include "iridiumxls.h"
#include "iridiumxls-config.h"

#include "platform.h"

int save2XLSX
(
	const IridiumxlsConfig *config,
	int *errorCode,
	std::string *errorDescription,
	const std::string &sqlCondition
);

#include "iridiumxls-config.h"
#include <iostream>
#include <argtable2.h>
#include <string.h>
#include <stdlib.h>

#include "platform.h"

// PostgreSQL
#define DEF_DB_HOST					"localhost"
#define DEF_DB_PORT					"5432"
#define DEF_DATABASESOCKET			""
#define DEF_DATABASECHARSET			"utf8"
#define DEF_DATABASECLIENTFLAGS		0

static const char* progname = "iridiumxls";
static const char *dateformat = "%FT%T";

/**
 * Unix epoch time (seconds) or 2015-11-25T23:59:11
 */
static time_t parseDate(const char *v)
{
	struct tm tmd;
	memset(&tmd, 0, sizeof(struct tm));

	time_t r;
	if (strptime(v, dateformat, &tmd) == NULL)
		r = strtol(v, NULL, 0);
	else
		r = mktime(&tmd);
	return r;
}

IridiumxlsConfig::IridiumxlsConfig()
	: file_name(""), verbosity(0), sql_condition("")
{
}

IridiumxlsConfig::IridiumxlsConfig
(
	int argc,
	char* argv[]
)
{
	errorcode = parseCmd(argc, argv);
}

/**
 * Parse command line into IridiumxlsConfig class
 * Return 0- success
 *        1- show help and exit, or command syntax error
 *        2- output file does not exists or can not open to write
 **/
int IridiumxlsConfig::parseCmd
(
	int argc,
	char* argv[]
)
{
	// GTFS https://developers.google.com/transit/gtfs/reference/?csw=1
	struct arg_file *a_file_name = arg_file0("f", "file", "<file>", "Output file name.");

	// time limits
	struct arg_str *a_start = arg_str0(NULL, "start", "<local time>", "e.g. 2018-04-01T00:00:00 or 1522508400 (Unix seconds)");
	struct arg_str *a_finish = arg_str0(NULL, "finish", "<local time>", "e.g. 2018-04-01T23:59:59 or 1522594799");

	// imeis filter
	struct arg_str *a_imeis = arg_strn(NULL, NULL, "<IMEI>", 0, 100, "e.g. 300234010157130");
	// output format
	struct arg_int *a_format = arg_int0("o", "format", "<number>", "0- hex, 1- JSON, 2- XLSX. Default 0.");

	struct arg_str *a_sql_condition = arg_str0(NULL, "sql", "<contition>", "e.g. \"raw_id > 59859\"");
	
	// user credentials
	struct arg_str *a_login = arg_str0("u", "login", "<login>", "user login name");
	struct arg_str *a_passwd = arg_str0("p", "passwd", "<password>", "user password (optional)");
	
	// database connection
	struct arg_str *a_conninfo = arg_str0(NULL, "conninfo", "<string>", "database connection");
	struct arg_str *a_user = arg_str0(NULL, "user", "<login>", "database login");
	struct arg_str *a_database = arg_str0(NULL, "database", "<scheme>", "database scheme");
	struct arg_str *a_password = arg_str0(NULL, "password", "<password>", "database user password");
	struct arg_str *a_host = arg_str0(NULL, "host", "<host>", "database host. Default localhost");
	struct arg_str *a_dbport = arg_str0(NULL, "port", "<integer>", "database port. Default 5432");
	struct arg_file *a_optionsfile = arg_file0(NULL, "options-file", "<file>", "database options file");
	struct arg_str *a_dbsocket = arg_str0(NULL, "dbsocket", "<socket>", "database socket. Default none.");
	struct arg_str *a_dbcharset = arg_str0(NULL, "dbcharset", "<charset>", "database client charset. Default utf8.");
	struct arg_int *a_dbclientflags = arg_int0(NULL, "dbclientflags", "<number>", "database client flags. Default 0.");

	// verbosity
	struct arg_lit *a_verbosity = arg_litn("v", "verbose", 0, 3, "-vvv - debug. Default none.");
	
	struct arg_lit *a_help = arg_lit0("h", "help", "Show this help");
	struct arg_end *a_end = arg_end(20);

	void* argtable[] = { 
		a_file_name, 
		a_format,
		a_start, a_finish, a_imeis,
		a_sql_condition,
		a_login, a_passwd,
		a_conninfo, a_user, a_database, a_password, a_host, a_dbport, a_optionsfile, a_dbsocket, a_dbcharset, a_dbclientflags,
		a_verbosity, a_help, a_end 
	};

	int nerrors;

	// verify the argtable[] entries were allocated successfully
	if (arg_nullcheck(argtable) != 0)
	{
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}
	// Parse the command line as defined by argtable[]
	nerrors = arg_parse(argc, argv, argtable);

	if (a_sql_condition ->count)
		sql_condition = *a_sql_condition->sval;
	else
		sql_condition = "";

	if (a_format ->count)
		format = *a_format ->ival;
	else
		format = 0;

	if (a_file_name->count)
		file_name = *a_file_name->filename;
	else
		file_name = "";

	if (a_login->count)
		login = *a_login->sval;
	else
		login = "";
	
	if (login.empty())
	{
		nerrors++;
		std::cerr << "No user login provided." << std::endl;
	}

	if (a_passwd->count)
		passwd = *a_passwd->sval;
	else
		passwd = "";

	if (a_start->count)
		start = parseDate(*a_start->sval);
	else
		start = 0;

	if (a_finish->count)
		finish = parseDate(*a_finish->sval);
	else
		finish = 0;

	for (int i = 0; i < a_imeis->count; i++)
	{
		imeis.push_back(a_imeis->sval[i]);
	}

	dbconn = *a_conninfo->sval;
	if (a_host->count)
		dbhost = *a_host->sval;
	else
		dbhost = DEF_DB_HOST;

	if (a_dbport->count)
		dbport = *a_dbport->sval;
	else
		dbport = DEF_DB_PORT;

	dboptionsfile = *a_optionsfile->filename;
	dbname = *a_database->sval;
	dbuser = *a_user->sval;
	dbpassword = *a_password->sval;
	if (a_dbsocket->count)
		dbsocket = *a_dbsocket->sval;
	else
		dbsocket = DEF_DATABASESOCKET;

	if (a_dbcharset->count)
		dbcharset = *a_dbcharset->sval;
	else
		dbcharset = DEF_DATABASECHARSET;

	if (a_dbclientflags->count)
		dbclientflags = *a_dbclientflags->ival;
	else
		dbclientflags = DEF_DATABASECLIENTFLAGS;

	verbosity = a_verbosity->count;

	// special case: '--help' takes precedence over error reporting
	if ((a_help->count) || nerrors)
	{
		if (nerrors)
			arg_print_errors(stderr, a_end, progname);
		std::cerr << "Usage: " << progname << std::endl;
		arg_print_syntax(stderr, argtable, "\n");
		std::cerr << "Save data in XLSX spreadsheat file utility." << std::endl;
		arg_print_glossary(stderr, argtable, "  %-25s %s\n");
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return 0;
}

int IridiumxlsConfig::error()
{
	return errorcode;
}

/**
 * Establish configured database connection
 */
PGconn *dbconnect(const IridiumxlsConfig *config)
{
	if (!config->dbconn.empty())
		return PQconnectdb(config->dbconn.c_str());
	else
		return PQsetdbLogin(config->dbhost.c_str(), config->dbport.c_str(), config->dboptionsfile.c_str(),
			NULL, config->dbname.c_str(), config->dbuser.c_str(), config->dbpassword.c_str());
}

#ifdef __cplusplus
#define CALLC extern "C" 
#else
#define CALLC
#endif

#ifdef _MSC_VER
#include "strptime.h"
#define FILE_DELIMITER	"\\"
#else
#define FILE_DELIMITER	"/"
#endif


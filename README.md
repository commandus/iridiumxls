# iridiumxls

Dump collar data into XLS file CGI utility, or print out JSON.

- [Repository git@gitlab.com:commandus/iridiumxls.git](https://gitlab.com/commandus/iridiumxls.git)
- [Site https://gitlab.com/commandus/iridiumxls](https://gitlab.com/commandus/iridiumxls)


Related projects: 

- [Android client] (https://gitlab.com/commandus/IridiumDeer)
- [C++ library] (https://gitlab.com/commandus/collarie)
- [Data processing utilities] (https://gitlab.com/commandus/pkt2)
- [SMS to Google Sheet Android client] (https://gitlab.com/commandus/sberbanksms)

## Usage

iridiumxls -o outputformat -u login -p password -f output --start unixepoch --finish unixepoch

For instance, 

```
iridiumxls -o 2 -u agro -p password --file "agro1.xlsx"
```

will save agro1.xlsx file in current directory

### Options

Date time format for options:

- start
- finish

is ISO 8601.

e.g. 2018-04-01T00:00:00 

or Unix epoch seconds e.g. 1522508400


Show all options:

```
iridiumxls --help
```

## Build

```
./autogen.sh
automake --add-missing
./configure
make
sudo make install
```

## Dependencies

- [libxlsxwriter](https://github.com/jmcnamara/libxlsxwriter) git@github.com:jmcnamara/libxlsxwriter.git
- [argtable2](http://argtable.sourceforge.net/doc/argtable2.html)
- strptime() Windows implementation (see License) 

Make each libraries static
```
./configure --enable-static --disable-shared
...
```
if you want.

## License

GNU General Public License v3

strptime() implementation Copyright (c) 1999 Kungliga Tekniska Hï¿œgskolan (Royal Institute of Technology, Stockholm, Sweden). All rights reserved.

## Building

### Linux

#### Сборка в docker для nova.ysn.ru

Предварительно нужно развернуть образ centos:nova с необходимыми инструментами и библиотеками.

В частности, установить библиотеку:

```
git clone https://github.com/jmcnamara/libxlsxwriter.git
cd libxlsxwriter

```

На nova остановите процессы:
```
ssh nova.ysn.ru
ps -ef | grep iridium_data_server
kill xxx
```

Запустите в docker со смонтированным каталогом с исходниками bash:
```
docker run -itv /home/andrei/src:/home/andrei/src centos:nova bash
```

В нем из каталога с исходными кодами пересоберите проект:

```
cd /home/andrei/src/iridiumxls
./tools/rebuild-nova
exit
```

Выйдя, закоммитить образ:

```
docker ps -a
docker commit stoic_ramanujan
docker images
docker tag c30cb68a6443 centos:nova
```

Запустить сервис iridium на nova:

```
cd /home/andrei/src/iridium
export LD_LIBRARY_PATH=/home/andrei/pkt2/lib;cd ~/src/iridium/
nohup ./iridium_data_server &
```
### Запуск в nova.ysn.ru

копировать недостающие библиотеки из docker, как:

```
scp /usr/local/lib/libxlsxwriter.so andrei@nova.ysn.ru:/home/andrei/pkt2/lib
```

Указать папку с библиотеками
```
export LD_LIBRARY_PATH=/home/andrei/pkt2/lib
nohup ./iridium_data_server &
```

### Тест xlsx

echo "XLS:84.237.104.57:biolog:21:1522335600:1522940400:300234060235340" | nc nova.ysn.ru 5680 > 1.xls

## SQL

update raw set flags =
case length(data) / 2
when 34 then 6
when 48 then 4
when 67 then 2
when 81 then 0
else 7
end;

#include "utilxls.h"

#include <iostream>
#include <sstream>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <xlsxwriter.h>

#include "collar/iridiumpacket.h"
#include "collar/iridiumpacketattr.h"
#include "collar/iridiummessage.h"
#include "collar/iridiummessageparser.h"

#define CHECK_STATEMENT(r, ret) \
if (!r || PQresultStatus(r) != ret) \
{ \
	if (errorCode) \
		*errorCode = PQresultStatus(r); \
	if (errorDescription) \
		*errorDescription = PQerrorMessage(db); \
	if (r) \
		PQclear(r); \
	PQfinish(db); \
	return ERR_DB_STATEMENT; \
}

const char *dateformats[] = {"%FT%T", "%F %T"};

#define COLUMN_COUNT	44
static const char *columns[COLUMN_COUNT] = {
	"version", "size",			// packet
	"cddref", "imei", "name",	// IO
	"status", "sentno",
	"recvno", "date", 
	"recvtime", "time",
	"lat", "lon",				// location
	"cepradius", "gpstime",		// packet8
	"gpsnavdata", "gpsolddata",
	"gpsencoded", "gpsfrommemory",
	"gpsnoformat", "gpsnosats",
	"gpsbadhdop", "satellites",
	"lat", "lon", 
	"hdop", "pdop",
	"alarmlow", "alarmhigh",
	"battery", "temperature",
	"r2", "failurepower",
	"failureeep", "failureclock",
	"failurecable", "failureint0",
	"failurewatchdog", "failurenoise",
	"failureworking", "key",
	"date", "time",
	"utc"
};

static const int widths[COLUMN_COUNT] = {
	1, 2,			// packet
	11, 17, 20,		// IO
	1, 5,
	5, 20, 
	20, 15, 
	10, 10, 		// location
	5, 1, 			// packet8
	1, 1, 
	1, 1, 
	1, 1, 
	1, 5, 
	10, 10, 
	5, 5, 
	1, 1, 
	3, 4, 
	3, 3, 
	1, 1, 
	1, 1, 
	1, 1, 
	1, 6, 
	20, 19, 
	13
};

int save2XLSX
(
	const IridiumxlsConfig *config,
	int *errorCode,
	std::string *errorDescription,
	const std::string &sqlCondition
)
{
	PGconn *db = dbconnect(config);
	if (!db)
		return CONNECTION_BAD;
	ConnStatusType status = PQstatus(db);
	if (status != CONNECTION_OK)
		return status;
	
	/* start a transaction block */
    PGresult *r = PQexec(db, "BEGIN");
	CHECK_STATEMENT(r, PGRES_COMMAND_OK)
		
	/* Convert integer value "2" to network byte order */
	uint32_t start = htonl((uint32_t) config->start);
	uint32_t finish = htonl((uint32_t) config->finish);

	
	const char *paramValues[3];
	int paramLengths[3];
	int paramFormats[3];
    /* Set up parameter arrays for PQexecParams */
	paramValues[0] = (char *) &start;
	paramValues[1] = (char *) &finish;
	paramValues[2] = (char *) config->login.c_str();
	paramLengths[0] = sizeof(start);
	paramLengths[1] = sizeof(finish);
	paramLengths[2] = sizeof(config->login.size());
	paramFormats[0] = 1;	// binary
	paramFormats[1] = 1;	// binary
	paramFormats[2] = 0;	// text

	std::stringstream ss;
	if (config->login == "root")
	{
		ss << "SELECT raw_id, extract(epoch from raw.when at time zone 'Asia/Yakutsk')::int4 dt, data, extractname(data) nm FROM raw where \
			extract(epoch from raw.when at time zone 'Asia/Yakutsk')::int4 >= $1::int4 and \
			extract(epoch from raw.when at time zone 'Asia/Yakutsk')::int4 <= $2::int4 AND $3 = $3";
	}
	else
	{
		ss << "SELECT raw_id, extract(epoch from raw.when at time zone 'Asia/Yakutsk')::int4 dt, data, extractname(data) nm FROM raw where \
			extract(epoch from raw.when at time zone 'Asia/Yakutsk')::int4 >= $1::int4 and \
			extract(epoch from raw.when at time zone 'Asia/Yakutsk')::int4 <= $2::int4 \
			and extractimei(data) in (select devices.imei FROM device_description,devices,users WHERE \
			device_description.imei = devices.id and device_description.current = 't' \
			and users.u_id = device_description.owner and users.accessdb = 'oper' and \
			users.login = $3)";
	}
	if (!sqlCondition.empty())
		ss << " AND (" << sqlCondition << ")";
	ss << " ORDER BY raw.raw_id ASC";

	if (config->imeis.size())
	{
		ss << " AND extractimei(data) IN (";
		for (std::vector<std::string>::const_iterator it = config->imeis.begin(); it != config->imeis.end(); ++it)
		{
			ss << "'" << *it << "', ";
		}
		ss << "'')";
	}
	
	if (config->verbosity > 2)
		std::cerr << ss.str() << std::endl;
	
	r = PQexecParams(db,
		ss.str().c_str(),
		3,
		NULL,
		paramValues,
		paramLengths,
		paramFormats,
	1);

	CHECK_STATEMENT(r, PGRES_TUPLES_OK)

	lxw_workbook  *workbook = NULL;
	lxw_worksheet *worksheet = NULL;
	lxw_format *formatBold = NULL;
	lxw_format *formatDate = NULL;
	if (config->format>= 2) {
		// Create a new workbook and add a worksheet.
		workbook  = workbook_new(config->file_name.c_str());
		worksheet = workbook_add_worksheet(workbook, "Collar");
		formatBold = workbook_add_format(workbook);
		formatDate = workbook_add_format(workbook);
		format_set_num_format(formatDate, "dd-mm-yyyy hh:mm:ss");
		format_set_bold(formatBold);

		for (int i = 0; i < COLUMN_COUNT; i++)
		{
			worksheet_write_string(worksheet, 0, i, columns[i], formatBold);
			worksheet_set_column(worksheet, i, i, widths[i], NULL);	
		}
		worksheet_freeze_panes(worksheet, 1, 0);

	}
	
	int fraw_id = PQfnumber(r, "raw_id");
	int fdt = PQfnumber(r, "dt");
	int fdata = PQfnumber(r, "data");
	int fname = PQfnumber(r, "nm");
	for (int i = 0; i < PQntuples(r); i++)
	{
		char *vraw_id = PQgetvalue(r, i, fraw_id);
		uint32_t raw_id = be64toh(*((uint64_t *) vraw_id));
		
		char *vdt = PQgetvalue(r, i, fdt);
		uint32_t dt = ntohl(*((uint32_t *) vdt));
		char *vdata = PQgetvalue(r, i, fdata);
		size_t len = PQgetlength(r, i, fdata);
		std::string data(vdata, len);

		char *vname = PQgetvalue(r, i, fname);
		len = PQgetlength(r, i, fname);
		std::string name(vname, len);

		switch(config->format) {
			case 0:
				std::cout << name << "\t" << dt << "\t" << data << std::endl;
				break;
			case 1:
				{
					TIridiumMessageBuild m(data.c_str());
					std::string r;
					if (m.valid())
					{
						r = name + "\t" + m.toJson();	// std::to_string(raw_id) + "\t" + 
					}
					std::cout << r << std::endl;
				}
				break;
			default:
				{
					TIridiumMessageBuild m(data.c_str());
					if (m.valid())
					{
						
						TInfoElement *packet = m.getMainPacket();
						if (packet) {
							worksheet_write_number(worksheet, i + 1, 0, packet->getVersion(), NULL);
							worksheet_write_number(worksheet, i + 1, 1, packet->getSize(), NULL);
							TIOHeader *io = m.getIOHeader();
							if (io) {
								char dt[32];
								time_t t = io->getRecvtime();
								struct tm tm;
								localtime_s(&tm, &t);
								strftime(dt, sizeof(dt), dateformats[0], &tm);
								lxw_datetime wd;
								wd.year = tm.tm_year + 1900;
								wd.month = tm.tm_mon;
								wd.day = tm.tm_mday;
								wd.hour = tm.tm_hour;
								wd.min = tm.tm_min;
								wd.sec = tm.tm_sec;

								worksheet_write_number(worksheet, i + 1, 2, io->getCdrref(), NULL);
								worksheet_write_string(worksheet, i + 1, 3, TIMEI::toString((IMEI*) io->getImei()).c_str(), NULL);
								worksheet_write_string(worksheet, i + 1, 4, name.c_str(), NULL);
								worksheet_write_number(worksheet, i + 1, 5, io->getStatus(), NULL);
								worksheet_write_number(worksheet, i + 1, 6, io->getSentno(), NULL);
								worksheet_write_number(worksheet, i + 1, 7, io->getRecvno(), NULL);
								worksheet_write_datetime(worksheet, i + 1, 8, &wd, formatDate);
								worksheet_write_string(worksheet, i + 1, 9, dt, NULL);
								worksheet_write_number(worksheet, i + 1, 10, t, NULL);
							}
							
							TIELocation *l = m.getLocation();
							if (l) {
								worksheet_write_number(worksheet, i + 1, 11, l->getLatitude(), NULL);
								worksheet_write_number(worksheet, i + 1, 12, l->getLongitude(), NULL);
								worksheet_write_number(worksheet, i + 1, 13, l->getCEPRadius(), NULL);
							}

							TIEPayload *p = m.getPayload();
							if (p) {
								packet8_t *p8 = p->get8();
								if (p8)
								{
									char dt[32];
									time_t t = time52time_t(&(p8->time));
									struct tm tm;
									localtime_s(&tm, &t);
									strftime(dt, sizeof(dt), dateformats[0], &tm);
									lxw_datetime wd;
									wd.year = tm.tm_year + 1900;
									wd.month = tm.tm_mon;
									wd.day = tm.tm_mday;
									wd.hour = tm.tm_hour;
									wd.min = tm.tm_min;
									wd.sec = tm.tm_sec;
									worksheet_write_number(worksheet, i + 1, 14, (int) p8->gpstime, NULL);
									worksheet_write_number(worksheet, i + 1, 15, (int) p8->gpsnavdata , NULL);
									worksheet_write_number(worksheet, i + 1, 16, (int) p8->gpsolddata, NULL);
									worksheet_write_number(worksheet, i + 1, 17, (int) p8->gpsencoded, NULL);
									worksheet_write_number(worksheet, i + 1, 18, (int) p8->gpsfrommemory, NULL);
									worksheet_write_number(worksheet, i + 1, 19, (int) p8->gpsnoformat, NULL);
									worksheet_write_number(worksheet, i + 1, 20, (int) p8->gpsnosats, NULL);
									worksheet_write_number(worksheet, i + 1, 21, (int) p8->gpsbadhdop, NULL);
									worksheet_write_number(worksheet, i + 1, 22, (int) p8->sats, NULL);
									GeoLocation g (&(p8->coord));
									worksheet_write_number(worksheet, i + 1, 23, g.lat, NULL);
									worksheet_write_number(worksheet, i + 1, 24, g.lon, NULL);
									worksheet_write_number(worksheet, i + 1, 25, g.hdop, NULL);
									worksheet_write_number(worksheet, i + 1, 26, g.pdop, NULL);
									worksheet_write_number(worksheet, i + 1, 27, (int) p8->alarmlow, NULL);
									worksheet_write_number(worksheet, i + 1, 28, (int) p8->alarmhigh, NULL);
									worksheet_write_number(worksheet, i + 1, 29, (int) p8->bat / 10.0, NULL);
									worksheet_write_number(worksheet, i + 1, 30, (int) p8->temperature, NULL);
									worksheet_write_number(worksheet, i + 1, 31, (int) p8->r2, NULL);
									worksheet_write_number(worksheet, i + 1, 32, (int) p8->failurepower, NULL);
									worksheet_write_number(worksheet, i + 1, 33, (int) p8->failureeep, NULL);
									worksheet_write_number(worksheet, i + 1, 34, (int) p8->failureclock, NULL);
									worksheet_write_number(worksheet, i + 1, 35, (int) p8->failurecable, NULL);
									worksheet_write_number(worksheet, i + 1, 36, (int) p8->failureint0, NULL);
									worksheet_write_number(worksheet, i + 1, 37, (int) p8->failurewatchdog, NULL);
									worksheet_write_number(worksheet, i + 1, 38, (int) p8->failurenoise, NULL);
									worksheet_write_number(worksheet, i + 1, 39, (int) p8->failureworking, NULL);
									worksheet_write_number(worksheet, i + 1, 40, (int) p8->key, NULL);
									worksheet_write_datetime(worksheet, i + 1, 41, &wd, formatDate);
									worksheet_write_string(worksheet, i + 1, 42, dt, NULL);
									worksheet_write_number(worksheet, i + 1, 43, t, NULL);
								}
							}
						}
					}
				}
				break;
		}
	}

	PQclear(r);

	if (config->format >= 2) {
		if (workbook)
			workbook_close(workbook);
	}


    r = PQexec(db, "COMMIT");
	CHECK_STATEMENT(r, PGRES_COMMAND_OK)
	
	// close the connection to the database and cleanup
    PQfinish(db);
	return 0;
}
